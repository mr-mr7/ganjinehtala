<?php
if (!defined('ABSPATH')) exit;
function Load_IranKish5_Gateway()
{
	if (class_exists('WC_Payment_Gateway') && !class_exists('WC_Gateway_Irankish5') && !function_exists('Woocommerce_Add_IranKish5_Gateway')) {

		add_filter('woocommerce_payment_gateways', 'Woocommerce_Add_IranKish5_Gateway');
		function Woocommerce_Add_IranKish5_Gateway($methods)
		{
			$methods[] = 'WC_Gateway_Irankish5';
			return $methods;
		}


		class WC_Gateway_Irankish5 extends WC_Payment_Gateway
		{

			public function __construct()
			{
				$this->author = 'irankish';
				$this->id = 'irankish5';
				$this->method_title = 'ایران‌کیش';
				$this->method_description = 'تنظیمات درگاه پرداخت ایران‌کیش برای افزونه فروشگاه ساز ووکامرس
				';
				$this->icon = apply_filters('WC_IranKish5_logo', WP_PLUGIN_URL . "/" . plugin_basename(dirname(__FILE__)) . '/assets/images/logo.png');
				$this->has_fields = false;

				$this->init_form_fields();
				$this->init_settings();

				$this->title = $this->settings['title'];
				$this->description = $this->settings['description'];

				$this->terminalID = $this->settings['terminalID'];
				$this->password = $this->settings['password'];
				$this->acceptOrId = $this->settings['acceptorId'];
				$this->pubKey = $this->settings['pub_key'];


				$this->success_massage = $this->settings['success_massage'];
				$this->failed_massage = $this->settings['failed_massage'];
				$this->cancelled_massage = $this->settings['cancelled_massage'];

				if (version_compare(WOOCOMMERCE_VERSION, '2.0.0', '>='))
					add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
				else
					add_action('woocommerce_update_options_payment_gateways', array($this, 'process_admin_options'));
				add_action('woocommerce_receipt_' . $this->id . '', array($this, 'Send_to_IranKish5_Gateway'));
				add_action('woocommerce_api_' . strtolower(get_class($this)) . '', array($this, 'Return_from_IranKish5_Gateway'));
			}


			public function admin_options()
			{
				$action = $this->author;
				do_action('WC_Gateway_Payment_Actions', $action);
				parent::admin_options();
			}

			public function init_form_fields()
			{
				$this->form_fields = apply_filters(
					'WC_IranKish5_Config',
					array(
						'base_confing' => array(
							'title'       =>  'تنظیمات پایه ای',
							'type'        => 'title',
							'description' => '',
						),
						'enabled' => array(
							'title'   =>  'فعالسازی/غیرفعالسازی',
							'type'    => 'checkbox',
							'label'   =>  'فعالسازی درگاه ایران‌کیش',
							'description' =>  'برای فعالسازی درگاه پرداخت ایران‌کیش باید چک باکس را تیک بزنید',
							'default' => 'yes',
							'desc_tip'    => true,
						),
						'title' => array(
							'title'       =>  'عنوان درگاه',
							'type'        => 'text',
							'description' =>  'عنوان درگاه که در طی خرید به مشتری نمایش داده میشود',
							'default'     =>  'ایران‌کیش',
							'desc_tip'    => true,
						),
						'description' => array(
							'title'       =>  'توضیحات درگاه',
							'type'        => 'text',
							'desc_tip'    => true,
							'description' =>  'توضیحاتی که در طی عملیات پرداخت برای درگاه نمایش داده خواهد شد',
							'default'     =>  'پرداخت امن به وسیله کلیه کارت های عضو شتاب از طریق درگاه ایران‌کیش'
						),
						'account_confing' => array(
							'title'       =>  'تنظیمات حساب ایران‌کیش',
							'type'        => 'title',
							'description' => '',
						),
						'terminalID' => array(
							'title'       =>  'شماره پایانه',
							'type'        => 'text',
							'description' =>  'شماره پایانه درگاه ایران‌کیش',
							'default'     => '',
							'desc_tip'    => true
						),
						'password' => array(
							'title'       =>  'کلمه عبور',
							'type'        => 'text',
							'description' =>  'کلمه عبور درگاه ایران‌کیش',
							'default'     => '',
							'desc_tip'    => true
						),
						'acceptorId' => array(
							'title'       =>  'شماره پذیرنده',
							'type'        => 'text',
							'description' =>  'شماره پذیرنده درگاه ایران‌کیش',
							'default'     => '',
							'desc_tip'    => true
						),
						'pub_key' => array(
							'title'       =>  'کلید عمومی',
							'type'        => 'textarea',
							'description' =>  'کلید عمومی درگاه ایران‌کیش',
							'default'     => '',
							'desc_tip'    => true
						),
						'payment_confing' => array(
							'title'       =>  'تنظیمات عملیات پرداخت',
							'type'        => 'title',
							'description' => '',
						),
						'success_massage' => array(
							'title'       =>  'پیام پرداخت موفق',
							'type'        => 'textarea',
							'description' =>  'متن پیامی که میخواهید بعد از پرداخت موفق به کاربر نمایش دهید را وارد نمایید . همچنین می توانید از شورت کد {refrenceID} برای نمایش کد رهگیری ( کد مرجع تراکنش ) و از شرت کد استفاده نمایید .',
							'default'     =>  'با تشکر از شما . سفارش شما با موفقیت پرداخت شد .',
						),
						'failed_massage' => array(
							'title'       =>  'پیام پرداخت ناموفق',
							'type'        => 'textarea',
							'description' =>  'متن پیامی که میخواهید بعد از پرداخت ناموفق به کاربر نمایش دهید را وارد نمایید . همچنین می توانید از شورت کد {fault} برای نمایش دلیل خطای رخ داده استفاده نمایید . این دلیل خطا از سایت ایران‌کیش ارسال میگردد .',
							'default'     =>  'پرداخت شما ناموفق بوده است . لطفا مجددا تلاش نمایید یا در صورت بروز اشکال با مدیر سایت تماس بگیرید .',
						),
						'cancelled_massage' => array(
							'title'       =>  'پیام انصراف از پرداخت',
							'type'        => 'textarea',
							'description' =>  'متن پیامی که میخواهید بعد از انصراف کاربر از پرداخت نمایش دهید را وارد نمایید . این پیام بعد از بازگشت از بانک نمایش داده خواهد شد .',
							'default'     =>  'پرداخت به دلیل انصراف شما ناتمام باقی ماند .',
						),
					)
				);
			}

			public function process_payment($order_id)
			{
				$order = new WC_Order($order_id);
				return array(
					'result'   => 'success',
					'redirect' => $order->get_checkout_payment_url(true)
				);
			}


			public function generateAuthenticationEnvelope($pub_key, $terminalID, $password, $amount)
			{
				$data = $terminalID . $password . str_pad($amount, 12, '0', STR_PAD_LEFT) . '00';
				$data = hex2bin($data);
				$AESSecretKey = openssl_random_pseudo_bytes(16);
				$ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
				$iv = openssl_random_pseudo_bytes($ivlen);
				$ciphertext_raw = openssl_encrypt($data, $cipher, $AESSecretKey, $options = OPENSSL_RAW_DATA, $iv);
				$hmac = hash('sha256', $ciphertext_raw, true);
				$crypttext = '';

				openssl_public_encrypt($AESSecretKey . $hmac, $crypttext, $pub_key);

				return array(
					"data" => bin2hex($crypttext),
					"iv" => bin2hex($iv),
				);
			}

			public function Send_to_IranKish5_Gateway($order_id)
			{
				global $woocommerce;
				$woocommerce->session->order_id_irankish5 = $order_id;
				$order = new WC_Order($order_id);
				$currency = $order->get_order_currency();
				$currency = apply_filters('WC_IranKish5_Currency', $currency, $order_id);
				$action = $this->author;
				do_action('WC_Gateway_Payment_Actions', $action);
				$form = '<form action="" method="POST" class="irankish5-checkout-form" id="irankish5-checkout-form">
						<input type="submit" name="irankish5_submit" class="button alt" id="irankish5-payment-button" value="' . 'پرداخت' . '"/>
						<a class="button cancel" href="' . $woocommerce->cart->get_checkout_url() . '">' .  'بازگشت' . '</a>
					 </form><br/>';
				$form = apply_filters('WC_IranKish5_Form', $form, $order_id, $woocommerce);

				do_action('WC_IranKish5_Gateway_Before_Form', $order_id, $woocommerce);
				echo $form;
				do_action('WC_IranKish5_Gateway_After_Form', $order_id, $woocommerce);

				if (isset($_POST["irankish5_submit"])) {

					$Amount = intval($order->order_total);
					$Amount = apply_filters('woocommerce_order_amount_total_IRANIAN_gateways_before_check_currency', $Amount, $currency);
					if (
						strtolower($currency) == strtolower('IRT') || strtolower($currency) == strtolower('TOMAN')
						|| strtolower($currency) == strtolower('Iran TOMAN') || strtolower($currency) == strtolower('Iranian TOMAN')
						|| strtolower($currency) == strtolower('Iran-TOMAN') || strtolower($currency) == strtolower('Iranian-TOMAN')
						|| strtolower($currency) == strtolower('Iran_TOMAN') || strtolower($currency) == strtolower('Iranian_TOMAN')
						|| strtolower($currency) == strtolower('تومان') || strtolower($currency) == strtolower('تومان ایران')
					)
						$Amount = $Amount * 10;
					else if (strtolower($currency) == strtolower('IRHT'))
						$Amount = $Amount * 1000 * 10;
					else if (strtolower($currency) == strtolower('IRHR'))
						$Amount = $Amount * 1000;

					$Amount = apply_filters('woocommerce_order_amount_total_IRANIAN_gateways_after_check_currency', $Amount, $currency);
					$Amount = apply_filters('woocommerce_order_amount_total_IRANIAN_gateways_irr', $Amount, $currency);
					$Amount = apply_filters('woocommerce_order_amount_total_IranKish5_gateway', $Amount, $currency);



					do_action('WC_IranKish5_Gateway_Payment', $order_id);


					$callBackUrl = add_query_arg('wc_order', $order_id, WC()->api_request_url('WC_Gateway_Irankish5'));
					$token = $this->generateAuthenticationEnvelope($this->pubKey, $this->terminalID, $this->password, $Amount);

					$data = array();
					$data["request"] = array(
						"acceptorId" => $this->acceptOrId,
						"amount" => (int) $Amount,
						"billInfo" => null,
						"requestId" => uniqid(),
						"paymentId" => (string) $order_id,
						"requestTimestamp" => time(),
						"revertUri" => $callBackUrl,
						"terminalId" => $this->terminalID,
						"transactionType" => "Purchase"
					);
					$data['authenticationEnvelope'] = $token;
					$data_string = json_encode($data);
					$ch = curl_init('https://ikc.shaparak.ir/api/v3/tokenization/make');
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						'Content-Type: application/json',
						'Content-Length: ' . strlen($data_string)
					));

					$result = curl_exec($ch);
					$response = array();
					if ($result === false) {
						$is_error = 'yes';
						$error_code = "curl:" + curl_error($ch);
					} else {
						$response = json_decode($result, JSON_OBJECT_AS_ARRAY);

						if ($response["responseCode"] != "00") {
							// $response["description"];
							$is_error = 'yes';
							$error_code = $response["responseCode"];
						} else {
							$Notice =  'در حال اتصال به بانک .....';
							$Notice = apply_filters('WC_IranKish5_Before_Send_to_Gateway_Notice', $Notice, $order_id);
							if ($Notice) {
								wc_add_notice($Notice, 'success');
							}

							do_action('WC_IranKish5_Before_Send_to_Gateway', $order_id);

							echo '<form id="redirect_to_irankish5" method="post" action="https://ikc.shaparak.ir/iuiv3/IPG/Index/" enctype="‫‪multipart/form-data‬‬" style="display:none !important;"  >
										<input type="hidden"  name="tokenIdentity" value="' . $response['result']['token'] . '" />
										<input type="submit" value="Pay"/>
									</form>
									<script language="JavaScript" type="text/javascript">
										document.getElementById("redirect_to_irankish5").submit();
									</script>';
						}
					}


					if ($is_error == 'yes') {

						$fault = $error_code;

						$Note = sprintf(__('خطا در هنگام ارسال به بانک : %s', 'woocommerce'), $this->Fault_IranKish5($fault));
						$Note = apply_filters('WC_IranKish5_Send_to_Gateway_Failed_Note', $Note, $order_id, $fault);
						$order->add_order_note($Note);


						$Notice = sprintf(__('در هنگام اتصال به بانک خطای زیر رخ داده است : <br/>%s', 'woocommerce'), $this->Fault_IranKish5($fault));
						$Notice = apply_filters('WC_IranKish5_Send_to_Gateway_Failed_Notice', $Notice, $order_id, $fault);
						if ($Notice)
							wc_add_notice($Notice, 'error');

						do_action('WC_IranKish5_Send_to_Gateway_Failed', $order_id, $fault);
					}
				}
			}

			public function Return_from_IranKish5_Gateway()
			{

				global $woocommerce;
				$action = $this->author;
				do_action('WC_Gateway_Payment_Actions', $action);

				if (isset($_GET['wc_order']))
					$order_id = sanitize_text_field($_GET['wc_order']);
				else
					$order_id = $woocommerce->session->order_id_irankish5;
				if ($order_id) {


					$order = new WC_Order($order_id);
					$currency = $order->get_order_currency();
					$currency = apply_filters('WC_IranKish5_Currency', $currency, $order_id);

					if ($order->status != 'completed') {

						$Amount = intval($order->order_total);
						$Amount = apply_filters('woocommerce_order_amount_total_IRANIAN_gateways_before_check_currency', $Amount, $currency);
						if (
							strtolower($currency) == strtolower('IRT') || strtolower($currency) == strtolower('TOMAN')
							|| strtolower($currency) == strtolower('Iran TOMAN') || strtolower($currency) == strtolower('Iranian TOMAN')
							|| strtolower($currency) == strtolower('Iran-TOMAN') || strtolower($currency) == strtolower('Iranian-TOMAN')
							|| strtolower($currency) == strtolower('Iran_TOMAN') || strtolower($currency) == strtolower('Iranian_TOMAN')
							|| strtolower($currency) == strtolower('تومان') || strtolower($currency) == strtolower('تومان ایران')
						)
							$Amount = $Amount * 10;
						else if (strtolower($currency) == strtolower('IRHT'))
							$Amount = $Amount * 1000 * 10;
						else if (strtolower($currency) == strtolower('IRHR'))
							$Amount = $Amount * 1000;

						$Amount = apply_filters('woocommerce_order_amount_total_IRANIAN_gateways_after_check_currency', $Amount, $currency);
						$Amount = apply_filters('woocommerce_order_amount_total_IRANIAN_gateways_irr', $Amount, $currency);
						$Amount = apply_filters('woocommerce_order_amount_total_IranKish5_gateway', $Amount, $currency);

						$fault = 0;
						$status = "failed";
						if (sanitize_text_field($_POST['responseCode'] != 0)) {
							if ($_POST['responseCode'] == 17 || $_POST['responseCode'] == '17') {
								$status = 'cancelled';
								$fault = 0;
							} else {
								$status = 'failed';
								$fault = $_POST['responseCode'];
							}
						} else {
							$data = array(
								"terminalId" => $this->terminalID,
								"retrievalReferenceNumber" => sanitize_text_field($_POST['retrievalReferenceNumber']),
								"systemTraceAuditNumber" => sanitize_text_field($_POST['systemTraceAuditNumber']),
								"tokenIdentity" => sanitize_text_field($_POST['token']),
							);

							$data_string = json_encode($data);
							$ch = curl_init('https://ikc.shaparak.ir/api/v3/confirmation/purchase');
							curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
							curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							curl_setopt($ch, CURLOPT_HTTPHEADER, array(
								'Content-Type: application/json',
								'Content-Length: ' . strlen($data_string)
							));

							$result = curl_exec($ch);

							curl_close($ch);

							$response = json_decode($result, JSON_OBJECT_AS_ARRAY);
							if ($response["responseCode"] != 0 && $response["responseCode"] != "00") {
								$status = "failed";
								$fault = $response["responseCode"];
							} else {
								$status = "completed";
							}
						}

						if ($status != 'cancelled' && $status != 'completed') {
							$status = 'failed';
						}



						$refrenceID = sanitize_text_field($_POST['retrievalReferenceNumber']);
						if ($status == 'completed') {
							$action = $this->author;
							do_action('WC_Gateway_Payment_Actions', $action);

							update_post_meta($order_id, '_refrenceID', $refrenceID);



							$order->payment_complete($refrenceID);
							$woocommerce->cart->empty_cart();


							$Note = sprintf(__('پرداخت موفقیت آمیز بود .<br/> کد رهگیری (کد مرجع تراکنش) : %s', 'woocommerce'), $refrenceID);
							$Note = apply_filters('WC_IranKish5_Return_from_Gateway_Success_Note', $Note, $order_id, $refrenceID);
							if ($Note)
								$order->add_order_note($Note, 1);

							$Notice = wpautop(wptexturize($this->success_massage));

							$Notice = str_replace("{refrenceID}", $refrenceID, $Notice);


							$Notice = apply_filters('WC_IranKish5_Return_from_Gateway_Success_Notice', $Notice, $order_id, $refrenceID);
							if ($Notice)
								wc_add_notice($Notice, 'success');


							do_action('WC_IranKish5_Return_from_Gateway_Success', $order_id, $refrenceID);

							wp_redirect(add_query_arg('wc_status', 'success', $this->get_return_url($order)));
							exit;
						} elseif ($status == 'cancelled') {
							$action = $this->author;
							do_action('WC_Gateway_Payment_Actions', $action);

							$tr_id = ($refrenceID && $refrenceID != 0) ? ('<br/>کد رهگیری (کد مرجع تراکنش) : ' . $refrenceID) : '';

							$Note = sprintf(__('کاربر در حین تراکنش از پرداخت انصراف داد . %s', 'woocommerce'), $tr_id);
							$Note = apply_filters('WC_IranKish5_Return_from_Gateway_Cancelled_Note', $Note, $order_id, $refrenceID);
							if ($Note)
								$order->add_order_note($Note, 1);


							$Notice =  wpautop(wptexturize($this->cancelled_massage));

							$Notice = str_replace("{refrenceID}", $refrenceID, $Notice);

							$Notice = apply_filters('WC_IranKish5_Return_from_Gateway_Cancelled_Notice', $Notice, $order_id, $refrenceID);
							if ($Notice)
								wc_add_notice($Notice, 'error');

							do_action('WC_IranKish5_Return_from_Gateway_Cancelled', $order_id, $refrenceID);

							wp_redirect($woocommerce->cart->get_checkout_url());
							exit;
						} else {
							$action = $this->author;
							do_action('WC_Gateway_Payment_Actions', $action);

							$tr_id = ($refrenceID && $refrenceID != 0) ? ('<br/>کد رهگیری (کد مرجع تراکنش) : ' . $refrenceID) : '';

							$Note = sprintf(__('خطا در هنگام بازگشت از بانک : %s %s', 'woocommerce'), $this->Fault_IranKish5($fault), $tr_id);
							$Note = apply_filters('WC_IranKish5_Return_from_Gateway_Failed_Note', $Note, $order_id, $refrenceID, $fault);
							if ($Note)
								$order->add_order_note($Note, 1);


							$Notice = wpautop(wptexturize($this->failed_massage));


							$Notice = str_replace("{refrenceID}", $refrenceID, $Notice);


							$Notice = str_replace("{fault}", $this->Fault_IranKish5($fault), $Notice);
							$Notice = apply_filters('WC_IranKish5_Return_from_Gateway_Failed_Notice', $Notice, $order_id, $refrenceID, $fault);
							if ($Notice)
								wc_add_notice($Notice, 'error');



							do_action('WC_IranKish5_Return_from_Gateway_Failed', $order_id, $refrenceID, $fault);

							wp_redirect($woocommerce->cart->get_checkout_url());
							exit;
						}
					} else {

						$action = $this->author;
						do_action('WC_Gateway_Payment_Actions', $action);

						$refrenceID = get_post_meta($order_id, '_refrenceID', true);

						$Notice = wpautop(wptexturize($this->success_massage));

						$Notice = str_replace("{refrenceID}", $refrenceID, $Notice);

						$Notice = apply_filters('WC_IranKish5_Return_from_Gateway_ReSuccess_Notice', $Notice, $order_id, $refrenceID, $refrenceID);
						if ($Notice)
							wc_add_notice($Notice, 'success');


						do_action('WC_IranKish5_Return_from_Gateway_ReSuccess', $order_id, $refrenceID);

						wp_redirect(add_query_arg('wc_status', 'success', $this->get_return_url($order)));
						exit;
					}
				} else {
					$refrenceID = sanitize_text_field($_POST['retrievalReferenceNumber']);
					$action = $this->author;
					do_action('WC_Gateway_Payment_Actions', $action);

					$fault = 'شماره سفارش وجود ندارد .';
					$Notice = wpautop(wptexturize($this->failed_massage));
					$Notice = str_replace("{fault}", $fault, $Notice);
					$Notice = apply_filters('WC_IranKish5_Return_from_Gateway_No_Order_ID_Notice', $Notice, $order_id, $fault);
					if ($Notice)
						wc_add_notice($Notice, 'error');

					do_action('WC_IranKish5_Return_from_Gateway_No_Order_ID', $order_id, $refrenceID, $fault);

					wp_redirect($woocommerce->cart->get_checkout_url());
					exit;
				}
			}

			private static function Fault_IranKish5($err_code)
			{

				$msgArray = array(
					'5' => '‫از‬ ‫انجام‬ ‫تراکنش‬ ‫صرف‬ ‫نظر‬ ‫شد‬',
					'3' => '‫پذیرنده‬ ‫فروشگاهی‬ ‫نا‬ ‫معتبر‬ ‫است‬',
					'64' => '‫مبلغ‬ ‫تراکنش‬ ‫نادرست‬ ‫است‪،‬جمع‬ ‫مبالغ‬ ‫تقسیم‬ ‫وجوه‬ ‫برابر‬ ‫مبلغ‬ ‫کل‬ ‫تراکنش‬ ‫نمی‬ ‫باشد‬',
					'94' => '‫تراکنش‬ ‫تکراری‬ ‫است‬',
					'25' => '‫تراکنش‬ ‫اصلی‬ ‫یافت‬ ‫نشد‬',
					'77' => '‫روز‬ ‫مالی‬ ‫تراکنش‬ ‫نا‬ ‫معتبر‬ ‫است‬',
					'63' => '‫تمهیدات‬ ‫امنیتی‬ ‫نقض‬ ‫گردیده‬ ‫است‬',
					'97' => '‫کد‬ ‫تولید‬ ‫کد‬ ‫اعتبار‬ ‫سنجی‬ ‫نا‬ ‫معتبر‬ ‫است‬',
					'30' => '‫فرمت‬ ‫پیام‬ ‫نادرست‬ ‫است‬',
					'86' => '‫شتاب‬ ‫در‬ ‫حال‬ ‫‪Off‬‬ ‫‪Sign‬‬ ‫است‬',
					'55' => '‫رمز‬ ‫کارت‬ ‫نادرست‬ ‫است‬',
					'40' => '‫عمل‬ ‫درخواستی‬ ‫پشتیبانی‬ ‫نمی‬ ‫شود‬',
					'57' => '‫انجام‬ ‫تراکنش‬ ‫مورد‬ ‫درخواست‬ ‫توسط‬ ‫پایانه‬ ‫انجام‬ ‫دهنده‬ ‫مجاز‬ ‫نمی‬ ‫باشد‬',
					'58' => '‫انجام‬ ‫تراکنش‬ ‫مورد‬ ‫درخواست‬ ‫توسط‬ ‫پایانه‬ ‫انجام‬ ‫دهنده‬ ‫مجاز‬ ‫نمی‬ ‫باشد‬',
					'96' => '‫قوانین‬ ‫سامانه‬ ‫نقض‬ ‫گردیده‬ ‫است‬ ‫‪،‬‬ ‫خطای‬ ‫داخلی‬ ‫سامانه‬',
					'2' => '‫تراکنش‬ ‫قبال‬ ‫برگشت‬ ‫شده‬ ‫است‬',
					'54' => '‫تاریخ‬ ‫انقضا‬ ‫کارت‬ ‫سررسید‬ ‫شده‬ ‫است‬',
					'62' => '‫کارت‬ ‫محدود‬ ‫شده‬ ‫است‬',
					'75' => '‫تعداد‬ ‫دفعات‬ ‫ورود‬ ‫رمز‬ ‫اشتباه‬ ‫از‬ ‫حد‬ ‫مجاز‬ ‫فراتر‬ ‫رفته‬ ‫است‬',
					'14' => '‫اطالعات‬ ‫کارت‬ ‫صحیح‬ ‫نمی‬ ‫باشد‬',
					'51' => '‫موجودی‬ ‫حساب‬ ‫کافی‬ ‫نمی‬ ‫باشد‬',
					'56' => '‫اطالعات‬ ‫کارت‬ ‫یافت‬ ‫نشد‬',
					'61' => '‫مبلغ‬ ‫تراکنش‬ ‫بیش‬ ‫از‬ ‫حد‬ ‫مجاز‬ ‫است‬',
					'65' => '‫تعداد‬ ‫دفعات‬ ‫انجام‬ ‫تراکنش‬ ‫بیش‬ ‫از‬ ‫حد‬ ‫مجاز‬ ‫است‬',
					'78' => '‫کارت‬ ‫فعال‬ ‫نیست‬',
					'79' => '‫حساب‬ ‫متصل‬ ‫به‬ ‫کارت‬ ‫بسته‬ ‫یا‬ ‫دارای‬ ‫اشکال‬ ‫است‬',
					'42' => '‫کارت‬ ‫یا‬ ‫حساب‬ ‫مقصد‬ ‫در‬ ‫وضعیت‬ ‫پذیرش‬ ‫نمی‬ ‫باشد‬',
					'901' => '‫درخواست‬ ‫نا‬ ‫معتبر‬ ‫است‬ ‫(‬ ‫‪Tokenization‬‬ ‫)‬',
					'902' => '‫پارامترهای‬ ‫اضافی‬ ‫درخواست‬ ‫نامعتبر‬ ‫می‬ ‫باشد		‬ ‫(‬ ‫‪Tokenization‬‬ ‫)‬',
					'903' => '‫شناسه‬ ‫پرداخت‬ ‫نامعتبر‬ ‫می‬ ‫باشد‬ ‫(‬ ‫‪Tokenization‬‬ ‫)‬',
					'904' => '‫اطالعات‬ ‫مرتبط‬ ‫با‬ ‫قبض‬ ‫نا‬ ‫معتبر‬ ‫می‬ ‫باشد‬ ‫(‬ ‫‪Tokenization‬‬ ‫)‬',
					'905' => '‫شناسه‬ ‫درخواست‬ ‫نامعتبر‬ ‫می‬ ‫باشد‬ ‫(‬ ‫‪Tokenization‬‬ ‫)‬',
					'906' => '‫درخواست‬ ‫تاریخ‬ ‫گذشته‬ ‫است‬ ‫(‬ ‫‪Tokenization‬‬ ‫)‬',
					'907' => '‫آدرس‬ ‫بازگشت‬ ‫نتیجه‬ ‫پرداخت‬ ‫نامعتبر‬ ‫می‬ ‫باشد‬ ‫(‬ ‫‪Tokenization‬‬ ‫)‬',
					'909' => '‫پذیرنده‬ ‫نامعتبر‬ ‫می‬ ‫باشد(‬ ‫‪Tokenization‬‬ ‫)‬',
					'910' => '‫پارامترهای‬ ‫مورد‬ ‫انتظار‬ ‫پرداخت‬ ‫تسهیمی‬ ‫تامین‬ ‫نگردیده‬ ‫است(‬ ‫‪Tokenization‬‬ ‫)‬',
					'911' => '‫پارامترهای‬ ‫مورد‬ ‫انتظار‬ ‫پرداخت‬ ‫تسهیمی‬ ‫نا‬ ‫معتبر‬ ‫یا‬ ‫دارای‬ ‫اشکال‬ ‫می‬ ‫باشد(‬ ‫‪Tokenization‬‬ ‫)‬',
					'912' => '‫تراکنش‬ ‫درخواستی‬ ‫برای‬ ‫پذیرنده‬ ‫فعال‬ ‫نیست‬ ‫(‬ ‫‪Tokenization‬‬ ‫)‬',
					'913' => '‫تراکنش‬ ‫تسهیم‬ ‫برای‬ ‫پذیرنده‬ ‫فعال‬ ‫نیست‬ ‫(‬ ‫‪Tokenization‬‬ ‫)‬',
					'914' => '‫آدرس‬ ‫آی‬ ‫پی‬ ‫دریافتی‬ ‫درخواست‬ ‫نا‬ ‫معتبر‬ ‫می‬ ‫باشد‬',
					'915' => '‫شماره‬ ‫پایانه‬ ‫نامعتبر‬ ‫می‬ ‫باشد‬ ‫(‬ ‫‪Tokenization‬‬ ‫)‬',
					'916' => '‫شماره‬ ‫پذیرنده‬ ‫نا‬ ‫معتبر‬ ‫می‬ ‫باشد‬ ‫(‬ ‫‪Tokenization‬‬ ‫)‬',
					'917' => '‫نوع‬ ‫تراکنش‬ ‫اعالم‬ ‫شده‬ ‫در‬ ‫خواست‬ ‫نا‬ ‫معتبر‬ ‫می‬ ‫باشد‬ ‫(‬ ‫‪Tokenization‬‬ ‫)‬',
					'918' => '‫پذیرنده‬ ‫فعال‬ ‫نیست‬',
					'919' => '‫مبالغ‬ ‫تسهیمی‬ ‫ارائه‬ ‫شده‬ ‫با‬ ‫توجه‬ ‫به‬ ‫قوانین‬ ‫حاکم‬ ‫بر‬ ‫وضعیت‬ ‫تسهیم‬ ‫پذیرنده‬ ‫‪،‬‬ ‫نا‬ ‫معتبر‬ ‫است‬ ‫(‬ ‫‪Tokenization‬‬ ‫)‬',
					'920' => '‫شناسه‬ ‫نشانه‬ ‫نامعتبر‬ ‫می‬ ‫باشد‬',
					'921' => '‫شناسه‬ ‫نشانه‬ ‫نامعتبر‬ ‫و‬ ‫یا‬ ‫منقضی‬ ‫شده‬ ‫است‬',
					'922' => '‫نقض‬ ‫امنیت‬ ‫درخواست‬ ‫(‬ ‫‪Tokenization‬‬ ‫)‬',
					'923' => '‫ارسال‬ ‫شناسه‬ ‫پرداخت‬ ‫در‬ ‫تراکنش‬ ‫قبض‬ ‫مجاز‬ ‫نیست‬ ‫(‬ ‫‪Tokenization‬‬ ‫)‬',
					'925' => '‫مبلغ‬ ‫مبادله‬ ‫شده‬ ‫نا‬ ‫معتبر‬ ‫می‬ ‫باشد‬',
				);

				if (isset($msgArray[$err_code])) {
					return $msgArray[$err_code];
				}
				return 'در حین پرداخت خطای سیستمی رخ داده است .' . $err_code;
			}
		}
	}
}
add_action('plugins_loaded', 'Load_IranKish5_Gateway', 0);
