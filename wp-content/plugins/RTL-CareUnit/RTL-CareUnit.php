<?php defined('ABSPATH') || die;
/**
 * RTL Care Unit Plugin
 *
 * @package           RTL-CareUnit
 * @author            RTL-Theme
 * @copyright         RTL-Theme
 * @license           RTL-Theme
 *
 * @wordpress-plugin
 *
 * Plugin Name:         واحد مراقبت راستچین
 * Plugin URI:          https://www.rtl-theme.com/
 * Description:         افزونه ای جهت فعالسازی، بروزرسانی و مراقبت از محصولات وردپرس راستچین
 * Version:             1.0.6
 * Requires at least:   5.0
 * Requires PHP:        7.2
 * Author:              RTL-Theme
 * Author URI:          https://www.rtl-theme.com/
 * License:             RTL-Theme License
 * License URI:         https://www.rtl-theme.com/
 * Text Domain:         rtltheme
 * Domain Path:         /languages
 */

// Set Max Execution Time
set_time_limit(1200);
ini_set('max_execution_time', '1200');

// Set Encoding To UTF-8
mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');

// Set Direct Download Method
add_filter('filesystem_method', function ($method) {
	return 'direct';
});

// Change WP Directory Permissions
defined('FS_CHMOD_DIR') || define('FS_CHMOD_DIR', 0755);

// Bootstrap
require_once __DIR__ . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

