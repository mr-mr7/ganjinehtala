jQuery(document).ready(function ($) {
    /**
     * increase jQuery Ajax Timeout
     */
    $.ajaxSetup({
        timeout: 120000
    });

    /**
     * Handle Ping Button Ajax
     */
    $(document).on('click', '.RCU-Notification.is-dismissible .notice-dismiss', function () {
        $.ajax({
            method: "POST",
            url   : RCU.URL,
            data  : {
                'action': 'rcuAjaxDismissNotification',
                'nonce' : RCU.Nonce,
                'id'    : $(this).parent().attr('data-id')
            }
        });
    });
});