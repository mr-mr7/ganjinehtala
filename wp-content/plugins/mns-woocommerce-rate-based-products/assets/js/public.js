jQuery(document).ready(function ($) {

	const { __, _x, _n, sprintf } = wp.i18n;

	function mnswmcRunPriceCalculator(thisEl) {

		var thisSection = thisEl.parents('.mnswmc-price-calculator'),
			unique = thisSection.data('unique'),
			rate = parseFloat(thisSection.find('#mnswmc_calculator_currency_id_' + unique).find(':selected').data('rate')),
			round = parseFloat(thisSection.find('#mnswmc_calculator_price_rounding_' + unique).find(':selected').val()),
			roundType = thisSection.find('#mnswmc_calculator_rounding_type_' + unique).find(':selected').val(),
			roundSide = thisSection.find('#mnswmc_calculator_rounding_side_' + unique).find(':selected').val(),
			profitValue = parseFloat(thisSection.find('#mnswmc_calculator_profit_margin_' + unique).val()),
			profitType = thisSection.find('#mnswmc_calculator_profit_type_' + unique).find(':selected').val(),
			price = parseFloat(thisSection.find('#mnswmc_calculator_regular_price_' + unique).val()),
			salePrice = parseFloat(thisSection.find('#mnswmc_calculator_sale_price_' + unique).val());

		if (thisSection.find('#mnswmc_calculator_currency_id_' + unique).length <= 0)
			return;

		if (thisSection.find('#mnswmc_calculator_profit_margin_' + unique).val().length <= 0)
			profitValue = 0;

		if (thisSection.find('#mnswmc_calculator_sale_price_' + unique).val().length > 0 && salePrice >= price) {
			thisSection.find('#mnswmc_calculator_sale_price_' + unique).on('focusout', function () {
				$(this).val('');
				thisSection.find('.mnswmc_sale_price_preview').find('code').text('0');
				thisSection.find('.show_if_mnswmc_sale_price_filled').fadeOut('fast');
			});
		} else {
			thisSection.find('#mnswmc_calculator_sale_price_' + unique).unbind('focusout');
		}

		if (rate != '0') {
			thisSection.find('.mnswmc_currency_rate_preview').find('code').text(mnswmcFormatNumbers(rate));
			thisSection.find('.show_if_mnswmc_currency_selected').fadeIn('fast');

			thisSection.find('.mnswmc_calculator_regular_price').find('.description').text(thisSection.find('#mnswmc_calculator_currency_id_' + unique).find(':selected').text());
			thisSection.find('.mnswmc_calculator_sale_price').find('.description').text(thisSection.find('#mnswmc_calculator_currency_id_' + unique).find(':selected').text());

		} else {
			thisSection.find('.mnswmc_currency_rate_preview').find('code').text('0');
			thisSection.find('.show_if_mnswmc_currency_selected').fadeOut('fast');

			thisSection.find('.mnswmc_calculator_regular_price').find('.description').text('');
			thisSection.find('.mnswmc_calculator_sale_price').find('.description').text('');

			thisSection.find('.show_if_mnswmc_regular_price_filled').fadeOut('fast');
			thisSection.find('.show_if_mnswmc_sale_price_filled').fadeOut('fast');

			return;
		}

		if (thisSection.find('#mnswmc_calculator_regular_price_' + unique).val().length > 0) {

			var finalPrice = mnswmcGenerateRateBasedPrice(price, rate, round, roundType, roundSide, profitValue, profitType);
			thisSection.find('.mnswmc_regular_price_preview').find('code').text(mnswmcFormatNumbers(finalPrice));
			thisSection.find('.show_if_mnswmc_regular_price_filled').fadeIn('fast');

		} else {
			thisSection.find('.mnswmc_regular_price_preview').find('code').text('0');
			thisSection.find('.show_if_mnswmc_regular_price_filled').fadeOut('fast');
		}

		if (thisSection.find('#mnswmc_calculator_sale_price_' + unique).val().length > 0) {

			var finalPrice = mnswmcGenerateRateBasedPrice(salePrice, rate, round, roundType, roundSide, profitValue, profitType);
			thisSection.find('.mnswmc_sale_price_preview').find('code').text(mnswmcFormatNumbers(finalPrice));
			thisSection.find('.show_if_mnswmc_sale_price_filled').fadeIn('fast');

		} else {
			thisSection.find('.mnswmc_sale_price_preview').find('code').text('0');
			thisSection.find('.show_if_mnswmc_sale_price_filled').fadeOut('fast');
		}

	}

	function mnswmcRunRateConverter(container, actionSide) {

		var fromSelect = container.find('.converter-from').find('select'),
			toSelect = container.find('.converter-to').find('select'),
			fromRate = parseFloat(fromSelect.val()),
			toRate = parseFloat(toSelect.val()),
			fromInput = container.find('.converter-from').find('input'),
			toInput = container.find('.converter-to').find('input'),
			fromValue = parseFloat(fromInput.val().replace(/\,/g, '')),
			toValue = parseFloat(toInput.val().replace(/\,/g, ''));

		if (actionSide == 'from') {

			var result = (fromValue * fromRate) / toRate;
			toInput.val(mnswmcFormatNumbers(result, mnswmcDynamicDecimal(result), '.', ','));

		} else if (actionSide == 'to') {

			var result = (toValue * toRate) / fromRate;
			fromInput.val(mnswmcFormatNumbers(result, mnswmcDynamicDecimal(result), '.', ','));

		}
	}

	$('.mnswmc-currency-chart-meta-box').each(function () {

		var chartContainer = $(this),
			sybmol = chartContainer.data('symbol'),
			chartID = chartContainer.find('.mnswmc-currency-chart').attr('id'),
			labels = JSON.parse(chartContainer.find('.mnswmc-currency-chart-labels').val()),
			values = JSON.parse(chartContainer.find('.mnswmc-currency-chart-values').val()),
			rates = JSON.parse(chartContainer.find('.mnswmc-currency-chart-rates').val());

		mnswmcLoadCurrencyChart(chartContainer, chartID, labels, values, rates, sybmol);
	});

	$('.mnswmc-price-calculator').each(function () {
		mnswmcRunPriceCalculator($(this).find('.mnswmc_calculator_field'));
	});

	$(document).on('change input keyup click paste', '.mnswmc_calculator_field', function (e) {
		e.stopImmediatePropagation();
		mnswmcRunPriceCalculator($(this));
	});

	$('.mnswmc-rate-converter').each(function () {
		mnswmcRunRateConverter($(this), 'from');
	});

	$(document).on('change input keyup click paste', '.mnswmc-rate-converter-field', function (e) {

		e.stopImmediatePropagation();

		var actionSide = $(this).parents('.mnswmc-converter-side').hasClass('converter-from') ? 'from' : 'to';
		mnswmcRunRateConverter($(this).parents('.mnswmc-rate-converter'), actionSide);
	});
});