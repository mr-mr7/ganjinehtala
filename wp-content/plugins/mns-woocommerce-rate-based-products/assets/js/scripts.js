jQuery(document).ready(function ($) {

	const { __, _x, _n, sprintf } = wp.i18n;

	function mnswmcPrepareFormula(formulaContainer) {

		var formula = formulaContainer.find('.mnswmc-formula-formul').val();
		formula = formula.trim();

		var formulaRegular = formula,
			formulaSale = formula;

		var regex = /{[a-zA-Z0-9-]+}/g,
			matches = formula.match(regex);

		if (matches == null)
			return formula = [formulaRegular, formulaSale];

		matches.forEach(codeToValue);

		function codeToValue(item, index) {
			var varID = item.replace(/{|}/g, ''),
				varElem = formulaContainer.find('.variable-' + varID),
				varRate = varElem.hasClass('custom-variable') ? parseFloat(varElem.find('.variable-rate input').val()) : parseFloat(varElem.data('rate')),
				varValue = parseFloat(varElem.find('.variable-unit input').val()),
				varSaleValue = parseFloat(varElem.find('.variable-sale input').val());

			if (!$.isNumeric(varRate))
				varRate = 1;

			/*if (!$.isNumeric(varValue))
				varValue = 0;*/

			if (!$.isNumeric(varSaleValue))
				varSaleValue = varValue;

			formulaRegular = formulaRegular.replace(item, (varRate * varValue));
			formulaSale = formulaSale.replace(item, (varRate * varSaleValue));
		}

		return formula = [formulaRegular, formulaSale];
	}

	function mnswmcRunFormula(formulaContainer, round = 0, roundType = 'zero', roundSide = 'close') {

		var formula = mnswmcPrepareFormula(formulaContainer),
			formulaValidation = formulaContainer.find('.mnswmc-formula-validation'),
			formulaPreview = formulaContainer.find('.mnswmc-formula-result'),
			formulaSalePreview = formulaContainer.find('.mnswmc-formula-sale-result');

		var parser = new formulaParser.Parser();
		var parserRegular = parser.parse(formula[0]),
			parserSale = parser.parse(formula[1]),
			resultRegular = mnswmcGenerateRateBasedPrice(parserRegular.result, 1, round, roundType, roundSide, 0, 'fixed'),
			resultSale = mnswmcGenerateRateBasedPrice(parserSale.result, 1, round, roundType, roundSide, 0, 'fixed');

		if (parserRegular.error == null && parserSale.error == null && formula[0].length > 0 && formula[1].length > 0) {

			if (resultRegular < 0)
				resultRegular = 0;

			if (resultSale < 0)
				resultSale = 0;

			if (formulaValidation.hasClass('valid')) {
				formulaPreview.find('code').text(mnswmcFormatNumbers(resultRegular));

				if (resultSale >= resultRegular) {
					formulaSalePreview.fadeOut('fast', function () {
						$(this).find('code').text('0');
					});
				} else {
					formulaSalePreview.find('code').text(mnswmcFormatNumbers(resultSale));
					formulaSalePreview.fadeIn('fast');
				}
			} else {
				formulaValidation.fadeOut('fast', function () {
					$(this).addClass('valid').removeClass('error');
					$(this).find('.dashicons').addClass('dashicons-yes').removeClass('dashicons-no-alt');
					$(this).find('.log').text($(this).find('.log').data('valid'));
					$(this).fadeIn('fast');
				});
				formulaPreview.find('code').text(mnswmcFormatNumbers(resultRegular));
				formulaPreview.fadeIn('fast');

				if (resultSale >= resultRegular) {
					formulaSalePreview.fadeOut('fast', function () {
						$(this).find('code').text('0');
					});
				} else {
					formulaSalePreview.find('code').text(mnswmcFormatNumbers(resultSale));
					formulaSalePreview.fadeIn('fast');
				}
			}
		} else {

			if (formulaValidation.hasClass('error')) {

				if (formula[0].length > 0 && formula[1].length > 0) {
					formulaValidation.find('.log').text(formulaValidation.find('.log').data('error'));
				} else {
					formulaValidation.find('.log').text(formulaValidation.find('.log').data('empty'));
				}
			} else {

				formulaValidation.fadeOut('fast', function () {
					$(this).addClass('error').removeClass('valid');
					$(this).find('.dashicons').addClass('dashicons-no-alt').removeClass('dashicons-yes');
					if (formula[0].length > 0 && formula[1].length > 0) {
						$(this).find('.log').text($(this).find('.log').data('error'));
					} else {
						$(this).find('.log').text($(this).find('.log').data('empty'));
					}
					$(this).fadeIn('fast');
				});
				formulaPreview.fadeOut('fast', function () {
					$(this).find('code').text('0');
				});
				formulaSalePreview.fadeOut('fast', function () {
					$(this).find('code').text('0');
				});
			}
		}
	}

	function mnswmcAddFormulVariables(formulaContainer, custom = false) {

		var selected = formulaContainer.find('.mnswmc-formula-add-currency-variable').find('option:selected'),
			elementCounter = parseFloat(formulaContainer.find('.mnswmc-formula-variables-counter').val()),
			elementRow = formulaContainer.find('.variable-placeholder').clone(),
			container = formulaContainer.find('.mnswmc-formula-variables-rows');

		if (selected.val() == '0' && !custom)
			return;

		if (custom) {

			elementRow.addClass('custom-variable');
			elementRow.data('rate', '');
			elementRow.find('.variable-name input').attr('name', elementRow.data('field-name') + '[' + selected.val() + '-' + elementCounter + '][name]');
			elementRow.find('.variable-rate input').attr('name', elementRow.data('field-name') + '[' + selected.val() + '-' + elementCounter + '][rate]');

		} else {

			elementRow.addClass('currency-variable');
			elementRow.data('rate', selected.data('rate'));
			elementRow.find('.variable-name').children('span').text(selected.text());
			elementRow.find('.variable-rate').children('span').html('<code>' + mnswmcFormatNumbers(selected.data('rate')) + '</code> ' + elementRow.data('symbol'));
		}

		elementRow.find('.variable-code > span > code').text('{' + selected.val() + '-' + elementCounter + '}');
		elementRow.find('.variable-unit input').attr('name', elementRow.data('field-name') + '[' + selected.val() + '-' + elementCounter + '][value]');
		elementRow.find('.variable-sale input').attr('name', elementRow.data('field-name') + '[' + selected.val() + '-' + elementCounter + '][sale]');
		elementRow.addClass('variable-' + selected.val() + '-' + elementCounter);

		elementCounter++;
		formulaContainer.find('.mnswmc-formula-variables-counter').val(elementCounter);

		elementRow.removeClass('variable-placeholder').appendTo(container).fadeIn('slow');

		formulaContainer.find('.mnswmc-formula-add-currency-variable').val('0');

		return;
	}

	function mnswmcLoadFormulVariables(formulaContainer, loadID = 0) {

		/*if (formulaContainer.find('.mnswmc-formula-data-placeholder').data('type') == 'product' && loadID == 0) {

			var deprecatedMessage = '<span class="formulaDeprecatedMessage error">' + __('Custom formula is deprecated and will be completely eliminated in the future. Therefore, it is recommended to define a formula for your products.', 'mnswmc-js') + '</span>';
			formulaContainer.find('.mnswmc-formula-variables-rows').before(deprecatedMessage);
		} else {
			formulaContainer.find('.formulaDeprecatedMessage').remove();
		}*/

		var container = formulaContainer.find('.mnswmc-formula-variables-rows'),
			variables = formulaContainer.find('.mnswmc-formula-data-placeholder').val(),
			appendDelay = 0;

		variables = JSON.parse(variables);

		if (!variables)
			return;

		$.each(variables[loadID].variables, function (code, variable) {

			var elementRow = formulaContainer.find('.variable-placeholder').clone();

			if (variable.type == 'custom') {

				if (loadID != 0) {

					elementRow.addClass('currency-variable');
					elementRow.data('rate', variable.rate);
					elementRow.find('.variable-name').children('span').text(variable.name);
					elementRow.find('.variable-rate').children('span').html('<code>' + mnswmcFormatNumbers(variable.rate) + '</code> ' + elementRow.data('symbol'));

				} else {

					elementRow.addClass('custom-variable');
					elementRow.data('rate', '');
					elementRow.find('.variable-name input').attr('name', elementRow.data('field-name') + '[' + code + '][name]').val(variable.name);
					elementRow.find('.variable-rate input').attr('name', elementRow.data('field-name') + '[' + code + '][rate]').val(variable.rate);
				}

			} else {

				var regex = /^\d+/g,
					matches = code.match(regex),
					variableID = matches[0],
					variableRate = formulaContainer.find('.mnswmc-formula-add-currency-variable').find('option[value=' + variableID + ']').data('rate'),
					variableName = formulaContainer.find('.mnswmc-formula-add-currency-variable').find('option[value=' + variableID + ']').text();

				if (variableID == 0)
					return;

				elementRow.addClass('currency-variable');
				elementRow.data('rate', variableRate);
				elementRow.find('.variable-name').children('span').text(variableName);
				elementRow.find('.variable-rate').children('span').html('<code>' + mnswmcFormatNumbers(variableRate) + '</code> ' + elementRow.data('symbol'));
			}

			elementRow.addClass('variable-' + code);
			elementRow.find('.variable-code > span > code').text('{' + code + '}');

			if (loadID != 0 && code in variables[0].variables) {

				variable.value = variables[0].variables[code].value;
				variable.sale = variables[0].variables[code].sale;
			}

			elementRow.find('.variable-unit input').attr('name', elementRow.data('field-name') + '[' + code + '][value]').val(variable.value);
			elementRow.find('.variable-sale input').attr('name', elementRow.data('field-name') + '[' + code + '][sale]').val(variable.sale);

			if (loadID != 0) {
				elementRow.find('.variable-actions').remove();
			}

			elementRow.removeClass('variable-placeholder').appendTo(container).delay(appendDelay).fadeIn('slow');
			appendDelay = appendDelay + 100;
		});

		return;
	}

	function mnswmcSwitchFormula(formulaContainer, loadID) {

		var custom = (loadID == 0) ? true : false;

		// decrepted custom formula
		var decErrorElement = $('<span class="mnswmcCustomFormulaErrorEl error">' + __('Custom formula is deprecated and will be completely eliminated in the future. It is recommended that you do not use this formulation method and correct any products that have already been formulated in this way. Alternative methods are pre-defined formulas that work much better. So first create your formula from Forumlas and then use it in your products.', 'mnswmc-js') + '</span>');

		if (custom) {
			formulaContainer.prepend(decErrorElement);
		} else {
			formulaContainer.find('.mnswmcCustomFormulaErrorEl').remove();
		}

		var formula = formulaContainer.find('.mnswmc-formula-data-placeholder').val(),
			formula = JSON.parse(formula),
			formula = formula[loadID];

		if (!formula)
			return;

		formulaContainer.find('.mnswmc-formula-formul').val(formula.formul);

		if (custom) {
			formulaContainer.find('.mnswmc-formula-variables-new').fadeIn('fast');
			formulaContainer.find('.mnswmc-formula-formul').prop('readonly', false);
		} else {
			formulaContainer.find('.mnswmc-formula-variables-new').fadeOut('fast');
			formulaContainer.find('.mnswmc-formula-formul').prop('readonly', true);
		}

		formulaContainer.find('.mnswmc-formula-variables-rows').empty();
		mnswmcLoadFormulVariables(formulaContainer, loadID);

		formulaContainer.find('.mnswmc-formula-variables-counter').val(formula.variables_counter);
	}

	function mnswmcFormulaInit(formulaContainer, round, roundType, roundSide) {

		mnswmcRunFormula(formulaContainer, round, roundType, roundSide);

		formulaContainer.on('change input keyup click paste', '.mnswmc_formula_field', function (e) {

			e.stopImmediatePropagation();
			mnswmcRunFormula(formulaContainer, round, roundType, roundSide);

			$(this).parents('.woocommerce_variation').addClass('variation-needs-update');
			$('button.cancel-variation-changes, button.save-variation-changes').removeAttr('disabled');
			$('#variable_product_options').trigger('woocommerce_variations_input_changed');
		});

		formulaContainer.on('change', '.mnswmc-formula-add-currency-variable', function (e) {

			e.stopImmediatePropagation();
			mnswmcAddFormulVariables(formulaContainer, false);
		});

		formulaContainer.on('click', '.mnswmc-formula-add-custom-variable', function (e) {

			e.stopImmediatePropagation();
			mnswmcAddFormulVariables(formulaContainer, true);
		});

		formulaContainer.on('click', '.var-remove', function (e) {

			e.stopImmediatePropagation();
			$(this).parents('.variable').fadeOut('fast', function () {
				$(this).remove();
				mnswmcRunFormula(formulaContainer, round, roundType, roundSide);
			});
			return;
		});
	}

	function mnswmcProductPriceCalc(thisSection, postType = 'product') {

		if (postType == 'variation') {

			var thisLoop = thisSection.data('loop'),
				idPrefix = '#variable',
				idSuffix = '_' + thisLoop,
				classPrefix = '.variable',
				classSuffix = idSuffix + '_field';
		} else {

			var thisLoop = false,
				idPrefix = '#',
				idSuffix = '',
				classPrefix = '.',
				classSuffix = '_field';
		}

		var activeEl = thisSection.find(idPrefix + '_mnswmc_active' + idSuffix),
			currencyEl = thisSection.find(idPrefix + '_mnswmc_currency_id' + idSuffix),
			roundValueEl = thisSection.find(idPrefix + '_mnswmc_price_rounding' + idSuffix),
			roundTypeEl = thisSection.find(idPrefix + '_mnswmc_rounding_type' + idSuffix),
			roundSideEl = thisSection.find(idPrefix + '_mnswmc_rounding_side' + idSuffix),
			dependenceEl = thisSection.find(idPrefix + '_mnswmc_dependence_type' + idSuffix),
			profitValueEl = thisSection.find(idPrefix + '_mnswmc_profit_margin' + idSuffix),
			profitTypeEl = thisSection.find(idPrefix + '_mnswmc_profit_type' + idSuffix),
			priceEl = thisSection.find(idPrefix + '_mnswmc_regular_price' + idSuffix),
			salePriceEl = thisSection.find(idPrefix + '_mnswmc_sale_price' + idSuffix);

		if (!activeEl.is(':checked')) {

			if (postType == 'product') {

				thisSection.siblings('.pricing').find('#_regular_price').prop('readonly', false);
				thisSection.siblings('.pricing').find('#_sale_price').prop('readonly', false);

			} else if (postType == 'variation') {

				thisSection.parent().find('#variable_regular_price_' + thisLoop).prop('readonly', false);
				thisSection.parent().find('#variable_sale_price' + thisLoop).prop('readonly', false);
			}

			thisSection.find('.show_if_mnswmc_active').fadeOut('fast');
			thisSection.find('.show_if_mnswmc_advanced_dependence').fadeOut('fast');
			thisSection.find('.show_if_mnswmc_simple_dependence').fadeOut('fast');
			thisSection.find('.show_if_mnswmc_currency_selected').fadeOut('fast');
			thisSection.find('.show_if_mnswmc_regular_price_filled').fadeOut('fast');
			thisSection.find('.show_if_mnswmc_sale_price_filled').fadeOut('fast');

			return;
		}

		if (postType == 'product') {

			thisSection.siblings('.pricing').find('#_regular_price').prop('readonly', true).val();
			thisSection.siblings('.pricing').find('#_sale_price').prop('readonly', true).val();

		} else if (postType == 'variation') {

			thisSection.parent().find('#variable_regular_price_' + thisLoop).prop('readonly', true).val();
			thisSection.parent().find('#variable_sale_price' + thisLoop).prop('readonly', true).val();
		}

		thisSection.find('.show_if_mnswmc_active').fadeIn('fast');

		var rate = parseFloat(currencyEl.find(':selected').data('rate')),
			round = parseFloat(roundValueEl.find(':selected').val()),
			roundType = roundTypeEl.find(':selected').val(),
			roundSide = roundSideEl.find(':selected').val(),
			defaultRound = parseFloat(roundValueEl.data('default')),
			defaultRoundType = roundTypeEl.data('default'),
			defaultRoundSide = roundSideEl.data('default'),
			dependence = dependenceEl.find(':selected').val(),
			profitValue = parseFloat(profitValueEl.val()),
			profitType = profitTypeEl.find(':selected').val(),
			price = parseFloat(priceEl.val()),
			salePrice = parseFloat(salePriceEl.val());

		if (round == '0' && defaultRound != '0') {
			round = defaultRound;
			roundType = defaultRoundType;
			roundSide = defaultRoundSide;
		}

		if (dependenceEl.find(':selected').val().length <= 0 || dependence != 'advanced') {

			if (postType == 'variation')
				thisSection.find(classPrefix + '_mnswmc_price_rounding' + classSuffix).removeClass('form-row-full').addClass('form-row-last');

			roundValueEl.removeClass('mnswmc_formul_field');
			roundTypeEl.removeClass('mnswmc_formul_field');

			thisSection.find('.show_if_mnswmc_advanced_dependence').not(classPrefix + '_mnswmc_price_rounding' + classSuffix).fadeOut('fast', function () {
				thisSection.find('.show_if_mnswmc_simple_dependence').fadeIn('fast');
			});

			if (currencyEl.length <= 0)
				return;

			if (profitValueEl.val().length <= 0)
				profitValue = 0;

			if (salePriceEl.val().length > 0 && salePrice >= price) {
				salePriceEl.on('focusout', function () {
					$(this).val('');
					thisSection.find('.mnswmc_sale_price_preview').find('code').text('0');
					thisSection.find('.show_if_mnswmc_sale_price_filled').fadeOut('fast');
				});
			} else {
				salePriceEl.unbind('focusout');
			}

			if (rate != '0') {
				thisSection.find('.mnswmc_currency_rate_preview').find('code').text(mnswmcFormatNumbers(rate));
				thisSection.find('.show_if_mnswmc_currency_selected').fadeIn('fast');

				thisSection.find(classPrefix + '_mnswmc_regular_price' + classSuffix).find('.description').text(currencyEl.find(':selected').text());
				thisSection.find(classPrefix + '_mnswmc_sale_price' + classSuffix).find('.description').text(currencyEl.find(':selected').text());

			} else {
				thisSection.find('.mnswmc_currency_rate_preview').find('code').text('0');
				thisSection.find('.show_if_mnswmc_currency_selected').fadeOut('fast');

				thisSection.find(classPrefix + '_mnswmc_regular_price' + classSuffix).find('.description').text('');
				thisSection.find(classPrefix + '_mnswmc_sale_price' + classSuffix).find('.description').text('');

				thisSection.find('.show_if_mnswmc_regular_price_filled').fadeOut('fast');
				thisSection.find('.show_if_mnswmc_sale_price_filled').fadeOut('fast');

				return;
			}

			if (priceEl.val().length > 0) {

				var finalPrice = mnswmcGenerateRateBasedPrice(price, rate, round, roundType, roundSide, profitValue, profitType);
				thisSection.find('.mnswmc_regular_price_preview').find('code').text(mnswmcFormatNumbers(finalPrice));
				thisSection.find('.show_if_mnswmc_regular_price_filled').fadeIn('fast');

			} else {
				thisSection.find('.mnswmc_regular_price_preview').find('code').text('0');
				thisSection.find('.show_if_mnswmc_regular_price_filled').fadeOut('fast');
			}

			if (salePriceEl.val().length > 0) {

				var finalPrice = mnswmcGenerateRateBasedPrice(salePrice, rate, round, roundType, roundSide, profitValue, profitType);
				thisSection.find('.mnswmc_sale_price_preview').find('code').text(mnswmcFormatNumbers(finalPrice));
				thisSection.find('.show_if_mnswmc_sale_price_filled').fadeIn('fast');

			} else {
				thisSection.find('.mnswmc_sale_price_preview').find('code').text('0');
				thisSection.find('.show_if_mnswmc_sale_price_filled').fadeOut('fast');
			}

		} else if (dependence == 'advanced') {

			if (postType == 'variation')
				thisSection.find(classPrefix + '_mnswmc_price_rounding' + classSuffix).addClass('form-row-full').removeClass('form-row-first');

			roundValueEl.addClass('mnswmc_formul_field');
			roundTypeEl.addClass('mnswmc_formul_field');

			thisSection.find('.show_if_mnswmc_simple_dependence, .show_if_mnswmc_currency_selected, .show_if_mnswmc_regular_price_filled, .show_if_mnswmc_sale_price_filled').not(classPrefix + '_mnswmc_price_rounding' + classSuffix).fadeOut('fast', function () {
				thisSection.find('.show_if_mnswmc_advanced_dependence').fadeIn('fast');
			});

			var formulaContainer = thisSection.find('.mnswmc-product-formula-container');

			mnswmcFormulaInit(formulaContainer, round, roundType, roundSide);

			thisSection.on('change', '.mnswmc_formula_switch', function (e) {

				e.stopImmediatePropagation();
				mnswmcSwitchFormula(formulaContainer, $(this).val());
				mnswmcRunFormula(formulaContainer, round, roundType, roundSide);
			});
		}

		return;
	}

	function mnswmcCurrencyRateCalc(thisSection) {

		var value = parseFloat(thisSection.find('#_mnswmc_currency_value').val()),
			profit = parseFloat(thisSection.find('#_mnswmc_currency_profit').val()),
			fee = parseFloat(thisSection.find('#_mnswmc_currency_fee').val()),
			fixed = parseFloat(thisSection.find('#_mnswmc_currency_fixed').val()),
			ratio = parseFloat(thisSection.find('#_mnswmc_currency_ratio').val()),
			update_type = thisSection.find('#_mnswmc_currency_update_type').find(':selected').val(),
			relation = parseFloat(thisSection.find('.mnswmc_webservice_select[data-webservice=' + update_type + ']').find(':selected').data('rate')),
			connection = thisSection.find('#_mnswmc_currency_update_type').find(':selected').data('connection'),
			connection_value = parseFloat(thisSection.find('#_mnswmc_currency_connection').find(':selected').data('rate')),
			lowest_rate = parseFloat(thisSection.find('#_mnswmc_currency_lowest_rate').val());

		if (update_type == 'none') {
			thisSection.find('#_mnswmc_currency_value').prop('readonly', false);
			thisSection.find('#_mnswmc_currency_lowest_rate').prop('readonly', true).val('0');
			thisSection.find('.show_if_auto_update').fadeOut('fast');
			thisSection.find('.mnswmc_webservice_select').prop('disabled', 'disabled').fadeOut('fast');
		} else {
			thisSection.find('#_mnswmc_currency_value').prop('readonly', true);
			thisSection.find('#_mnswmc_currency_lowest_rate').prop('readonly', false);
			thisSection.find('.show_if_auto_update').fadeIn('fast');
			thisSection.find('.mnswmc_webservice_select').not('.mnswmc_webservice_select[data-webservice=' + update_type + ']').prop('disabled', 'disabled').hide();
			thisSection.find('.mnswmc_webservice_select[data-webservice=' + update_type + ']').prop('disabled', false).show();
			value = relation;
		}

		if (connection) {
			thisSection.find('.mnswmc_connection_select').prop('disabled', false);
			thisSection.find('.show_if_connection').fadeIn('fast');
			value = value * connection_value;
		} else {
			thisSection.find('.mnswmc_connection_select').prop('disabled', true);
			thisSection.find('.show_if_connection').fadeOut('fast');
		}

		if (thisSection.find('#_mnswmc_currency_profit').val().length <= 0)
			profit = 0;

		if (thisSection.find('#_mnswmc_currency_fee').val().length <= 0)
			fee = 0;

		if (thisSection.find('#_mnswmc_currency_fixed').val().length <= 0)
			fixed = 0;

		if (thisSection.find('#_mnswmc_currency_ratio').val().length <= 0)
			ratio = 1;

		profit += fee;

		var rate = mnswmcGenerateRateBasedPrice(ratio, value, 0, 'zero', 'zero', profit, 'percent'),
			rate = rate + fixed,
			value = mnswmcGenerateRateBasedPrice(1, value, 0, 'zero', 'zero', 0, 'percent');

		if (rate < lowest_rate)
			rate = lowest_rate;

		if (update_type != 'none')
			thisSection.find('#_mnswmc_currency_value').val(value);

		thisSection.find('._mnswmc_currency_rate_preview').find('code').text(mnswmcFormatNumbers(rate));
		return;
	}

	function mnswmcInstallLicenseAjax(container, licenseToken, nonce) {
		$.ajax({
			url: ajaxurl,
			type: "post",
			beforeSend: function () {
				container.find('#mnswmc_license_submit').addClass('disabled').text(container.find('#mnswmc_license_submit').data('loading'));
				container.find('#mnswmc_license_token').addClass('hide');
				container.find('.message').fadeOut('fast');
			},
			data: {
				action: "mnswmc_install_license",
				license_token: licenseToken,
				nonce: nonce
			},
			success: function (r) {

				var result = jQuery.parseJSON(r);

				if (result.status) {
					container.find('.message').html(result.message).removeClass('notice-info notice-warning notice-error').addClass('notice-success').fadeIn('fast');
					container.find('#mnswmc_license_submit').removeClass('install disabled').addClass('uninstall').text(container.find('#mnswmc_license_submit').data('uninstall'));
					container.find('#mnswmc_license_token').removeClass('hide').attr('disabled', 'disabled');
				} else {
					container.find('.message').html(result.message).removeClass('notice-info notice-warning notice-success').addClass('notice-error').fadeIn('fast');
					container.find('#mnswmc_license_submit').removeClass('uninstall disabled').addClass('install').text(container.find('#mnswmc_license_submit').data('install'));
					container.find('#mnswmc_license_token').removeClass('hide');
				}
			}
		})
	}

	function mnswmcUninstallLicenseAjax(container, nonce) {
		$.ajax({
			url: ajaxurl,
			type: "post",
			beforeSend: function () {
				container.find('#mnswmc_license_submit').addClass('disabled').text(container.find('#mnswmc_license_submit').data('loading'));
				container.find('#mnswmc_license_token').addClass('hide');
				container.find('.message').fadeOut('fast');
			},
			data: {
				action: "mnswmc_uninstall_license",
				nonce: nonce
			},
			success: function (result) {
				container.find('.message').html(result).removeClass('notice-warning notice-error notice-success').addClass('notice-info').fadeIn('fast');
				container.find('#mnswmc_license_submit').removeClass('uninstall disabled').addClass('install').text(container.find('#mnswmc_license_submit').data('install'));
				container.find('#mnswmc_license_token').removeClass('hide').removeAttr('disabled').val('');
			}
		})
	}

	function mnswmcRatingNoticeInfoAjax(container, type) {
		$.ajax({
			url: ajaxurl,
			type: "post",
			beforeSend: function () { },
			data: {
				action: "mnswmc_rating_notice_info",
				action_type: type,
				nonce: container.data('nonce')
			},
			success: function (r) {

				if (r == '1') {
					container.find('.notice-dismiss').click();
				}
			}
		})
	}

	function mnswmcChangeRestApiMainTokenAjax(container) {
		$.ajax({
			url: ajaxurl,
			type: "post",
			beforeSend: function () { },
			data: {
				action: "mnswmc_change_rest_api_main_token",
				nonce: container.data('nonce')
			},
			success: function (result) {

				result = JSON.parse(result);

				if (!result) {
					console.log(result);
				}

				if (result.status === true) {
					container.find('.token').text(result.token);
					container.find('.url').text(result.url);
				}
			}
		})
	}

	function mnswmcTelegramBotAddTokenAjax(container) {

		$.ajax({
			url: ajaxurl,
			type: "post",
			beforeSend: function () {
				container.find('#mnswmc-telegram-bot-token-submit').text(container.find('#mnswmc-telegram-bot-token-submit').data('wait'));
			},
			data: {
				action: "mnswmc_telegram_bot_add_token",
				nonce: container.data('nonce'),
				token: container.find('#mnswmc-telegram-bot-token-input').val()
			},
			success: function (result) {

				result = JSON.parse(result);

				console.log(result);

				if (!result) {
					console.log(result);
				}

				container.find('#mnswmc-telegram-bot-token-submit').text(container.find('#mnswmc-telegram-bot-token-submit').data('text'));

				if (result.status === true) {
					container.find('.telegram-bot-submit').hide();
					container.find('.telegram-bot-account').fadeIn('fast');
					container.find('.telegram-bot-account').find('.token').text(result.token);
					container.find('.telegram-bot-account').find('.account-code').text(result.account_code);
					container.find('.telegram-bot-account').find('.account-code').text(result.account_code);
				} else {
					container.find('.telegram-bot-submit').find('.message').fadeIn('fast').html(result.message);
				}
			}
		})
	}

	function mnswmcTelegramBotDeleteTokenAjax(container) {

		$.ajax({
			url: ajaxurl,
			type: "post",
			beforeSend: function () {
				container.find('#mnswmc-telegram-bot-token-delete').text(container.find('#mnswmc-telegram-bot-token-delete').data('wait'));
			},
			data: {
				action: "mnswmc_telegram_bot_delete_token",
				nonce: container.data('nonce')
			},
			success: function (result) {

				result = JSON.parse(result);

				console.log(result);

				if (!result) {
					console.log(result);
				}

				container.find('#mnswmc-telegram-bot-token-delete').text(container.find('#mnswmc-telegram-bot-token-delete').data('text'));

				if (result.status === true) {
					container.find('.telegram-bot-submit').fadeIn('fast');
					container.find('.telegram-bot-account').hide();
				}
			}
		})
	}

	function mnsGetOtherProductsAjax() {

		container = $('.mns-container-products');

		$.ajax({
			url: ajaxurl,
			type: "post",
			beforeSend: function () { },
			data: {
				action: "mnswmc_other_mns_products",
				nonce: container.data('nonce')
			},
			success: function (r) {

				var result = jQuery.parseJSON(r);

				if (!result.status) {
					console.log(result);
					return;
				}

				var placeholder = $('.mns-product-placeholder');

				// Create our number formatter.
				var formatter = new Intl.NumberFormat();

				$.each(result.products, function (index, element) {

					var item = placeholder.clone();
					item.find('.mns-product-cover').html('<img src="' + element.image + '">');
					item.find('.mns-product-title').text(element.name);
					item.find('.mns-product-price').find('span').text(formatter.format(parseInt(element.price)));
					item.find('.mns-product-url').attr('href', element.url);

					item.removeClass('mns-product-placeholder').fadeIn('fast');
					container.append(item);

				});
			}
		})
	}

	if ($('.mns-container-products.mnswmc').length) {

		mnsGetOtherProductsAjax();
	}

	(function () {
		if ($('.mnswmc_simple_product_fields').length) {

			var thisSection = $('.mnswmc_simple_product_fields'),
				formulaContainer = thisSection.find('.mnswmc-product-formula-container'),
				formulaID = thisSection.find('.mnswmc_formula_switch').val();

			mnswmcSwitchFormula(formulaContainer, formulaID);
			mnswmcProductPriceCalc(thisSection);

			thisSection.on('change input keyup click paste', '.mnswmc_simple_field', function (e) {
				e.stopImmediatePropagation();
				mnswmcProductPriceCalc(thisSection);
			});
		}
	})();

	$(document).on('woocommerce_variations_loaded woocommerce_variations_added', function (e) {

		$('.mnswmc_variation_product_fields').each(function () {

			var thisSection = $(this),
				formulaContainer = thisSection.find('.mnswmc-product-formula-container'),
				formulaID = thisSection.find('.mnswmc_formula_switch').val();

			mnswmcSwitchFormula(formulaContainer, formulaID);
			mnswmcProductPriceCalc(thisSection, 'variation');

			// meta-boxes-product-variation.js : Line 399, 697
			//$('#variable_product_options .woocommerce_variations :input').trigger('change');
			//$('.variations-defaults select').trigger('change');
		});
	});

	$(document).on('change input keyup click paste', '.mnswmc_variation_field', function (e) {

		let thisSection = $(this).parents('.mnswmc_variation_product_fields');
		e.stopImmediatePropagation();
		mnswmcProductPriceCalc(thisSection, 'variation');
	});

	(function () {
		if ($('.mnswmc-currency-meta-box').length) {

			let thisSection = $('.mnswmc-currency-meta-box');

			mnswmcCurrencyRateCalc(thisSection);

			thisSection.on('change input keyup click paste', '.mnswmc_currency_field', function (e) {
				e.stopImmediatePropagation();
				mnswmcCurrencyRateCalc(thisSection);
			});
		}
	})();

	(function () {
		if ($('.mnswmc-formula-formula-container').length) {

			let formulaContainer = $('.mnswmc-formula-formula-container');

			mnswmcLoadFormulVariables(formulaContainer);
			mnswmcFormulaInit(formulaContainer);
		}
	})();

	$(document).on('click', '#mnswmc_license_submit', function (e) {
		e.preventDefault();

		var container = $(this).parents('.mnswmc-license-form'),
			nonce = container.data('nonce'),
			licenseToken = container.find('#mnswmc_license_token').val();

		if ($(this).hasClass('install')) {
			mnswmcInstallLicenseAjax(container, licenseToken, nonce);
		} else if ($(this).hasClass('uninstall')) {
			mnswmcUninstallLicenseAjax(container, nonce);
		}
	});

	$(document).on('click', '.button.mnswmc-rating-notice-info', function (e) {
		e.preventDefault();

		var container = $(this).parents('.notice-info'),
			action_type = $(this).data('type');

		mnswmcRatingNoticeInfoAjax(container, action_type);
	});

	$(document).on('click', '#mnswmc-change-rest-api-main-token', function (e) {

		e.preventDefault();
		mnswmcChangeRestApiMainTokenAjax($(this).parents('.mnswmc-rest-api-option'));
	});

	$(document).on('click', '#mnswmc-active-telegram-bot-button', function (e) {

		e.preventDefault();

		$(this).hide();
		$(this).parents('.telegram-bot-submit').find('.mnswmc-telegram-fields').fadeIn('fast');

		$(this).parents('.telegram-bot-submit').find('#mnswmc-telegram-bot-token-cancel').on('click', function () {
			$(this).parents('.telegram-bot-submit').find('.mnswmc-telegram-fields').hide();
			$(this).parents('.telegram-bot-submit').find('#mnswmc-active-telegram-bot-button').fadeIn('fast');
		});
	});

	$(document).on('click', '#mnswmc-telegram-bot-token-submit', function (e) {

		e.preventDefault();
		mnswmcTelegramBotAddTokenAjax($(this).parents('.mnswmc-telegram-bot-option'));
	});

	$(document).on('click', '#mnswmc-telegram-bot-token-delete', function (e) {

		e.preventDefault();
		mnswmcTelegramBotDeleteTokenAjax($(this).parents('.mnswmc-telegram-bot-option'));
	});

	// inline and bulk edit
	(function () {

		if (typeof inlineEditPost == 'undefined')
			return;

		// Prepopulating our quick-edit post info
		var $inline_editor = inlineEditPost.edit;
		inlineEditPost.edit = function (id) {

			// call old copy 
			$inline_editor.apply(this, arguments);

			// our custom functionality below
			var post_id = 0;
			if (typeof (id) == 'object') {
				post_id = parseInt(this.getId(id));
			}

			// if we have our post
			if (post_id != 0) {

				// find our row
				$post_row = $('#post-' + post_id);
				$edit_row = $('#edit-' + post_id);

				$product_type = $post_row.find('#woocommerce_inline_' + post_id).find('.product_type').text();

				if ($product_type != 'simple' && $product_type != 'external') {

					$edit_row.find('.mnswmc-inline-edit').hide();

				} else {

					$edit_row.find('.mnswmc-inline-edit').show();
					$inline = $post_row.find('#mnswmc_inline_' + post_id);
					$inline = ($inline.length) ? $inline : $post_row.find('#inline_' + post_id);

					$is_active = $inline.find('.mnswmc_active').text();
					$dependence_type = $inline.find('.mnswmc_dependence_type').text();
					$currency_id = $inline.find('.mnswmc_currency_id').text();
					$regular_price = $inline.find('.mnswmc_regular_price').text();
					$sale_price = $inline.find('.mnswmc_sale_price').text();
					$rounding_type = $inline.find('.mnswmc_rounding_type').text();
					$rounding_side = $inline.find('.mnswmc_rounding_side').text();
					$price_rounding = $inline.find('.mnswmc_price_rounding').text();
					$profit_margin = $inline.find('.mnswmc_profit_margin').text();
					$profit_type = $inline.find('.mnswmc_profit_type').text();

					if ($is_active == 'yes') {
						$edit_row.find('.mnswmc-inline-edit').find('.mnswmc_active').prop('checked', true);
					} else {
						$edit_row.find('.mnswmc-inline-edit').find('.mnswmc_active').prop('checked', false);
					}

					if ($dependence_type == '') {
						$dependence_type = 'simple';
					}

					$edit_row.find('.mnswmc-inline-edit').find('.mnswmc_dependence_type').val($dependence_type).change();

					if ($dependence_type == 'simple') {

						$edit_row.find('.mnswmc-inline-edit').find('.show-if-simple-dependence').show();
						$edit_row.find('.mnswmc-inline-edit').find('.show-if-advanced-dependence').hide();

						$edit_row.find('.mnswmc-inline-edit').find('.mnswmc_currency_id').val($currency_id).change();
						$edit_row.find('.mnswmc-inline-edit').find('.mnswmc_regular_price').val($regular_price);
						$edit_row.find('.mnswmc-inline-edit').find('.mnswmc_sale_price').val($sale_price);
						$edit_row.find('.mnswmc-inline-edit').find('.mnswmc_rounding_type').val($rounding_type).change();
						$edit_row.find('.mnswmc-inline-edit').find('.mnswmc_rounding_side').val($rounding_side).change();
						$edit_row.find('.mnswmc-inline-edit').find('.mnswmc_price_rounding').val($price_rounding).change();
						$edit_row.find('.mnswmc-inline-edit').find('.mnswmc_profit_margin').val($profit_margin);
						$edit_row.find('.mnswmc-inline-edit').find('.mnswmc_profit_type').val($profit_type).change();

						$edit_row.find('.mnswmc-inline-edit').on('change input keyup click paste', '.mnswmc_simple_inline_field', function (e) {

							e.stopImmediatePropagation();

							var this_section = $edit_row.find('.mnswmc-inline-edit'),
								this_active = this_section.find('.mnswmc_active').val(),
								this_currency_rate = parseFloat(this_section.find('.mnswmc_currency_id').find(':selected').data('rate')),
								this_regular_price = parseFloat(this_section.find('.mnswmc_regular_price').val()),
								this_sale_price = parseFloat(this_section.find('.mnswmc_sale_price').val()),
								this_rounding_type = this_section.find('.mnswmc_rounding_type').val(),
								this_rounding_side = this_section.find('.mnswmc_rounding_side').val(),
								this_price_rounding = parseFloat(this_section.find('.mnswmc_price_rounding').val()),
								this_profit_margin = parseFloat(this_section.find('.mnswmc_profit_margin').val()),
								this_profit_type = this_section.find('.mnswmc_profit_type').val();

							var finalPrice = mnswmcGenerateRateBasedPrice(this_regular_price, this_currency_rate, this_price_rounding, this_rounding_type, this_rounding_side, this_profit_margin, this_profit_type),
								finalSalePrice = mnswmcGenerateRateBasedPrice(this_sale_price, this_currency_rate, this_price_rounding, this_rounding_type, this_rounding_side, this_profit_margin, this_profit_type);

							if (this_section.find('.mnswmc_active').is(':checked')) {
								this_section.find('.mnswmc-inline-currency-rate-preview').fadeIn('fast').find('code').text(mnswmcFormatNumbers(this_currency_rate));

								if (this_section.find('.mnswmc_regular_price').val().length) {
									this_section.find('.mnswmc-inline-regular-price-preview').fadeIn('fast').find('code').text(mnswmcFormatNumbers(finalPrice));
								} else {
									this_section.find('.mnswmc-inline-regular-price-preview').fadeOut('fast').find('code').text('0');
								}

								if (this_section.find('.mnswmc_sale_price').val().length) {
									this_section.find('.mnswmc-inline-sale-price-preview').fadeIn('fast').find('code').text(mnswmcFormatNumbers(finalSalePrice));
								} else {
									this_section.find('.mnswmc-inline-sale-price-preview').fadeOut('fast').find('code').text('0');
								}

							} else {
								this_section.find('.mnswmc-inline-currency-rate-preview').fadeOut('fast').find('code').text('0');
								this_section.find('.mnswmc-inline-regular-price-preview').fadeOut('fast').find('code').text('0');
								this_section.find('.mnswmc-inline-sale-price-preview').fadeOut('fast').find('code').text('0');
							}
						});

					} else {

						//$edit_row.find('.mnswmc-inline-edit').find('.mnswmc_active').prop('disabled', true);
						$edit_row.find('.mnswmc-inline-edit').find('.show-if-simple-dependence').hide();
						$edit_row.find('.mnswmc-inline-edit').find('.show-if-advanced-dependence').show();
					}
				}
			}
		}

		$('.mnswmc-inline-bulk-edit').on('change click', '.change_to', function (e) {

			var changeInput = $(this).parents('.change-action').siblings('.change-input');

			if ($(this).val() == 'none') {
				changeInput.hide();
			} else {
				changeInput.show();
			}
		});

		var bulkEditing = false;

		$('body').on('click', 'input[name="bulk_edit"]', function (e) {

			if (!bulkEditing) {
				e.preventDefault();
			} else {
				bulkEditing = false;
				return;
			}

			var thisButton = $(this),
				bulk_edit_row = thisButton.parents('tr#bulk-edit'),
				post_ids = new Array(),
				bulk_edit_mnswmc_block = bulk_edit_row.find('.mnswmc-inline-bulk-edit');

			var mnswmc_active = bulk_edit_mnswmc_block.find('.mnswmc_active').val(),
				mnswmc_currency_id = bulk_edit_mnswmc_block.find('.mnswmc_currency_id').val(),
				change_mnswmc_regular_price = bulk_edit_mnswmc_block.find('.change_mnswmc_regular_price').val(),
				mnswmc_regular_price = bulk_edit_mnswmc_block.find('.mnswmc_regular_price').val(),
				change_mnswmc_sale_price = bulk_edit_mnswmc_block.find('.change_mnswmc_sale_price').val(),
				mnswmc_sale_price = bulk_edit_mnswmc_block.find('.mnswmc_sale_price').val(),
				mnswmc_rounding_type = bulk_edit_mnswmc_block.find('.mnswmc_rounding_type').val(),
				mnswmc_rounding_side = bulk_edit_mnswmc_block.find('.mnswmc_rounding_side').val(),
				mnswmc_price_rounding = bulk_edit_mnswmc_block.find('.mnswmc_price_rounding').val(),
				mnswmc_profit_margin = bulk_edit_mnswmc_block.find('.mnswmc_profit_margin').val(),
				mnswmc_profit_type = bulk_edit_mnswmc_block.find('.mnswmc_profit_type').val();

			// Obtain the post IDs selected for bulk edit
			bulk_edit_row.find('#bulk-titles').children().each(function () {
				post_ids.push($(this).attr('id').replace(/^(ttle)/i, ''));
			});

			// Save the data with AJAX
			$.ajax({
				url: ajaxurl,
				type: 'POST',
				beforeSend: function () {
					bulkEditing = true;
					thisButton.after('<span class="spinner is-active"></span>');
				},
				data: {
					action: 'mnswmc_product_bulk_edit_save',
					post_ids: post_ids,
					_mnswmc_active: mnswmc_active,
					_mnswmc_currency_id: mnswmc_currency_id,
					change_mnswmc_regular_price: change_mnswmc_regular_price,
					_mnswmc_regular_price: mnswmc_regular_price,
					change_mnswmc_sale_price: change_mnswmc_sale_price,
					_mnswmc_sale_price: mnswmc_sale_price,
					_mnswmc_rounding_type: mnswmc_rounding_type,
					_mnswmc_rounding_side: mnswmc_rounding_side,
					_mnswmc_price_rounding: mnswmc_price_rounding,
					_mnswmc_profit_margin: mnswmc_profit_margin,
					_mnswmc_profit_type: mnswmc_profit_type,
					nonce: bulk_edit_row.find('#_mnswmc_bulk_edit_product_nonce').val()
				},
				success: function () {
					thisButton.trigger('click');
				}
			});

			return;
		});
	})();
});