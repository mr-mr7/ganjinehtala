function mnswmcDynamicDecimal(value) {

	value = Math.abs(parseFloat(value)),
		decimal = 0;

	if (value >= 1000000) decimal = 0;
	else if (value >= 1 || value == 0) decimal = 2;
	else if (value < 1 && value >= 0.1) decimal = 3;
	else if (value < 0.1 && value >= 0.01) decimal = 4;
	else if (value < 0.01 && value >= 0.001) decimal = 5;
	else if (value < 0.001 && value >= 0.0001) decimal = 6;
	else if (value < 0.0001 && value >= 0.00001) decimal = 7;
	else decimal = 8;

	return decimal;
}

function mnswmcFormatNumbers(number, decimal = null, decimalSep = '.', thousandSep = ',') {

	number = Number(number);

	if (decimal == null) {
		if (Number.isInteger(number)) {
			decimal = 0;
		} else {
			decimal = mnswmcDynamicDecimal(number);
		}
	}

	return jQuery.number(number, decimal, decimalSep, thousandSep);
}

function mnswmcRoundNumber(number, round = 0, type = 'zero', side = 'close') {

	if (round == 0)
		return number;

	switch (type) {
		case 'zero':

			if (side == 'close') {
				number = round * Math.round(number / round);
			} else if (side == 'up') {
				number = round * Math.ceil(number / round);
			} else if (side == 'down') {
				number = round * Math.floor(number / round);
			}
			break;

		case 'nine':

			if (side == 'close') {
				number = (round * 10 * Math.round(number / (round * 10))) - 1;
			} else if (side == 'up') {
				number = (round * 10 * Math.ceil(number / (round * 10))) - 1;
			} else if (side == 'down') {
				number = (round * 10 * Math.floor(number / (round * 10))) - 1;
			}
			break;

		case 'hybrid':

			if (side == 'close') {
				number = (round * 10 * Math.round(number / (round * 10))) - round;
			} else if (side == 'up') {
				number = (round * 10 * Math.ceil(number / (round * 10))) - round;
			} else if (side == 'down') {
				number = (round * 10 * Math.floor(number / (round * 10))) - round;
			}
			break;

		default:
			break;
	}

	return number;
}

function mnswmcGenerateRateBasedPrice(price, rate, round, roundType, roundSide, profitValue, profitType) {

	switch (profitType) {
		case 'percent':

			if (profitValue < -100)
				profitValue = -100;

			profitValue = (profitValue / 100) + 1;
			price = price * rate * profitValue;
			break;
		case 'fixed':

			if (profitValue < 0)
				profitValue = 0;

			price = (price + (profitValue / rate)) * rate;
			break;

		default:
			break;
	}

	var finalPrice = mnswmcRoundNumber(price, round, roundType, roundSide);

	if (finalPrice < 0)
		finalPrice = 0;

	return finalPrice;
}

function mnswmcLoadCurrencyChart(chartContainer, canvasId, labels, values, rates, sybmol) {

	const { __, _x, _n, sprintf } = wp.i18n;

	var fontFamily = chartContainer.css('font-family');

	var data = {
		labels: labels,
		datasets: [
			{
				label: __('Base Rate', 'mnswmc-js'),
				data: values,
				backgroundColor: "rgba(255, 99, 132, 0.2)",
				borderColor: "rgba(255, 99, 132, 1)",
				borderWidth: 2,
				hoverBackgroundColor: "rgba(255, 99, 132, 0.4)",
				hoverBorderColor: "rgba(255, 99, 132, 1)",
				pointBackgroundColor: "rgba(255, 99, 132, 1)",
				pointHoverBackgroundColor: "rgba(255, 99, 132, 1)"
			},
			{
				label: __('Final Rate', 'mnswmc-js'),
				data: rates,
				backgroundColor: "rgba(75, 192, 192, 0.2)",
				borderColor: "rgba(75, 192, 192, 1)",
				borderWidth: 2,
				hoverBackgroundColor: "rgba(75, 192, 192, 0.4)",
				hoverBorderColor: "rgba(75, 192, 192, 1)",
				pointBackgroundColor: "rgba(75, 192, 192, 1)",
				pointHoverBackgroundColor: "rgba(75, 192, 192, 1)"
			}
		]
	};

	var options = {
		maintainAspectRatio: false,
		layout: {
			padding: {
				left: 10,
				right: 10,
				top: 10,
				bottom: 10
			}
		},
		title: {
			display: false,
		},
		tooltips: {
			mode: 'index',
			intersect: false,
			rtl: true,
			textDirection: 'rtl',
			titleAlign: 'left',
			bodyAlign: 'right',
			footerAlign: 'right',
			backgroundColor: '#fff',
			borderColor: '#ccd0d4',
			borderWidth: 1,
			titleFontFamily: fontFamily,
			titleFontColor: '#444',
			bodyFontFamily: fontFamily,
			bodyFontColor: '#444',
			footerFontFamily: fontFamily,
			footerFontColor: '#444',
			titleMarginBottom: 10,
			footerMarginTop: 10,
			xPadding: 10,
			yPadding: 10,
			titleSpacing: 10,
			bodySpacing: 10,
			footerSpacing: 10,
			callbacks: {
				label: function (tooltipItem, data) {
					return ' ' + mnswmcFormatNumbers(tooltipItem.yLabel) + ' ' + sybmol;
				}
			}
		},
		legend: {
			display: true,
			align: 'start',
			textDirection: 'rtl',
			rtl: true,
			labels: {
				fontColor: '#444',
				fontFamily: fontFamily,
			}
		},
		scales: {
			yAxes: [{
				stacked: false,
				gridLines: {
					display: false
				},
				ticks: {
					fontFamily: fontFamily,
					callback: function (value, index, values) {
						return value;
					}
				}
			}],
			xAxes: [{
				ticks: {
					fontFamily: fontFamily,
				},
				gridLines: {
					display: false
				}
			}]
		}
	};

	var ctx = document.getElementById(canvasId).getContext("2d");

	var chart = new Chart(ctx, {
		type: 'line',
		data: data,
		options: options,
	});
}