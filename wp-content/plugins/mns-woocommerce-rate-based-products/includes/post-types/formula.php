<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Register Custom Post Type
function mnswmc_post_type_formula() {

	$labels = array(
		'name'                  => _x( 'Formulas', 'Post Type General Name', 'mnswmc' ),
		'singular_name'         => _x( 'Formula', 'Post Type Singular Name', 'mnswmc' ),
		'menu_name'             => __( 'Formulas', 'mnswmc' ),
		'name_admin_bar'        => __( 'Formula', 'mnswmc' ),
		'archives'              => __( 'Formula Archives', 'mnswmc' ),
		'attributes'            => __( 'Formula Attributes', 'mnswmc' ),
		'parent_item_colon'     => __( 'Parent Formula:', 'mnswmc' ),
		'all_items'             => __( 'Formulas', 'mnswmc' ),
		'add_new_item'          => __( 'Add New Formula', 'mnswmc' ),
		'add_new'               => __( 'Add New', 'mnswmc' ),
		'new_item'              => __( 'New Formula', 'mnswmc' ),
		'edit_item'             => __( 'Edit Formula', 'mnswmc' ),
		'update_item'           => __( 'Update Formula', 'mnswmc' ),
		'view_item'             => __( 'View Formula', 'mnswmc' ),
		'view_items'            => __( 'View Formula', 'mnswmc' ),
		'search_items'          => __( 'Search Formula', 'mnswmc' ),
		'not_found'             => __( 'Not found', 'mnswmc' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'mnswmc' ),
		'featured_image'        => __( 'Featured Image', 'mnswmc' ),
		'set_featured_image'    => __( 'Set featured image', 'mnswmc' ),
		'remove_featured_image' => __( 'Remove featured image', 'mnswmc' ),
		'use_featured_image'    => __( 'Use as featured image', 'mnswmc' ),
		'insert_into_item'      => __( 'Insert into Formula', 'mnswmc' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Formula', 'mnswmc' ),
		'items_list'            => __( 'Formulas list', 'mnswmc' ),
		'items_list_navigation' => __( 'Formulas list navigation', 'mnswmc' ),
		'filter_items_list'     => __( 'Filter formulas list', 'mnswmc' ),
	);
	$args = array(
		'label'                 => __( 'Formula', 'mnswmc' ),
		'description'           => __( 'Rate Based Formulas', 'mnswmc' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail'),
		'hierarchical'          => false,
		'public'                => false,
		'show_ui'               => true,
		'show_in_menu'          => 'edit.php?post_type=product',
		'menu_position'         => 80,
		'menu_icon'             => 'dashicons-tickets',
		'show_in_admin_bar'     => false,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => false,
		'rewrite'               => false,
		'capability_type'       => 'page',
		'show_in_rest'          => false,
	);
	register_post_type( 'mnswmc-formula', $args );

}
add_action( 'init', 'mnswmc_post_type_formula', 0 );