<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Register Custom Post Type
function mnswmc_post_type() {

	$labels = array(
		'name'                  => _x( 'Currencies', 'Post Type General Name', 'mnswmc' ),
		'singular_name'         => _x( 'Currency', 'Post Type Singular Name', 'mnswmc' ),
		'menu_name'             => __( 'Currencies', 'mnswmc' ),
		'name_admin_bar'        => __( 'Currency', 'mnswmc' ),
		'archives'              => __( 'Currency Archives', 'mnswmc' ),
		'attributes'            => __( 'Currency Attributes', 'mnswmc' ),
		'parent_item_colon'     => __( 'Parent Currency:', 'mnswmc' ),
		'all_items'             => __( 'Currencies', 'mnswmc' ),
		'add_new_item'          => __( 'Add New Currency', 'mnswmc' ),
		'add_new'               => __( 'Add New', 'mnswmc' ),
		'new_item'              => __( 'New Currency', 'mnswmc' ),
		'edit_item'             => __( 'Edit Currency', 'mnswmc' ),
		'update_item'           => __( 'Update Currency', 'mnswmc' ),
		'view_item'             => __( 'View Currency', 'mnswmc' ),
		'view_items'            => __( 'View Currencies', 'mnswmc' ),
		'search_items'          => __( 'Search Currency', 'mnswmc' ),
		'not_found'             => __( 'Not found', 'mnswmc' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'mnswmc' ),
		'featured_image'        => __( 'Featured Image', 'mnswmc' ),
		'set_featured_image'    => __( 'Set featured image', 'mnswmc' ),
		'remove_featured_image' => __( 'Remove featured image', 'mnswmc' ),
		'use_featured_image'    => __( 'Use as featured image', 'mnswmc' ),
		'insert_into_item'      => __( 'Insert into currency', 'mnswmc' ),
		'uploaded_to_this_item' => __( 'Uploaded to this currency', 'mnswmc' ),
		'items_list'            => __( 'Currencies list', 'mnswmc' ),
		'items_list_navigation' => __( 'Currencies list navigation', 'mnswmc' ),
		'filter_items_list'     => __( 'Filter currencies list', 'mnswmc' ),
	);
	$args = array(
		'label'                 => __( 'Currency', 'mnswmc' ),
		'description'           => __( 'Rate Based Currencies', 'mnswmc' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail'),
		'hierarchical'          => false,
		'public'                => false,
		'show_ui'               => true,
		'show_in_menu'          => 'edit.php?post_type=product',
		'menu_position'         => 80,
		'menu_icon'             => 'dashicons-tickets',
		'show_in_admin_bar'     => false,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => false,
		'rewrite'               => false,
		'capability_type'       => 'page',
		'show_in_rest'          => false,
	);
	register_post_type( 'mnswmc', $args );

}
add_action( 'init', 'mnswmc_post_type', 0 );