<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function mnswmc_admin_woocommerce_notice__error() {

	$message = sprintf( __( 'We detected that you have not installed WooCommerce Plugin! Please install WooCommerce first to work %s plugin correctly.', 'mnswmc' ), mnswmc_info('name') );
	printf( '<div class="notice notice-error"><p>%1$s</p></div>', $message );
}

function mnswmc_admin_woocommerce_update_notice__error() {
	
	$message = sprintf( __( 'Your WooCommerce version is too low, and %s plugin may not work well! Please update WooCommerce to a newer version.', 'mnswmc' ), mnswmc_info('name') );
	printf( '<div class="notice notice-error"><p>%1$s</p></div>', $message );
}

function mnswmc_admin_wordpress_update_notice__error() {
	
	$message = sprintf( __( 'Your WordPress version is too low, and %s plugin may not work well! Please update WordPress to a newer version.', 'mnswmc' ), mnswmc_info('name') );
	printf( '<div class="notice notice-error"><p>%1$s</p></div>', $message );
}

function mnswmc_admin_php_update_notice__error() {
	
	$message = sprintf( __( 'Your PHP version is too low, and %s plugin may not work well! If your server management is not up to you, ask your support to update PHP to a newer version.', 'mnswmc' ), mnswmc_info('name') );
	printf( '<div class="notice notice-error"><p>%1$s</p></div>', $message );
}

function mnswmc_admin_sourceguardian_loader_notice__error() {

	$message = sprintf( __( 'Unfortunately, SourceGuardian Loader is not installed on your host or server and %s plugin can not work correctly. If your server management is not up to you, ask your support to install SourceGuardian Loader.', 'mnswmc' ), mnswmc_info('name') );
	printf( '<div class="notice notice-error"><p>%1$s</p></div>', $message );
}

function mnswmc_admin_sourceguardian_loader_update_notice__error() {
	
	$message = sprintf( __( 'Your SourceGuardian version is too low, and %s plugin may not work well! Please update SourceGuardian to a newer version. If your server management is not up to you, ask your support to update SourceGuardian Loader.', 'mnswmc' ), mnswmc_info('name') );
	printf( '<div class="notice notice-error"><p>%1$s</p></div>', $message );
}

function mnswmc_admin_ioncube_loader_notice__error() {

	$message = sprintf( __( 'Unfortunately, ionCube Loader is not installed on your host or server and %s plugin can not work correctly. If your server management is not up to you, ask your support to install ionCube Loader.', 'mnswmc' ), mnswmc_info('name') );
	printf( '<div class="notice notice-error"><p>%1$s</p></div>', $message );
}

function mnswmc_admin_ioncube_loader_update_notice__error() {
	
	$message = sprintf( __( 'Your ionCube Loader version is too low, and %s plugin may not work well! The minimum version required is <code>%s</code> and yours is <code>%s</code>. If your server management is not up to you, ask your support to update ionCube Loader.', 'mnswmc' ), mnswmc_info('name'), MNSWMC_IC_MIN, implode('.', array_map('intval', str_split(ioncube_loader_iversion(), 2))) );
	printf( '<div class="notice notice-error"><p>%1$s</p></div>', $message );
}

function mnswmc_admin_activation_notice__error() {

	$message = sprintf( __( '%s plugin license key not registered! Please <a href="%s">register your license key</a> to enable the plugin and function properly.', 'mnswmc' ), mnswmc_info('name'), mnswmc_settings_url() );
	printf( '<div class="notice notice-error"><p>%1$s</p></div>', $message );
}

function mnswmc_admin_rating_notice__info() {

	if(defined('MNSWMC_DEMO_TUTORIAL')) {
		$message = sprintf( __( 'You are using the %s plugin demo. What do you think to download the help file to learn how using and work with this plugin?!', 'mnswmc' ), mnswmc_info('name') );
		$dl_button = '<a href="' . wp_get_attachment_url(MNSWMC_DEMO_TUTORIAL) . '" class="button">' . __('Download help file', 'mnswmc') . '</a>';
		printf( '<div class="notice notice-info"><p>%1$s</p><p>%2$s</p></div>', $message, $dl_button );
		return;
	}

	if(!current_user_can('manage_options'))
		return;

	$user_demiss = get_user_meta( get_current_user_id(), '_mnswmc_user_rating_notice_info_demiss', true );

	if($user_demiss)
		return;

	$install_time = get_option( 'mnswmc_install_time', 0 );
	$interval = 604800;
	$install_passed = time() - $install_time;

	if($install_passed < $interval)
		return;

	$reminder_time = get_user_meta( get_current_user_id(), '_mnswmc_user_rating_notice_info_reminder_time', true );
	$reminder_time = $reminder_time ? $reminder_time : 0;
	$reminder_passed = time() - $reminder_time;

	if($reminder_passed > $interval) {

		function mnswmc_admin_rating_notice__info_five_star() {
			$stars = '';
			for ($i=0; $i < 5; $i++) { 
				$stars .= '<i class="dashicons dashicons-star-filled"></i>';
			}
			return $stars;
		}

		$nonce = wp_create_nonce('mnswmc_rating_notice_info_nonce');
		$five_star = '<span style="color: #fea000;">' . mnswmc_admin_rating_notice__info_five_star() . '</span>';
		$message = sprintf( __( 'You\'ve been using the %s plugin for more than a week. If you are satisfied with the performance of this plugin and how to support it, make us happy with a %s rating :)', 'mnswmc' ), mnswmc_info('name'), $five_star );
		$urls = '<a href="' . mnswmc_info('url') . '" target="_blank" class="button">' . __('Rate or leave a comment', 'mnswmc') . '</a> <a href="#reminder" class="button mnswmc-rating-notice-info" data-type="reminder">' . __('Remind me later', 'mnswmc') . '</a> <a href="#demiss" class="button mnswmc-rating-notice-info" data-type="demiss">' . __('Don\'t show it anymore', 'mnswmc') . '</a>';
		printf( '<div class="notice notice-info is-dismissible" data-nonce="%3$s"><p>%1$s</p><p>%2$s</p></div>', $message, $urls, $nonce );
	}
}