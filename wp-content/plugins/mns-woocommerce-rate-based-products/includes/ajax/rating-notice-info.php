<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


function mnswmc_rating_notice_info_ajax_handler() {

	if(!wp_doing_ajax())
		exit();

	if(!isset($_POST['action_type']) or !isset($_POST['nonce']))
		exit();

	if(!wp_verify_nonce($_POST['nonce'], 'mnswmc_rating_notice_info_nonce'))
		exit();

	$action = sanitize_text_field($_POST['action_type']);

	if($action == 'reminder') {

		update_user_meta( get_current_user_id(), '_mnswmc_user_rating_notice_info_reminder_time', time() );

	} elseif($action == 'demiss') {

		delete_user_meta( get_current_user_id(), '_mnswmc_user_rating_notice_info_reminder_time' );
		update_user_meta( get_current_user_id(), '_mnswmc_user_rating_notice_info_demiss', true );
	}

	exit('1');

}
add_action('wp_ajax_mnswmc_rating_notice_info', 'mnswmc_rating_notice_info_ajax_handler');