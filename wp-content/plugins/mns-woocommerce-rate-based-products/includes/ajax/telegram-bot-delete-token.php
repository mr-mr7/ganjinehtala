<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function mnswmc_telegram_bot_delete_token_ajax_handler() {

	if(!wp_doing_ajax())
		exit();

	if(!isset($_POST['nonce']))
		exit();

	if(!wp_verify_nonce($_POST['nonce'], 'mnswmc_telegram_bot_nonce'))
		exit();

	$token = mnswmc_get_telegram_bot_token();

	mnswmc_update_telegram_bot_token(false);
	mnswmc_delete_telegram_bot_account();

	// delete webhook
	wp_remote_get('https://api.telegram.org/bot' . $token . '/deleteWebhook');

	$output = array(
		'status' => true,
		'message' => ''
	);
	exit(json_encode($output));
}
add_action('wp_ajax_mnswmc_telegram_bot_delete_token', 'mnswmc_telegram_bot_delete_token_ajax_handler');