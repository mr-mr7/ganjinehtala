<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function mnswmc_other_mns_products_ajax_handler() {

	if(!wp_doing_ajax())
		exit();

	/*if(!isset($_POST['nonce']))
		exit();*/

	/*if(!wp_verify_nonce($_POST['nonce'], 'mnswmc_other_mns_products_setting_nonce'))
		exit();*/

	$data_json = wp_remote_get(esc_url('https://hoomanmns.com/app/products.php?json'));

	if(is_wp_error($data_json)) {
		$output = array(
			'status' => false,
		);
		exit(json_encode($output));
	}

	$data_array = json_decode($data_json['body'], true);

	if(empty($data_array)) {
		$output = array(
			'status' => false,
		);
		exit(json_encode($output));
	}

	if(!isset($data_array['status']) or !$data_array['status']) {
		$output = array(
			'status' => false,
		);
		exit(json_encode($output));
	}

	exit(json_encode($data_array));
}
add_action('wp_ajax_mnswmc_other_mns_products', 'mnswmc_other_mns_products_ajax_handler');
add_action('wp_ajax_nopriv_mnswmc_other_mns_products', 'mnswmc_other_mns_products_ajax_handler');