<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function mnswmc_telegram_bot_add_token_ajax_handler() {

	if(!wp_doing_ajax())
		exit();

	if(!isset($_POST['token']) or !isset($_POST['nonce']))
		exit();

	if(!wp_verify_nonce($_POST['nonce'], 'mnswmc_telegram_bot_nonce'))
		exit();

	$token = sanitize_text_field($_POST['token']);

	$data_json = wp_remote_get(esc_url('https://api.telegram.org/bot' . $token . '/getme'));
	
	if(is_wp_error($data_json)) {
		$output = array(
			'status' => false,
			'message' => $data_json->get_error_message()
		);
		exit(json_encode($output));
	}

	$data_array = json_decode($data_json['body'], true);

	if(empty($data_array) or !isset($data_array['ok'])) {
		$output = array(
			'status' => false,
			'message' => __('Can not connect to Telegram servers.', 'mnswmc')
		);
		exit(json_encode($output));
	}

	if($data_array['ok'] != true) {
		$output = array(
			'status' => false,
			'message' => $data_array['description']
		);
		exit(json_encode($output));
	}

	$add_account_code = mnswmc_update_telegram_bot_add_account_code();
	mnswmc_update_telegram_bot_token($token);

	// add webhook
	wp_remote_get('https://api.telegram.org/bot' . $token . '/setWebhook?url=' . mnswmc_telegram_bot_webhook_file($token));

	$bot_name = $data_array['result']['first_name'];
	$bot_url = '<a href="https:t.me/' . $data_array['result']['username'] . '"><code>@' . $data_array['result']['username'] . '</code></a>';
	$message = sprintf(__('Your Telegram bot named %s has been submitted. To add accounts to this bot you have to send "Add Account Code" to %.', 'mnswmc'), $bot_name, $bot_url);

	$output = array(
		'status' => true,
		'message' => '',
		'token' => $token,
		'account_code' => $add_account_code
	);
	exit(json_encode($output));
}
add_action('wp_ajax_mnswmc_telegram_bot_add_token', 'mnswmc_telegram_bot_add_token_ajax_handler');