<?php

require_once (MNSWMC_DIR . '/includes/ajax/rating-notice-info.php');
require_once (MNSWMC_DIR . '/includes/ajax/change-rest-api-main-token.php');
require_once (MNSWMC_DIR . '/includes/ajax/telegram-bot-add-token.php');
require_once (MNSWMC_DIR . '/includes/ajax/telegram-bot-delete-token.php');
require_once (MNSWMC_DIR . '/includes/ajax/mns-products.php');