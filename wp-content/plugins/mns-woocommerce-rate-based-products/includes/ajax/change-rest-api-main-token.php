<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


function mnswmc_change_rest_api_main_token_ajax_handler() {

	if(!wp_doing_ajax())
		exit();

	if(!isset($_POST['nonce']))
		exit();

	if(!wp_verify_nonce($_POST['nonce'], 'mnswmc_change_rest_api_main_token_nonce'))
		exit();

	$token = rand(1, 1000000000);
	$token = time() . $token;
	$token = md5($token);

	update_option('mnswmc_rest_api_main_token', $token);

	$output = array(
		'status' => true,
		'token' => $token,
		'url' => mnswmc_rest_api_request_url('currency', $token)
	);

	exit(json_encode($output));
}
add_action('wp_ajax_mnswmc_change_rest_api_main_token', 'mnswmc_change_rest_api_main_token_ajax_handler');