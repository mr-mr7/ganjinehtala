<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function mnswmc_dashboard_widget_rate_converter_register() {
	wp_add_dashboard_widget(
		'mnswmc_dashboard_widget_rate_converter',
		__('Rate Converter', 'mnswmc'),
		'mnswmc_dashboard_widget_rate_converter_output'
	);
}
add_action( 'wp_dashboard_setup', 'mnswmc_dashboard_widget_rate_converter_register' );

function mnswmc_dashboard_widget_rate_converter_output() {

	mnswmc_rate_converter_template();
}