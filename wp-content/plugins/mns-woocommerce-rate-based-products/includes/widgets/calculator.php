<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function mnswmc_dashboard_widget_price_calculator_register() {
	wp_add_dashboard_widget(
		'mnswmc_dashboard_widget_price_calculator',
		__('Quick Price Calculator', 'mnswmc'),
		'mnswmc_dashboard_widget_price_calculator_output'
	);
}
add_action( 'wp_dashboard_setup', 'mnswmc_dashboard_widget_price_calculator_register' );

function mnswmc_dashboard_widget_price_calculator_output() {

	mnswmc_price_calculator_template('0', '0', '1');
}