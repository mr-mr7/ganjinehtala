<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function mnswmc_get_telegram_bot_token() {

	return get_option('mnswmc_telegram_bot_token');
}

function mnswmc_update_telegram_bot_token($token) {

	update_option('mnswmc_telegram_bot_token', $token);
}

function mnswmc_telegram_bot_add_account_code() {

	return get_option('mnswmc_telegram_bot_add_account_code');
}

function mnswmc_update_telegram_bot_add_account_code() {

	$add_account_code = rand(100000, 999999);
	update_option('mnswmc_telegram_bot_add_account_code', $add_account_code);

	return $add_account_code;
}

function mnswmc_telegram_bot_accounts() {

	$accounts = get_option('mnswmc_telegram_bot_accounts', array());
	$accounts = is_array($accounts) ? $accounts : array();

	return $accounts;
}

function mnswmc_update_telegram_bot_accounts($chat_id, $values) {

	$accounts = mnswmc_telegram_bot_accounts();
	$accounts[$chat_id] = $values;

	update_option('mnswmc_telegram_bot_accounts', $accounts);
}

function mnswmc_delete_telegram_bot_account($chat_id = true) {

	$accounts = mnswmc_telegram_bot_accounts();

	if($chat_id === true) {
		update_option('mnswmc_telegram_bot_accounts', array());
		return;
	}

	if(!isset($accounts[$chat_id]))
		return;

	unset($accounts[$chat_id]);
	update_option('mnswmc_telegram_bot_accounts', $accounts);
}

function mnswmc_send_telegram_message($token, $chat_id, $message) {

	$send = wp_remote_get('https://api.telegram.org/bot' . $token . '/sendMessage?chat_id=' . $chat_id . '&text=' . $message);
}

function mnswmc_telegram_bot_webhook() {

	register_rest_route('mnswmc/v1', '/telegram/webhook/(?P<token>[a-zA-Z0-9-\:\-\_]+)', array(
		'methods' => WP_REST_Server::READABLE,
		'callback' => 'mnswmc_telegram_bot_webhook_callback',
		'args' => array(
			'token' => array(
				'validate_callback' => function($param, $request, $key) {
					return true;
				}
			),
		),
		//'permission_callback' => '__return_true',
		'permission_callback' => function () {
			return true;
		}
	));
}
add_action( 'rest_api_init', 'mnswmc_telegram_bot_webhook');

function mnswmc_telegram_bot_webhook_url($token, $args = array()) {

	return rest_url('mnswmc/v1/telegram/webhook/' . $token);
}

function mnswmc_telegram_bot_webhook_file($token, $args = array()) {

	$webhook_file = MNSWMC_URL . 'includes/webhook.php?webhookUrl=' . mnswmc_telegram_bot_webhook_url($token);
	return apply_filters('mnswmc_telegram_bot_webhook_file', $webhook_file);
}

function mnswmc_telegram_bot_webhook_callback($request) {

	$token = sanitize_text_field($request['token']);

	if(mnswmc_get_telegram_bot_token() != $token) {
		return new WP_Error( 'not-found', 'Endpoint not found.', array( 'status' => 404 ) );
	}

	if(!isset($_GET['getUpdate']) or empty($_GET['getUpdate'])) {
		return new WP_Error( 'not-found', 'Data not found.', array( 'status' => 404 ) );
	}

	$result = json_decode(stripcslashes($_GET['getUpdate']), true);

	if(empty($result) or !is_array($result) or !isset($result['update_id'])) {
		return new WP_Error( 'not-found', 'Invalid data not received.', array( 'status' => 404 ) );
	}

	$chat_id = $result['message']['chat']['id'];
	$first_name = $result['message']['chat']['first_name'];
	$text = $result['message']['text'];

	if(is_numeric($text) and strlen($text) == 6) {

		if($text == mnswmc_telegram_bot_add_account_code()) {

			mnswmc_update_telegram_bot_accounts($chat_id, array(
				'chat_id' => $chat_id,
				'first_name' => $first_name
			));
			mnswmc_update_telegram_bot_add_account_code();

			$message = __('Thank you! Your account has beed added!', 'mnswmc');
			$send = mnswmc_send_telegram_message($token, $chat_id, $message);
	
			$output = array(
				'status' => true,
				'chat_id' => $chat_id,
				'message' => $message
			);
			return $output;

		} else {

			$message = __('Add account code is not valid!', 'mnswmc');
			$send = mnswmc_send_telegram_message($token, $chat_id, $message);
	
			$output = array(
				'status' => true,
				'chat_id' => $chat_id,
				'message' => $message
			);
			return $output;

		}
	}

	$commands = mnswmc_get_telegram_bot_commands();

	foreach ($commands as $command_key => $command_values) {
		if($text == '/' . $command_key) {
			if(function_exists($command_values['callback_function'])) {
				return $command_values['callback_function']($token, $chat_id, null);
			}
			break;
		}
	}
	
	$output = array(
		'status' => false,
		'message' => 'Not valid command.'
	);
	return $output;
}

function mnswmc_get_telegram_bot_commands() {

	$commands = array(
		'start' => array(
			'description' => __('Welcome message', 'mnswmc'),
			'callback_function' => 'mnswmc_telegram_bot_command_start'
		),
		'help' => array(
			'description' => __('Display available commands', 'mnswmc'),
			'callback_function' => 'mnswmc_telegram_bot_command_help'
		),
		'remove' => array(
			'description' => __('Remove your account from list', 'mnswmc'),
			'callback_function' => 'mnswmc_telegram_bot_command_remove'
		),
		'getCurrencies' => array(
			'description' => __('Get all currency rates', 'mnswmc'),
			'callback_function' => 'mnswmc_telegram_bot_command_getCurrencies'
		),
	);

	return apply_filters('mnswmc_telegram_bot_commands', $commands);
}

function mnswmc_telegram_bot_command_start($token, $chat_id, $message = null) {

	$message = sprintf(__('Welcome to %s bot. To add your account please send add account code.', 'mnswmc'), get_bloginfo('name'));
	$send = mnswmc_send_telegram_message($token, $chat_id, $message);

	$output = array(
		'status' => true,
		'chat_id' => $chat_id,
		'message' => $message
	);
	return $output;
}

function mnswmc_telegram_bot_command_help($token, $chat_id, $message = null) {

	$commands = mnswmc_get_telegram_bot_commands();
	$message = '';

	foreach ($commands as $command_key => $command_values) {
		$message .= $command_values['description'] . ':' . "\n" . '/' .$command_key . "\n\n";
	}

	$send = mnswmc_send_telegram_message($token, $chat_id, $message);

	$output = array(
		'status' => true,
		'chat_id' => $chat_id,
		'message' => $message
	);
	return $output;
}

function mnswmc_telegram_bot_command_remove($token, $chat_id, $message = null) {

	mnswmc_delete_telegram_bot_account($chat_id);

	$message = __('You have been removed from bot list.', 'mnswmc');
	$send = mnswmc_send_telegram_message($token, $chat_id, $message);

	$output = array(
		'status' => true,
		'chat_id' => $chat_id,
		'message' => $message
	);
	return $output;
}

function mnswmc_telegram_bot_command_getCurrencies($token, $chat_id, $message = null) {

	$accounts = mnswmc_telegram_bot_accounts();

	if(!isset($accounts[$chat_id])) {
		$message = __('You are not in bot accounts list!', 'mnswmc');
		$send = mnswmc_send_telegram_message($token, $chat_id, $message);
	
		$output = array(
			'status' => true,
			'chat_id' => $chat_id,
			'message' => $message
		);
		return $output;
	}

	$currencies = mnswmc_get_currencies();
	$symbol = get_woocommerce_currency_symbol();
	$message = '';

	foreach ($currencies as $currency) {

		$message .= get_the_title($currency->ID) . "\n";
		$message .= mnswmc_display_decimal_number(mnswmc_get_currency_rate($currency->ID)) . ' ' . $symbol . "\n\n";
	}

	$send = mnswmc_send_telegram_message($token, $chat_id, $message);

	$output = array(
		'status' => true,
		'chat_id' => $chat_id,
		'message' => $message
	);
	return $output;
}

add_action('mnswmc_currency_rate_updated', function($currency_id, $rate) {

	$token = mnswmc_get_telegram_bot_token();

	if(empty($token))
		return;

	$message = '';
	$message .= get_the_title($currency_id) . "\n";
	$message .= mnswmc_display_decimal_number($rate) . ' ' . get_woocommerce_currency_symbol();

	$accounts = mnswmc_telegram_bot_accounts();

	foreach ($accounts as $account_key => $account_value) {
		mnswmc_send_telegram_message($token, $account_key, $message);
	}

}, 10, 2);

add_action('mnswmc_product_price_updated', function($product_id, $product_type, $live_regular_price, $live_sale_price) {

	$token = mnswmc_get_telegram_bot_token();

	if(empty($token))
		return;

	if(mnswmc_get_option_telegram_bot_alert() != 'yes')
		return;

	if(mnswmc_get_product_price_alert($product_id) != 'yes')
		return;

	$message = sprintf(__('The price of the %s product changed', 'mnswmc'), get_the_title($product_id)) . "\n";
	$message .= __('New price: ', 'mnswmc') . mnswmc_display_number($live_regular_price) . ' ' . get_woocommerce_currency_symbol();

	$accounts = mnswmc_telegram_bot_accounts();

	foreach ($accounts as $account_key => $account_value) {
		mnswmc_send_telegram_message($token, $account_key, $message);
	}

}, 10, 4);