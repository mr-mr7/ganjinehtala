<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function mnswmc_get_option_telegram_bot_alert() {

	$active = get_option('mnswmc_telegram_bot_product_alert');
	return $active ? 'yes' : false;
}

function mnswmc_get_option_global_rounding() {

	$rounding = get_option('mnswmc_global_rounding', array());
	$rounding = is_array($rounding) ? $rounding : array();

	$rounding['value'] = isset($rounding['value']) ? $rounding['value'] : '0';
	$rounding['type'] = isset($rounding['type']) ? $rounding['type'] : 'zero';
	$rounding['side'] = isset($rounding['side']) ? $rounding['side'] : 'close';

	return $rounding;
}

function mnswmc_get_option_webservices_options() {

	$options = get_option('mnswmc_webservices_options', array());
	$options = is_array($options) ? $options : array();

	return $options;
}

function mnswmc_get_option_webservices_times() {

	$times = get_option('mnswmc_webservices_times', array());
	$times = is_array($times) ? $times : array();

	return $times;
}

function mnswmc_update_webservice_time($webservice_id, $time) {

	$times = mnswmc_get_option_webservices_times();
	$times[$webservice_id] = $time;

	update_option('mnswmc_webservices_times', $times);
}

function mnswmc_get_option_webservice_data($webservice_id) {

	$data = get_option('mnswmc_webservice_data_' . $webservice_id, array());
	$data = is_array($data) ? $data : array();

	return $data;
}

function mnswmc_update_webservice_data($webservice_id, $data) {

	$data = is_array($data) ? $data : array();
	update_option('mnswmc_webservice_data_' . $webservice_id, $data);
}