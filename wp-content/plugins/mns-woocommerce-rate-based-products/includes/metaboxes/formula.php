<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


// Register the Meta box
function mnswmc_formula_meta_box() {

	add_meta_box( 'mnswmc_formula_meta_box', __('Formula', 'mnswmc'), 'mnswmc_formula_meta_box_output', array( 'mnswmc-formula' ), 'normal', 'default' );
}

add_action( 'add_meta_boxes', 'mnswmc_formula_meta_box' );


// Output the Meta box
function mnswmc_formula_meta_box_output( $post ) {

	// create a nonce field
	wp_nonce_field( 'mnswmc_formula_meta_box_nonce', '_mnswmc_formula_nonce' );

	echo '<div class="mnswmc mnswmc-formula-container mnswmc-formula-formula-container wp-clearfix">';
	mnswmc_formula_calculator_metabox_template($post->ID, 'formula', mnswmc_get_formula_data($post->ID), array(
		'formul' => '_mnswmc_formula_formul',
		'variables' => '_mnswmc_formula_variables',
		'variables_counter' => '_mnswmc_formula_variables_counter'
	));
	echo '</div>';
}

// Save the Metabox values
function mnswmc_formula_meta_box_save( $post_id ) {

	// Stop the script when doing autosave
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return;

	// Verify the nonce. If insn't there, stop the script
	if( !isset( $_POST['_mnswmc_formula_nonce'] ) || !wp_verify_nonce( $_POST['_mnswmc_formula_nonce'], 'mnswmc_formula_meta_box_nonce' ) )
		return;

	// Stop the script if the user does not have edit permissions
	if( !current_user_can( 'edit_post', $post_id ) )
		return;

	// Check to update details or not!
	if( isset( $_POST['_mnswmc_formula_formul'] ) and $_POST['_mnswmc_formula_formul'] != '' ) {
		update_post_meta( $post_id, '_mnswmc_formula_formul', sanitize_textarea_field( trim($_POST['_mnswmc_formula_formul']) ) );
	} else {
		update_post_meta( $post_id, '_mnswmc_formula_formul', '' );
	}

	// check variables counter
	if( isset($_POST['_mnswmc_formula_variables_counter']) and is_numeric($_POST['_mnswmc_formula_variables_counter']) and $_POST['_mnswmc_formula_variables_counter'] > 0 ) {
		update_post_meta( $post_id, '_mnswmc_formula_variables_counter', sanitize_text_field( $_POST['_mnswmc_formula_variables_counter'] ) );
	} else {
		update_post_meta( $post_id, '_mnswmc_formula_variables_counter', '1' );
	}

	// check variables
	if( !empty($_POST['_mnswmc_formula_variables']) and is_array($_POST['_mnswmc_formula_variables']) ) {

		$variables = array();
		foreach($_POST['_mnswmc_formula_variables'] as $key => $value) {

			if(!isset($value['value']) or !isset($value['sale']))
				continue;

			$variables[$key] = array();

			$variables[$key]['value'] = (is_numeric($value['value']) ? sanitize_text_field($value['value']) : '0');
			$variables[$key]['sale'] = (is_numeric($value['sale']) ? sanitize_text_field($value['sale']) : '');

			if(isset($value['name']) and isset($value['rate'])) {
				$variables[$key]['name'] = (!empty($value['name']) ? sanitize_text_field($value['name']) : __('Unnamed', 'mnswmc'));
				$variables[$key]['rate'] = ((is_numeric($value['rate']) and $value['rate'] > 0) ? sanitize_text_field($value['rate']) : '1');
				$variables[$key]['type'] = 'custom';
			} else {
				$variables[$key]['type'] = 'currency';
			}
		}
		update_post_meta( $post_id, '_mnswmc_formula_variables', $variables );
	} else {
		delete_post_meta( $post_id, '_mnswmc_formula_variables' );
	}

	// update formula
	mnswmc_update_formula_data($post_id);
	
}
add_action( 'save_post', 'mnswmc_formula_meta_box_save' );