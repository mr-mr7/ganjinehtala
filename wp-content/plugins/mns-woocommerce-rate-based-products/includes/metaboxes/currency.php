<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


// Register the Meta box
function mnswmc_currency_meta_box() {

	add_meta_box( 'mnswmc_currency_meta_box', __('Currency Rate', 'mnswmc'), 'mnswmc_currency_meta_box_output', array( 'mnswmc' ), 'normal', 'default' );
	add_meta_box( 'mnswmc_currency_chart_meta_box', __('Currency Rate Chart', 'mnswmc'), 'mnswmc_currency_chart_meta_box_output', array( 'mnswmc' ), 'normal', 'default' );
}

add_action( 'add_meta_boxes', 'mnswmc_currency_meta_box' );


// Output the Meta box
function mnswmc_currency_meta_box_output( $post ) {

	// create a nonce field
	wp_nonce_field( 'mnswmc_currency_meta_box_nonce', '_mnswmc_currency_nonce' );

	$woo_currency_symbol = get_woocommerce_currency_symbol();
	$value = mnswmc_get_currency_value($post->ID);
	$rate = mnswmc_get_currency_rate($post->ID);
	$profit = mnswmc_get_currency_profit($post->ID);
	$fee = mnswmc_get_currency_fee($post->ID);
	$ratio = mnswmc_get_currency_ratio($post->ID);
	$fixed = mnswmc_get_currency_fixed($post->ID);
	$lowest_rate = mnswmc_get_currency_lowest_rate($post->ID);
	$update_type = mnswmc_get_currency_update_type($post->ID);
	$relation = mnswmc_get_currency_relation($post->ID);
	$connection = mnswmc_get_currency_connection($post->ID);
	$connection_currencies = get_posts(array(
		'post_type' => 'mnswmc',
		'post_status' => 'publish',
		'post__not_in' => array( $post->ID ),
		'nopaging' => true,
		'meta_query' => array(array(
			'key' => '_mnswmc_currency_connection',
			'compare' => 'NOT EXISTS'
		))
	));
?>
	
<div class="mnswmc mnswmc-currency-meta-box wp-clearfix">
	<div class="column">
		<div class="field-box _mnswmc_currency_value">
			<p>
				<label for="_mnswmc_currency_value"><?php _e('Rate', 'mnswmc'); ?>:</label>
				<input type="text" name="_mnswmc_currency_value" id="_mnswmc_currency_value" class="mnswmc_currency_field" value="<?php echo $value; ?>">
				<span><?php echo $woo_currency_symbol; ?></span>
			</p>
		</div>
		<div class="field-box _mnswmc_currency_lowest_rate">
			<p>
				<label for="_mnswmc_currency_lowest_rate"><?php _e('Lowest Rates', 'mnswmc'); ?>:</label>
				<input type="text" name="_mnswmc_currency_lowest_rate" id="_mnswmc_currency_lowest_rate" class="mnswmc_currency_field" value="<?php echo $lowest_rate; ?>">
				<span><?php echo $woo_currency_symbol; ?></span>
			</p>
		</div>
		<div class="field-box _mnswmc_currency_fixed">
			<p>
				<label for="_mnswmc_currency_fixed"><?php _e('Fixed', 'mnswmc'); ?>:</label>
				<input type="text" min="-100" name="_mnswmc_currency_fixed" id="_mnswmc_currency_fixed" class="mnswmc_currency_field" value="<?php echo $fixed; ?>">
				<span><?php echo $woo_currency_symbol; ?></span>
			</p>
		</div>
	</div>
	<div class="column">
		<div class="field-box _mnswmc_currency_profit">
			<p>
				<label for="_mnswmc_currency_profit"><?php _e('Profit', 'mnswmc'); ?>:</label>
				<input type="text" min="-100" name="_mnswmc_currency_profit" id="_mnswmc_currency_profit" class="mnswmc_currency_field" value="<?php echo $profit; ?>">
				<span>%</span>
			</p>
		</div>
		<div class="field-box _mnswmc_currency_fee">
			<p>
				<label for="_mnswmc_currency_fee"><?php _e('Fee', 'mnswmc'); ?>:</label>
				<input type="text" min="-100" name="_mnswmc_currency_fee" id="_mnswmc_currency_fee" class="mnswmc_currency_field" value="<?php echo $fee; ?>">
				<span>%</span>
			</p>
		</div>
		<div class="field-box _mnswmc_currency_ratio">
			<p>
				<label for="_mnswmc_currency_ratio"><?php _e('Ratio', 'mnswmc'); ?>:</label>
				<input type="text" name="_mnswmc_currency_ratio" id="_mnswmc_currency_ratio" class="mnswmc_currency_field" value="<?php echo $ratio; ?>">
			</p>
		</div>
	</div>
	<div class="column _mnswmc_currency_update_type">
		<p>
			<label for="_mnswmc_currency_update_type"><?php _e('Update Type', 'mnswmc'); ?>:</label>
			<select name="_mnswmc_currency_update_type" id="_mnswmc_currency_update_type" class="mnswmc_currency_field">
				<option value="none" data-connection="" <?php selected('none', $update_type); ?>><?php _e('Manual update', 'mnswmc'); ?></option>
				<?php foreach (mnswmc_get_webservices() as $webservice_id => $webservice) {

					if(!mnswmc_get_webservice_option($webservice_id, 'active'))
						continue;

					echo '<option value="' . $webservice_id . '" data-connection="' . $webservice['connection'] . '" ' . selected($webservice_id, $update_type, false) . '>' . __('Auto update', 'mnswmc') . ': ' . $webservice['name'] . '</option>';
				} ?>
			</select>
		</p>
		<p class="show_if_auto_update" style="display: none;">
			<?php foreach(mnswmc_get_webservices() as $webservice_id => $webservice) {

				if(!mnswmc_get_webservice_option($webservice_id, 'active'))
					continue;
				
				$webservice_data = mnswmc_get_option_webservice_data($webservice_id);

				if(!is_array($webservice_data) or count($webservice_data) <= 0)
					continue;

				echo '<select id="_mnswmc_currency_relation" name="_mnswmc_currency_relation" class="mnswmc_currency_field mnswmc_webservice_select" data-webservice="' . $webservice_id . '" style="display: none;">';
				foreach ($webservice_data as $ckey => $cvalue) {
					$webservice_currency_rate = $webservice['connection'] ? esc_attr($cvalue['price']) : mnswmc_sanitize_number(mnswmc_currency_division(esc_attr($cvalue['price'])));
					$webservice_currency_rate_display = $webservice['connection'] ? $webservice_currency_rate . ' ' . esc_attr($webservice['currency']) : mnswmc_display_decimal_number($webservice_currency_rate) . ' ' . $woo_currency_symbol;
					if(isset($cvalue['price']) and isset($cvalue['name'])) {
						echo '<option value="' . esc_attr($ckey) . '" data-rate="' . $webservice_currency_rate . '"' . selected($ckey, $relation, false) . '>' . esc_attr($cvalue['name']) . ' (' . $webservice_currency_rate_display . ')' . '</option>';
					}
				}
				echo '</select>';
			} ?>
		</p>
		<p class="show_if_connection" style="display: none;">
			<?php if(is_array($connection_currencies) and count($connection_currencies) > 0) { ?>
			<select id="_mnswmc_currency_connection" name="_mnswmc_currency_connection" class="mnswmc_currency_field mnswmc_connection_select">
				<?php foreach ($connection_currencies as $currency) {
					echo '<option value="' . $currency->ID . '" data-rate="' . mnswmc_get_currency_rate($currency->ID) . '"' . selected($currency->ID, $connection, false) . '>' . $currency->post_title . ' (' . mnswmc_display_decimal_number(mnswmc_get_currency_rate($currency->ID)) . ' ' . $woo_currency_symbol . ')' . '</option>';
				} ?>
			</select>
			<?php } else {
				echo sprintf( wp_kses( __( 'You have not created any independent currencies! <a href="%s">Create</a> now', 'mnswmc' ), array(  'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php?post_type=mnswmc' ) ) );
			} ?>
		</p>
	</div>
	<div class="column _mnswmc_currency_rate_preview">
		<p>
			<?php _e('Final Rate', 'mnswmc'); ?>: <code>0</code>
			<span><?php echo $woo_currency_symbol; ?></span>
		</p>
	</div>
</div>

<?php
}

// Save the Metabox values
function mnswmc_currency_meta_box_save( $post_id ) {

	// Stop the script when doing autosave
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return;

	// Verify the nonce. If insn't there, stop the script
	if( !isset( $_POST['_mnswmc_currency_nonce'] ) || !wp_verify_nonce( $_POST['_mnswmc_currency_nonce'], 'mnswmc_currency_meta_box_nonce' ) )
		return;

	// Stop the script if the user does not have edit permissions
	if( !current_user_can( 'edit_post', $post_id ) )
		return;

	// Check to update details or not!
	if( !empty( $_POST['_mnswmc_currency_value'] ) and is_numeric( $_POST['_mnswmc_currency_value'] ) ) {
		update_post_meta( $post_id, '_mnswmc_currency_value', mnswmc_sanitize_number( $_POST['_mnswmc_currency_value'] ) );
	} else {
		update_post_meta( $post_id, '_mnswmc_currency_value', '1' );
	}

	if( !empty( $_POST['_mnswmc_currency_profit'] ) and is_numeric($_POST['_mnswmc_currency_profit']) ) {

		if($_POST['_mnswmc_currency_profit'] < -100)
			update_post_meta( $post_id, '_mnswmc_currency_profit', '-100.00' );
		else
			update_post_meta( $post_id, '_mnswmc_currency_profit', mnswmc_sanitize_number($_POST['_mnswmc_currency_profit'], 2) );

	} else {
		update_post_meta( $post_id, '_mnswmc_currency_profit', '0.00' );
	}

	if( !empty( $_POST['_mnswmc_currency_fee'] ) and is_numeric($_POST['_mnswmc_currency_fee']) ) {

		if($_POST['_mnswmc_currency_fee'] < -100)
			update_post_meta( $post_id, '_mnswmc_currency_fee', '-100.00' );
		else
			update_post_meta( $post_id, '_mnswmc_currency_fee', mnswmc_sanitize_number($_POST['_mnswmc_currency_fee'], 2) );

	} else {
		update_post_meta( $post_id, '_mnswmc_currency_fee', '0.00' );
	}

	if( !empty( $_POST['_mnswmc_currency_ratio'] ) and is_numeric($_POST['_mnswmc_currency_ratio']) ) {

		if($_POST['_mnswmc_currency_ratio'] <= 0)
			update_post_meta( $post_id, '_mnswmc_currency_ratio', '1.00' );
		else
			update_post_meta( $post_id, '_mnswmc_currency_ratio', mnswmc_sanitize_number($_POST['_mnswmc_currency_ratio'], 2) );

	} else {
		update_post_meta( $post_id, '_mnswmc_currency_ratio', '1.00' );
	}

	if( !empty( $_POST['_mnswmc_currency_fixed'] ) and is_numeric($_POST['_mnswmc_currency_fixed']) ) {

		update_post_meta( $post_id, '_mnswmc_currency_fixed', mnswmc_sanitize_number($_POST['_mnswmc_currency_fixed'], 2) );

	} else {
		update_post_meta( $post_id, '_mnswmc_currency_fixed', '0' );
	}

	if( !empty( $_POST['_mnswmc_currency_update_type'] ) ) {
		
		update_post_meta( $post_id, '_mnswmc_currency_update_type', sanitize_text_field($_POST['_mnswmc_currency_update_type']) );
	} else {
		update_post_meta( $post_id, '_mnswmc_currency_update_type', 'none' );
	}

	if( !empty( $_POST['_mnswmc_currency_relation'] ) ) {
		
		update_post_meta( $post_id, '_mnswmc_currency_relation', sanitize_text_field($_POST['_mnswmc_currency_relation']) );
	} else {
		delete_post_meta( $post_id, '_mnswmc_currency_relation' );
	}

	if( !empty( $_POST['_mnswmc_currency_connection'] ) and is_numeric( $_POST['_mnswmc_currency_connection'] ) ) {
		
		update_post_meta( $post_id, '_mnswmc_currency_connection', sanitize_text_field($_POST['_mnswmc_currency_connection']) );
	} else {
		delete_post_meta( $post_id, '_mnswmc_currency_connection');
	}

	if( !empty( $_POST['_mnswmc_currency_lowest_rate'] ) and is_numeric( $_POST['_mnswmc_currency_lowest_rate'] ) ) {
		
		update_post_meta( $post_id, '_mnswmc_currency_lowest_rate', mnswmc_sanitize_number($_POST['_mnswmc_currency_lowest_rate']) );
	} else {
		update_post_meta( $post_id, '_mnswmc_currency_lowest_rate', '0' );
	}

	mnswmc_do_update_currency($post_id);
}
add_action( 'save_post', 'mnswmc_currency_meta_box_save' );


// Output the Meta box
function mnswmc_currency_chart_meta_box_output( $post ) {

	mnswmc_template_currency_chart($post->ID, 'day', mnswmc_get_currency_max_records());
}