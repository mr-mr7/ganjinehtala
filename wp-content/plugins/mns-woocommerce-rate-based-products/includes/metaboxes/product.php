<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

function mnswmc_product_fields($post_type, $object_id, $loop = '', $variation_data = false, $variation = false) {

    $section_class = $post_type == 'product' ? 'options_group show_if_simple show_if_external show_if_deposit hidden mnswmc_simple_product_fields' : 'mnswmc_variation_product_fields';
    $data_loop     = $post_type == 'product' ? '' : ' data-loop="' . $loop . '"';

    $name_prefix  = $post_type == 'product' ? '' : '_variable';
    $name_suffix  = $post_type == 'product' ? '[0]' : '[' . $loop . ']';
    $id_prefix    = $post_type == 'product' ? '' : 'variable';
    $id_suffix    = $post_type == 'product' ? '' : '_' . $loop;
    $class_prefix = $post_type == 'product' ? '' : 'variable';
    $class_suffix = $post_type == 'product' ? '_field' : '_' . $loop . '_field';

    $field_default_class   = $post_type == 'product' ? 'mnswmc_simple_field' : 'mnswmc_variation_field';
    $wrapper_default_class = $post_type == 'product' ? '' : 'form-row';

    $active = mnswmc_get_product_active($object_id);
    $price_alert = mnswmc_get_product_price_alert($object_id);
    $dependence = mnswmc_get_product_dependence_type($object_id);
    $regular_price = mnswmc_get_product_regular_price($object_id);
    $sale_price = mnswmc_get_product_sale_price($object_id);
    $variables = mnswmc_get_product_formula_variables($object_id);
    $variables_counter = mnswmc_get_product_variables_counter($object_id);
    $formula_id = mnswmc_get_product_formula_id($object_id);
    $formula = mnswmc_get_product_formula_text($object_id);
    $currency_id = mnswmc_get_product_currency_id($object_id);
    $rounding = mnswmc_get_product_rounding_value($object_id);
    $rounding_type = mnswmc_get_product_rounding_type($object_id);
    $rounding_side = mnswmc_get_product_rounding_side($object_id);
    $profit = mnswmc_get_product_profit_margin($object_id);
    $profit_type = mnswmc_get_product_profit_type($object_id);

    $global_rounding = mnswmc_get_option_global_rounding();
    $currencies = mnswmc_get_currencies();
    $formulas = mnswmc_get_formulas();

    echo '<div class="' . $section_class . ' mnswmc mnswmc_product_fields"' . $data_loop . '>';

    do_action( 'mnswmc_before_product_fields', $post_type, $object_id, $loop );

    woocommerce_wp_checkbox(
        array(
            'id'            => $id_prefix . '_mnswmc_active' . $id_suffix,
            'name'          => $name_prefix . '_mnswmc_active' . $name_suffix,
            'wrapper_class' => $wrapper_default_class . ($post_type == 'product' ? '' : ' form-row-full'),
            'class'         => $field_default_class,
            'label'         => __('Rate Based', 'mnswmc'),
            'description'   => __( 'Check this if current product price is rate based', 'mnswmc' ),
            'value'         => $active
        )
    );

    if(mnswmc_get_option_telegram_bot_alert() == 'yes') {
        woocommerce_wp_checkbox(
            array(
                'id'            => $id_prefix . '_mnswmc_price_alert' . $id_suffix,
                'name'          => $name_prefix . '_mnswmc_price_alert' . $name_suffix,
                'wrapper_class' => $wrapper_default_class . ($post_type == 'product' ? '' : ' form-row-full') . 'show_if_mnswmc_active',
                'class'         => $field_default_class,
                'label'         => __('Price Alert', 'mnswmc'),
                'description'   => __( 'Telegram alert when price changes', 'mnswmc' ),
                'value'         => $price_alert
            )
        );
    }

    do_action( 'mnswmc_after_product_active_field', $post_type, $object_id, $loop );

    woocommerce_wp_select(
        array(
            'id'            => $id_prefix . '_mnswmc_dependence_type' . $id_suffix,
            'name'          => $name_prefix . '_mnswmc_dependence_type' . $name_suffix,
            'wrapper_class' => $wrapper_default_class . ($post_type == 'product' ? '' : ' form-row-first ') . 'show_if_mnswmc_active',
            'class'         => $field_default_class,
            'label'         => __('Dependency Type', 'mnswmc'),
            'description'   => __('Set the price of this product depending on one currency or several currencies', 'mnswmc'),
            'desc_tip'      => true,
            'options'       => array(
                'simple'    => __('Simple', 'mnswmc'),
                'advanced'  => __('Advanced (Formulation)', 'mnswmc')
            ),
            'value'         => $dependence
        )
    );

    do_action( 'mnswmc_after_product_dependence_type_field', $post_type, $object_id, $loop );
    ?>

    <p class="<?php echo 'form-field ' . $wrapper_default_class . ($post_type == 'product' ? '' : ' form-row-last ') . $class_prefix . '_mnswmc_formula_id' . $class_suffix . ' show_if_mnswmc_advanced_dependence'; ?>">
        <label for="<?php echo $id_prefix . '_mnswmc_formula_id' . $id_suffix; ?>"><?php _e( 'Choose formula', 'mnswmc' ); ?></label>
        <?php
        echo wc_help_tip(__('Type a custom formula or choose a predefined formula', 'mnswmc'));
        mnswmc_formulas_list($formulas, $id_prefix . '_mnswmc_formula_id' . $id_suffix, 'mnswmc_formula_field mnswmc_formula_switch', $name_prefix . '_mnswmc_formula_id' . $name_suffix, $formula_id, __('Custom Formula', 'mnswmc'));
        ?>
    </p>

    <p class="<?php echo 'form-field ' . $wrapper_default_class . ($post_type == 'product' ? '' : ' form-row-full ') . $class_prefix . '_mnswmc_formula' . $class_suffix . ' mnswmc-formula-container mnswmc-product-formula-container show_if_mnswmc_advanced_dependence'; ?>">
        <?php mnswmc_formula_calculator_metabox_template($object_id, $post_type, $data = array(
            'formul' => $formula,
            'variables' => $variables,
            'variables_counter' => $variables_counter
        ), $fields = array(
            'formul' => $name_prefix . '_mnswmc_formula' . $name_suffix,
            'variables' => $name_prefix . '_mnswmc_variables' . $name_suffix,
            'variables_counter' => $name_prefix . '_mnswmc_variables_counter' . $name_suffix
        )); ?>
    </p>

    <?php do_action( 'mnswmc_after_product_formula_field', $post_type, $object_id, $loop ); ?>
    <?php do_action( 'mnswmc_after_product_formul_variables_field', $post_type, $object_id, $loop ); ?>

    <p class="<?php echo 'form-field ' . $wrapper_default_class . ($post_type == 'product' ? '' : ' form-row-last ') . $class_prefix . '_mnswmc_currency_id' . $class_suffix . ' show_if_mnswmc_simple_dependence'; ?>">
        <label for="<?php echo $id_prefix . '_mnswmc_currency_id' . $id_suffix; ?>"><?php _e( 'Choose Currency', 'mnswmc' ); ?></label>
        <?php
        echo wc_help_tip(__('The currency that current product price is dependent on', 'mnswmc'));
        mnswmc_currencies_list($currencies, $id_prefix . '_mnswmc_currency_id' . $id_suffix, $field_default_class, $name_prefix . '_mnswmc_currency_id' . $name_suffix, $currency_id);
        ?>
    </p>

    <?php
    do_action( 'mnswmc_after_product_currency_field', $post_type, $object_id, $loop );

    woocommerce_wp_text_input(
        array(
            'id'            => $id_prefix . '_mnswmc_regular_price' . $id_suffix,
            'name'          => $name_prefix . '_mnswmc_regular_price' . $name_suffix,
            'wrapper_class' => $wrapper_default_class . ($post_type == 'product' ? '' : ' form-row-first ') . 'show_if_mnswmc_currency_selected',
            'class'         => $field_default_class . ' wc_input_price',
            'type'          => 'text',
            'label'         => __('Rate Price', 'mnswmc' ),
            'description'   => ' ',
            'value'         => $regular_price
        )
    );

    do_action( 'mnswmc_after_product_regular_price_field', $post_type, $object_id, $loop );

    woocommerce_wp_text_input(
        array(
            'id'            => $id_prefix . '_mnswmc_sale_price' . $id_suffix,
            'name'          => $name_prefix . '_mnswmc_sale_price' . $name_suffix,
            'wrapper_class' => $wrapper_default_class . ($post_type == 'product' ? '' : ' form-row-last ') . 'show_if_mnswmc_currency_selected',
            'class'         => $field_default_class . ' wc_input_price',
            'type'          => 'text',
            'label'         => __('Sale Rate Price', 'mnswmc' ),
            'description'   => ' ',
            'value'         => $sale_price
        )
    );

    do_action( 'mnswmc_after_product_sale_price_field', $post_type, $object_id, $loop );
    ?>

    <p class="<?php echo 'form-field ' . $wrapper_default_class . ($post_type == 'product' ? '' : ' form-row-first ') . $class_prefix . '_mnswmc_profit_margin' . $class_suffix . ' show_if_mnswmc_currency_selected'; ?>">
        <label for="<?php echo $id_prefix . '_mnswmc_profit_margin' . $id_suffix; ?>"><?php _e( 'Profit Margin', 'mnswmc' ); ?></label>
        <input type="text" name="<?php echo $name_prefix . '_mnswmc_profit_margin' . $name_suffix; ?>" id="<?php echo $id_prefix . '_mnswmc_profit_margin' . $id_suffix; ?>" class="<?php echo $field_default_class; ?>" value="<?php echo $profit; ?>">
        <select name="<?php echo $name_prefix . '_mnswmc_profit_type' . $name_suffix; ?>" class="<?php echo $field_default_class; ?>" id="<?php echo $id_prefix . '_mnswmc_profit_type' . $id_suffix; ?>">
            <option value="percent" <?php selected('percent', $profit_type); ?>><?php _e('Percent', 'mnswmc'); ?></option>
            <option value="fixed" <?php selected('fixed', $profit_type); ?>><?php echo __('Fixed', 'mnswmc') . ' (' . get_woocommerce_currency_symbol() . ')'; ?></option>
        </select>
    </p>

    <?php do_action( 'mnswmc_after_product_margin_field', $post_type, $object_id, $loop ); ?>

    <p class="<?php echo 'form-field ' . $wrapper_default_class . ($post_type == 'product' ? '' : ' form-row-last ') . $class_prefix . '_mnswmc_price_rounding' . $class_suffix . ' show_if_mnswmc_currency_selected show_if_mnswmc_advanced_dependence'; ?>">
        <label for="<?php echo $id_prefix . '_mnswmc_price_rounding' . $id_suffix; ?>"><?php _e( 'Rounding', 'mnswmc' ); ?></label>
        <select name="<?php echo $name_prefix . '_mnswmc_price_rounding' . $name_suffix; ?>" class="<?php echo $field_default_class; ?> mnswmc_formul_field" id="<?php echo $id_prefix . '_mnswmc_price_rounding' . $id_suffix; ?>" data-default="<?php echo $global_rounding['value']; ?>">
            <?php foreach(mnswmc_price_rounding_items() as $key => $value) {
                echo '<option value="' . $key . '" ' . selected($key, $rounding, false) . '>' . $value . ($key == '0' ? '' : ' ' . get_woocommerce_currency_symbol()) . '</option>';
            } ?>
        </select>
        <select name="<?php echo $name_prefix . '_mnswmc_rounding_type' . $name_suffix; ?>" class="<?php echo $field_default_class; ?> mnswmc_formul_field" id="<?php echo $id_prefix . '_mnswmc_rounding_type' . $id_suffix; ?>" data-default="<?php echo $global_rounding['type']; ?>">
            <option value="zero" <?php selected('zero', $rounding_type); ?>><?php _e('To zero', 'mnswmc'); ?></option>
            <option value="nine" <?php selected('nine', $rounding_type); ?>><?php _e('To nine', 'mnswmc'); ?></option>
            <option value="hybrid" <?php selected('hybrid', $rounding_type); ?>><?php _e('Hybrid', 'mnswmc'); ?></option>
        </select>
        <select name="<?php echo $name_prefix . '_mnswmc_rounding_side' . $name_suffix; ?>" class="<?php echo $field_default_class; ?> mnswmc_formul_field" id="<?php echo $id_prefix . '_mnswmc_rounding_side' . $id_suffix; ?>" data-default="<?php echo $global_rounding['side']; ?>">
            <option value="close" <?php selected('close', $rounding_side); ?>><?php _e('Close', 'mnswmc'); ?></option>
            <option value="up" <?php selected('up', $rounding_side); ?>><?php _e('Up', 'mnswmc'); ?></option>
            <option value="down" <?php selected('down', $rounding_side); ?>><?php _e('Down', 'mnswmc'); ?></option>
        </select>
    </p>

    <?php do_action( 'mnswmc_after_product_rounding_field', $post_type, $object_id, $loop ); ?>

    <p class="<?php echo 'form-field ' . $wrapper_default_class . ($post_type == 'product' ? '' : ' form-row-full ') . 'mnswmc_currency_rate_preview show_if_mnswmc_currency_selected'; ?>">
        <label><?php _e( 'Currency Rate', 'mnswmc' ); ?></label>
        <code>0</code> <?php echo get_woocommerce_currency_symbol(); ?>
    </p>
    <p class="<?php echo 'form-field ' . $wrapper_default_class . ($post_type == 'product' ? '' : ' form-row-first ') . 'mnswmc_regular_price_preview show_if_mnswmc_regular_price_filled'; ?>">
        <label><?php _e('Final Price', 'mnswmc'); ?></label>
        <code>0</code> <?php echo get_woocommerce_currency_symbol(); ?>
    </p>
    <p class="<?php echo 'form-field ' . $wrapper_default_class . ($post_type == 'product' ? '' : ' form-row-last ') . 'mnswmc_sale_price_preview show_if_mnswmc_sale_price_filled'; ?>">
        <label><?php _e('Final Sale Price', 'mnswmc'); ?></label>
        <code>0</code> <?php echo get_woocommerce_currency_symbol(); ?>
    </p>
    <?php

    do_action( 'mnswmc_after_product_price_preview', $post_type, $object_id, $loop );
    echo '</div>';
}

function mnswmc_product_fields_save($post_type, $object_id, $loop = '') {

    $name_prefix  = $post_type == 'product' ? '' : '_variable';
    $name_suffix  = $post_type == 'product' ? 0 : $loop;

    do_action( 'mnswmc_before_product_fields_save', $post_type, $object_id, $loop );

    // check active
    if( !empty( $_POST[$name_prefix . '_mnswmc_active'][$name_suffix] ) ) {
        update_post_meta( $object_id, '_mnswmc_active', 'yes' );
    } else {
        mnswmc_delete_all_product_price_fields($object_id);
        return;
    }

    // check if price alert field was exists and checked!
    if( !empty( $_POST[$name_prefix . '_mnswmc_price_alert'][$name_suffix] ) ) {
        update_post_meta( $object_id, '_mnswmc_price_alert', 'yes' );
    } else {
        if(mnswmc_get_option_telegram_bot_alert() == 'yes') {
            delete_post_meta( $object_id, '_mnswmc_price_alert' );
        }
    }

    $dependence_type = isset($_POST[$name_prefix . '_mnswmc_dependence_type'][$name_suffix]) ? $_POST[$name_prefix . '_mnswmc_dependence_type'][$name_suffix] : '';
    $price_rounding = isset($_POST[$name_prefix . '_mnswmc_price_rounding'][$name_suffix]) ? $_POST[$name_prefix . '_mnswmc_price_rounding'][$name_suffix] : '';
    $rounding_type = isset($_POST[$name_prefix . '_mnswmc_rounding_type'][$name_suffix]) ? $_POST[$name_prefix . '_mnswmc_rounding_type'][$name_suffix] : '';
    $rounding_side = isset($_POST[$name_prefix . '_mnswmc_rounding_side'][$name_suffix]) ? $_POST[$name_prefix . '_mnswmc_rounding_side'][$name_suffix] : '';
    $currency_id = isset($_POST[$name_prefix . '_mnswmc_currency_id'][$name_suffix]) ? $_POST[$name_prefix . '_mnswmc_currency_id'][$name_suffix] : '';
    $regular_price = isset($_POST[$name_prefix . '_mnswmc_regular_price'][$name_suffix]) ? $_POST[$name_prefix . '_mnswmc_regular_price'][$name_suffix] : '';
    $sale_price = isset($_POST[$name_prefix . '_mnswmc_sale_price'][$name_suffix]) ? $_POST[$name_prefix . '_mnswmc_sale_price'][$name_suffix] : '';
    $profit_type = isset($_POST[$name_prefix . '_mnswmc_profit_type'][$name_suffix]) ? $_POST[$name_prefix . '_mnswmc_profit_type'][$name_suffix] : '';
    $profit_margin = isset($_POST[$name_prefix . '_mnswmc_profit_margin'][$name_suffix]) ? $_POST[$name_prefix . '_mnswmc_profit_margin'][$name_suffix] : '';
    $formula_id = isset($_POST[$name_prefix . '_mnswmc_formula_id'][$name_suffix]) ? $_POST[$name_prefix . '_mnswmc_formula_id'][$name_suffix] : '';
    $formula = isset($_POST[$name_prefix . '_mnswmc_formula'][$name_suffix]) ? $_POST[$name_prefix . '_mnswmc_formula'][$name_suffix] : '';
    $variables = isset($_POST[$name_prefix . '_mnswmc_variables'][$name_suffix]) ? $_POST[$name_prefix . '_mnswmc_variables'][$name_suffix] : '';
    $variables_counter = isset($_POST[$name_prefix . '_mnswmc_variables_counter'][$name_suffix]) ? $_POST[$name_prefix . '_mnswmc_variables_counter'][$name_suffix] : '';

    // check dependence
    if( !empty($dependence_type) and in_array($dependence_type, array('simple', 'advanced')) ) {
        update_post_meta( $object_id, '_mnswmc_dependence_type', sanitize_text_field( $dependence_type ) );
    } else {
        update_post_meta( $object_id, '_mnswmc_dependence_type', 'simple' );
    }

    // check price rounding
    if( !empty($price_rounding) and is_numeric($price_rounding) and $price_rounding >= '0' ) {
        update_post_meta( $object_id, '_mnswmc_price_rounding', sanitize_text_field( $price_rounding ) );
    } else {
        update_post_meta( $object_id, '_mnswmc_price_rounding', '0' );
    }

    // check rounding type
    if( !empty($rounding_type) and in_array($rounding_type, array('zero', 'nine', 'hybrid')) ) {
        update_post_meta( $object_id, '_mnswmc_rounding_type', sanitize_text_field( $rounding_type ) );
    } else {
        update_post_meta( $object_id, '_mnswmc_rounding_type', 'zero' );
    }

    // check rounding side
    if( !empty($rounding_side) and in_array($rounding_side, array('close', 'up', 'down')) ) {
        update_post_meta( $object_id, '_mnswmc_rounding_side', sanitize_text_field( $rounding_side ) );
    } else {
        update_post_meta( $object_id, '_mnswmc_rounding_side', 'close' );
    }

    // check dependence to do somethings
    if($dependence_type == 'simple') {

        // check currency id
        if( !empty($currency_id) and is_numeric($currency_id) ) {
            update_post_meta( $object_id, '_mnswmc_currency_id', sanitize_text_field( $currency_id ) );
        } else {
            mnswmc_delete_all_product_price_fields($object_id);
            return;
        }

        // check regular price
        if( !empty($regular_price) and is_numeric($regular_price) and $regular_price > 0 ) {
            update_post_meta( $object_id, '_mnswmc_regular_price', sanitize_text_field( $regular_price ) );
        } else {
            update_post_meta( $object_id, '_mnswmc_regular_price', '0' );
        }

        // check sale price
        if( isset($sale_price) and is_numeric($sale_price) and $sale_price >= 0 and $sale_price < mnswmc_get_product_regular_price($object_id) ) {
            update_post_meta( $object_id, '_mnswmc_sale_price', sanitize_text_field( $sale_price ) );
        } else {
            delete_post_meta( $object_id, '_mnswmc_sale_price' );
        }

        // check profit type
        if( !empty($profit_type) and in_array($profit_type, array('percent', 'fixed')) ) {
            update_post_meta( $object_id, '_mnswmc_profit_type', sanitize_text_field( $profit_type ) );
        } else {
            update_post_meta( $object_id, '_mnswmc_profit_type', 'fixed' );
        }

        // check profit margin
        if( !empty($profit_margin) and is_numeric($profit_margin) ) {

            if(mnswmc_get_product_profit_type($object_id) == 'percent') {
                if($profit_margin < -100)
                    $profit_margin = -100;
            }
            update_post_meta( $object_id, '_mnswmc_profit_margin', sanitize_text_field( $profit_margin ) );

        } else {
            update_post_meta( $object_id, '_mnswmc_profit_margin', '0' );
        }

        mnswmc_delete_all_product_price_fields($object_id, 'advanced');

    } elseif($dependence_type == 'advanced') {

        // check formula id
        if( !empty($formula_id) and is_numeric($formula_id) ) {
            update_post_meta( $object_id, '_mnswmc_formula_id', sanitize_text_field( $formula_id ) );
        } else {
            update_post_meta( $object_id, '_mnswmc_formula_id', '0' );
        }

        // check formula
        if( !empty($formula) ) {
            update_post_meta( $object_id, '_mnswmc_formula', sanitize_textarea_field( trim($formula) ) );
        } else {
            update_post_meta( $object_id, '_mnswmc_formula', '' );
        }

        // check variables
        if( !empty($variables) and is_array($variables) ) {
            foreach($variables as $key => $value) {

                if(!isset($value['value']) or !isset($value['sale']))
                    continue;

                $variables[$key]['value'] = is_numeric($value['value']) ? sanitize_text_field($value['value']) : '0';
                $variables[$key]['sale'] = is_numeric($value['sale']) ? sanitize_text_field($value['sale']) : '';
            }
            update_post_meta( $object_id, '_mnswmc_variables', $variables );
        } else {
            delete_post_meta( $object_id, '_mnswmc_variables' );
        }

        // check variables counter
        if( !empty($variables_counter) and is_numeric($variables_counter) and $variables_counter >= 1 ) {
            update_post_meta( $object_id, '_mnswmc_variables_counter', sanitize_text_field( $variables_counter ) );
        } else {
            update_post_meta( $object_id, '_mnswmc_variables_counter', '1' );
        }

        mnswmc_delete_all_product_price_fields($object_id, 'simple');

    } else {
        mnswmc_delete_all_product_price_fields($object_id);
        return;
    }

    do_action( 'mnswmc_after_product_fields_save', $post_type, $object_id, $loop );

    mnswmc_update_product_obj_price($object_id);
    return;
}

function mnswmc_add_custom_general_fields() {

    global $woocommerce, $post;
    mnswmc_product_fields('product', get_the_ID());
    return;
}
add_action( 'woocommerce_product_options_general_product_data', 'mnswmc_add_custom_general_fields' );
//add_action( 'woocommerce_product_options_pricing', 'mnswmc_add_custom_general_fields' );

function mnswmc_add_custom_variation_fields( $loop, $variation_data, $variation ) {
    mnswmc_product_fields('variation', $variation->ID, $loop, $variation_data, $variation);
    return;
}
add_action( 'woocommerce_variation_options_pricing', 'mnswmc_add_custom_variation_fields', 10, 3 );
/*
add_action( 'woocommerce_variation_options', 'mnswmc_add_custom_variation_fields', 10, 3 ); // After variation Enabled/Downloadable/Virtual/Manage Stock checkboxes
add_action( 'woocommerce_variation_options_pricing', 'mnswmc_add_custom_variation_fields', 10, 3 ); // After Price fields
add_action( 'woocommerce_variation_options_inventory', 'mnswmc_add_custom_variation_fields', 10, 3 ); // After Manage Stock fields
add_action( 'woocommerce_variation_options_dimensions', 'mnswmc_add_custom_variation_fields', 10, 3 ); // After Weight/Dimension fields
add_action( 'woocommerce_variation_options_tax', 'mnswmc_add_custom_variation_fields', 10, 3 ); // After Shipping/Tax Class fields
add_action( 'woocommerce_variation_options_download', 'mnswmc_add_custom_variation_fields', 10, 3 ); // After Download fields
add_action( 'woocommerce_product_after_variable_attributes', 'mnswmc_add_custom_variation_fields', 10, 3 ); // After all Variation fields
*/

function mnswmc_add_custom_general_fields_save( $post_id ){

    mnswmc_product_fields_save('product', $post_id);
    return;
}
add_action( 'woocommerce_process_product_meta', 'mnswmc_add_custom_general_fields_save', 99999, 1 );

function mnswmc_add_custom_variation_fields_save( $variation_id, $i ){

    mnswmc_product_fields_save('variation', $variation_id, $i);
    return;
}
add_action( 'woocommerce_save_product_variation', 'mnswmc_add_custom_variation_fields_save', 10, 2 );


// Save product price relations
function mnswmc_update_product_price_relations( $post_id ) {

    // Stop the script when doing autosave
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;

    // Stop the script if the user does not have edit permissions
    if( !current_user_can( 'edit_post', $post_id ) )
        return;

    if(get_post_type($post_id) != 'product')
        return;

    $product = wc_get_product( $post_id );

    if($product->is_type( 'simple' )) {

        $currency_ids = array(mnswmc_get_product_currency_id($post_id));
        $formula_ids = array(mnswmc_get_product_formula_id($post_id));

    } elseif($product->is_type( 'variable' )) {

        mnswmc_delete_all_product_price_fields($post_id);

        $variations = $product->get_available_variations();

        foreach ($variations as $key => $variation) {

            mnswmc_convert_delete_old_metas($variation['variation_id']);

            $currency_ids[] = mnswmc_get_product_currency_id($variation['variation_id']);
            $formula_ids[] = mnswmc_get_product_formula_id($variation['variation_id']);
        }
    } else {
        return;
    }

    update_post_meta( $post_id, '_mnswmc_currency_ids', json_encode($currency_ids) );
    update_post_meta( $post_id, '_mnswmc_formula_ids', json_encode($formula_ids) );
}
add_action( 'save_post', 'mnswmc_update_product_price_relations', 9999 );
