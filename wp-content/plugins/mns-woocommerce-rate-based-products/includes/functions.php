<?php

if (!defined('ABSPATH')) {
    exit;
}

require_once(MNSWMC_DIR . '/includes/functions/options.php');
require_once(MNSWMC_DIR . '/includes/functions/telegram.php');
require_once(MNSWMC_DIR . '/includes/formula-parser.php');

function mnswmc_fa_num_to_en($num)
{

    $fa_nums = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
    $en_nums = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');

    $en = str_replace($fa_nums, $en_nums, $num);
    return $en;
}

function mnswmc_en_num_to_fa($num)
{

    $fa_nums = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
    $en_nums = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');

    $en = str_replace($en_nums, $fa_nums, $num);
    return $en;
}

function mnswmc_calculate_change($from, $to)
{

    if (!is_numeric($from) or !is_numeric($to))
        return 0;

    $change = $to / $from;
    $change = $change - 1;
    $change = $change * 100;

    return $change;
}

function mnswmc_is_decimal($val)
{

    //return fmod($val, 1) !== 0.00;
    return is_numeric($val) and floor($val) != $val;
}

function mnswmc_dynamic_decimal($value)
{

    if (!is_numeric($value))
        return false;

    $val = abs(floatval($value));

    if ($val >= 1000000) $decimal = 0;
    elseif ($val >= 1 or $val == 0) $decimal = 2;
    elseif ($val < 1 and $val >= 0.1) $decimal = 3;
    elseif ($val < 0.1 and $val >= 0.01) $decimal = 4;
    elseif ($val < 0.01 and $val >= 0.001) $decimal = 5;
    elseif ($val < 0.001 and $val >= 0.0001) $decimal = 6;
    elseif ($val < 0.0001 and $val >= 0.00001) $decimal = 7;
    else $decimal = 8;

    return $decimal;
}

// Usually for sanitize numbers in backend
function mnswmc_sanitize_number($number, $decimals = null)
{

    if ($decimals == null) {
        if (!mnswmc_is_decimal($number)) {
            $decimals = 0;
        } else {
            $decimals = mnswmc_dynamic_decimal(floatval($number));
        }
    }

    return number_format(floatval($number), $decimals, '.', '');
}

// Usually for display price
function mnswmc_display_number($number)
{

    if (!is_numeric($number))
        return false;

    $decimals = get_option('woocommerce_price_num_decimals');
    $decimal_sep = get_option('woocommerce_price_decimal_sep');
    $thousand_sep = get_option('woocommerce_price_thousand_sep');

    return number_format(floatval($number), $decimals, $decimal_sep, $thousand_sep);
}

// Usually for display rate
function mnswmc_display_decimal_number($number)
{

    if (!is_numeric($number))
        return false;

    if (!mnswmc_is_decimal($number)) {
        $decimals = 0;
    } else {
        $decimals = mnswmc_dynamic_decimal(floatval($number));
    }

    $thousand_sep = get_option('woocommerce_price_thousand_sep');

    return number_format(floatval($number), $decimals, '.', $thousand_sep);
}

function mnswmc_round_number($number, $round = 0, $type = 'zero', $side = 'close')
{

    if ($round == 0)
        return $number;

    switch ($type) {
        case 'zero':

            if ($side == 'close') {
                $number = $round * round($number / $round);
            } elseif ($side == 'up') {
                $number = $round * ceil($number / $round);
            } elseif ($side == 'down') {
                $number = $round * floor($number / $round);
            }
            break;

        case 'nine':

            if ($side == 'close') {
                $number = ($round * 10 * round($number / ($round * 10))) - 1;
            } elseif ($side == 'up') {
                $number = ($round * 10 * ceil($number / ($round * 10))) - 1;
            } elseif ($side == 'down') {
                $number = ($round * 10 * floor($number / ($round * 10))) - 1;
            }
            break;

        case 'hybrid':

            if ($side == 'close') {
                $number = ($round * 10 * round($number / ($round * 10))) - $round;
            } elseif ($side == 'up') {
                $number = ($round * 10 * ceil($number / ($round * 10))) - $round;
            } elseif ($side == 'down') {
                $number = ($round * 10 * floor($number / ($round * 10))) - $round;
            }
            break;

        default:
            break;
    }

    return $number;

}

function mnswmc_apply_margin($value, $margin, $type)
{

    switch ($type) {
        case 'percent':

            if ($margin < -100)
                $margin = -100;

            $margin = ($margin / 100) + 1;
            $value = $value * $margin;
            break;

        case 'fixed':

            if ($margin < 0)
                $margin = 0;

            $value = $value + $margin;
            break;

        default:
            break;
    }

    return $value;
}

// Usually for convert IRR to other Iranian currencies. Return denominator
function mnswmc_get_currency_denominator()
{

    $currency = get_woocommerce_currency();

    switch ($currency) {
        case 'IRR':
            $currency_denominator = 1;
            break;
        case 'IRHR':
            $currency_denominator = 1000;
            break;
        case 'IRT':
            $currency_denominator = 10;
            break;
        case 'IRHT':
            $currency_denominator = 10000;
            break;
        default:
            $currency_denominator = 1;
            break;
    }
    return $currency_denominator;
}

// Usually for convert IRR to other Iranian currencies. Return divided price
function mnswmc_currency_division($price)
{

    if (!is_numeric($price))
        return false;

    $denominator = mnswmc_get_currency_denominator();
    $price = mnswmc_sanitize_number($price / $denominator);

    return $price;
}

function mnswmc_price_rounding_items()
{

    $rounding_items = array(1000, 10000, 100000, 1000000, 10000000);
    $rounding_items = apply_filters('mnswmc_price_rounding_items', $rounding_items);
    $rounding_items = array_map('mnswmc_currency_division', $rounding_items);

    $rounding_array[0] = '---';

    foreach ($rounding_items as $value) {
        $rounding_array[$value] = mnswmc_display_number($value);
    }

    return $rounding_array;
}

function mnswmc_get_currencies($c = array())
{

    $currencies_args = array(
        'post_type' => 'mnswmc',
        'post_status' => 'publish',
        'include' => $c,
        'nopaging' => true
    );
    $currencies = get_posts($currencies_args);
    return $currencies;
}

function mnswmc_get_formulas($f = array())
{

    $formulas_args = array(
        'post_type' => 'mnswmc-formula',
        'post_status' => 'publish',
        'include' => $f,
        'nopaging' => true
    );
    $formulas = get_posts($formulas_args);
    return $formulas;
}

function mnswmc_currencies_list($currencies, $id = '', $class = '', $name = '', $currency_id = null, $none_text = '---')
{

    if (is_array($currencies) and count($currencies) > 0) {

        echo '<select id="' . $id . '" name="' . $name . '" class="' . $class . '">';
        echo '<option value="0" data-rate="0">' . $none_text . '</option>';

        foreach ($currencies as $currency) {
            echo '<option value="' . $currency->ID . '" data-rate="' . mnswmc_get_currency_rate($currency->ID) . '" ' . selected($currency->ID, $currency_id, false) . '>' . $currency->post_title . '</option>';
        }

        echo '</select>';
    } else {
        echo sprintf(wp_kses(__('You have not created any currencies! <a href="%s">Create</a> now', 'mnswmc'), array('a' => array('href' => array()))), esc_url(admin_url('post-new.php?post_type=mnswmc')));
    }
}

function mnswmc_formulas_list($formulas, $id = '', $class = '', $name = '', $formula_id = null, $none_text = null)
{

    $none_text = ($none_text) ? $none_text : __('Custom formula', 'mnswmc');

    echo '<select id="' . $id . '" name="' . $name . '" class="' . $class . '">';
    echo '<option value="0">' . $none_text . '</option>';

    if (is_array($formulas) and count($formulas) > 0) {
        foreach ($formulas as $formula) {
            echo '<option value="' . $formula->ID . '" ' . ($name == '' ? '' : selected($formula->ID, $formula_id, false)) . '>' . $formula->post_title . '</option>';
        }
    }

    echo '</select>';
}

// param1: object id, param2: product|product_variation
function mnswmc_delete_all_product_price_fields($post_id, $dependence = false)
{

    if ($dependence == false) {
        delete_post_meta($post_id, '_mnswmc_active');
        delete_post_meta($post_id, '_mnswmc_price_alert');
        delete_post_meta($post_id, '_mnswmc_dependence_type');
        delete_post_meta($post_id, '_mnswmc_price_rounding');
        delete_post_meta($post_id, '_mnswmc_rounding_type');
        delete_post_meta($post_id, '_mnswmc_rounding_side');
        delete_post_meta($post_id, '_variable_mnswmc_active');
        delete_post_meta($post_id, '_variable_mnswmc_dependence_type');
        delete_post_meta($post_id, '_variable_mnswmc_price_rounding');
        delete_post_meta($post_id, '_variable_mnswmc_rounding_type');
        delete_post_meta($post_id, '_variable_mnswmc_rounding_side');
    }

    if ($dependence == false or $dependence == 'advanced') {
        delete_post_meta($post_id, '_mnswmc_formula_id');
        delete_post_meta($post_id, '_mnswmc_formula');
        delete_post_meta($post_id, '_mnswmc_variables');
        delete_post_meta($post_id, '_mnswmc_variables_counter');
        delete_post_meta($post_id, '_variable_mnswmc_formula_id');
        delete_post_meta($post_id, '_variable_mnswmc_formula');
        delete_post_meta($post_id, '_variable_mnswmc_variables');
        delete_post_meta($post_id, '_variable_mnswmc_variables_counter');
    }

    if ($dependence == false or $dependence == 'simple') {
        delete_post_meta($post_id, '_mnswmc_currency_id');
        delete_post_meta($post_id, '_mnswmc_regular_price');
        delete_post_meta($post_id, '_mnswmc_sale_price');
        delete_post_meta($post_id, '_mnswmc_profit_margin');
        delete_post_meta($post_id, '_mnswmc_profit_type');
        delete_post_meta($post_id, '_variable_mnswmc_currency_id');
        delete_post_meta($post_id, '_variable_mnswmc_regular_price');
        delete_post_meta($post_id, '_variable_mnswmc_sale_price');
        delete_post_meta($post_id, '_variable_mnswmc_profit_margin');
        delete_post_meta($post_id, '_variable_mnswmc_profit_type');
    }
}

// var key, value type (value or sale), product formula variables, variable defaults
function mnswmc_get_formula_value($key, $value_type = 'value', $variables = array(), $defaults = array())
{

    if (isset($variables[$key]['value'])) {
        // use own val
        if (isset($variables[$key][$value_type]) and is_numeric($variables[$key][$value_type])) {
            $value = $variables[$key][$value_type];
        } elseif (isset($variables[$key]['value']) and is_numeric($variables[$key]['value'])) {
            $value = $variables[$key]['value'];
        } else {
            $value = 0;
        }
    } else {
        // use default val
        if (isset($defaults[$value_type]) and is_numeric($defaults[$value_type])) {
            $value = $defaults[$value_type];
        } elseif (isset($defaults['value']) and is_numeric($defaults['value'])) {
            $value = $defaults['value'];
        } else {
            $value = 0;
        }
    }

    return $value;
}

function mnswmc_execute_formula($object_id, $meta_key = '_regular_price')
{

    $variable_index = ($meta_key == '_regular_price') ? 'value' : 'sale';

    $formula_id = mnswmc_get_product_formula_id($object_id);
    $own_variables = mnswmc_get_product_formula_variables($object_id);

    if ($formula_id == '0') {
        $formula = mnswmc_get_product_formula_text($object_id);
        $variables = $own_variables;
    } else {
        $formula_data = mnswmc_get_formula_data($formula_id);
        $formula = $formula_data['formul'];
        $variables = $formula_data['variables'];
    }

    if ($formula == '')
        return null;

    $rates = mnswmc_get_currency_rates();

    foreach ($variables as $key => $variable) {

        preg_match('/^\d+/', $key, $matches);

        if (!is_array($matches) or !isset($matches[0]))
            continue;

        if (!is_numeric($matches[0]))
            continue;

        if ($matches[0] == '0') {
            $rate = ($formula_id != '0' and isset($variable['rate'])) ? $variable['rate'] : '1';
        } else {
            $rate = isset($rates[$matches[0]]) ? $rates[$matches[0]] : mnswmc_get_currency_rate($matches[0]);
        }

        if (!is_numeric($rate) or $rate <= 0)
            $rate = 1;

        $value = mnswmc_get_formula_value($key, $variable_index, $own_variables, $variable);

        //$formula = str_replace('{' . $key . '}', mnswmc_sanitize_number($rate * $value), $formula);// bug on very decimal values
        $formula = str_replace('{' . $key . '}', ($rate * $value), $formula);
    }

    setlocale(LC_NUMERIC, 'en_US');

    $formula_parser = new FormulaParser\FormulaParser($formula);
    $result = $formula_parser->getResult();

    setlocale(LC_NUMERIC, get_locale());

    if (is_array($result) and isset($result[0]) and $result[0] == 'done')
        return mnswmc_sanitize_number($result[1]);

    return null;
}

// still needs $post_type to run converter (part of convert project!)
function mnswmc_is_active($object, $post_type)
{

    if (!mnswmc_check_activation())
        return false;

    if (get_post_meta($object->get_id(), '_mnswmc_active', true) != 'yes') {

        // check old version
        if ($post_type == 'product')
            return false;

        $value = get_post_meta($object->get_id(), '_variable_mnswmc_active', true);

        if ($value == 'yes') {
            mnswmc_convert_five_product_metas($object->get_id());
            return true;
        } else {
            return false;
        }
    }

    return true;
}

function mnswmc_generate_rate_based_price($object_id, $meta_key = '_regular_price')
{

    $dependence = mnswmc_get_product_dependence_type($object_id);

    if ($dependence == 'advanced') {

        $price = mnswmc_execute_formula($object_id, $meta_key);

        if ($price == null)
            return null;

    } elseif ($dependence == 'simple') {

        $regular_price = mnswmc_get_product_regular_price($object_id);
        $sale_price = mnswmc_get_product_sale_price($object_id);
        $currency_id = mnswmc_get_product_currency_id($object_id);
        $profit_margin = mnswmc_get_product_profit_margin($object_id);
        $profit_type = mnswmc_get_product_profit_type($object_id);
        $price = ($meta_key == '_regular_price') ? $regular_price : $sale_price;

        if ($price == null)
            return null;

        $currency_rate = mnswmc_get_currency_rate($currency_id);
        $price = mnswmc_apply_margin(($price * $currency_rate), $profit_margin, $profit_type);

    } else {

        return null;
    }

    $global_rounding = mnswmc_get_option_global_rounding();

    $rounding_value = mnswmc_get_product_rounding_value($object_id);
    $rounding_type = mnswmc_get_product_rounding_type($object_id);
    $rounding_side = mnswmc_get_product_rounding_side($object_id);

    if ($rounding_value == '0' and $global_rounding['value'] != '0') {
        $rounding_value = $global_rounding['value'];
        $rounding_type = $global_rounding['type'];
        $rounding_side = $global_rounding['side'];
    }

    $price = mnswmc_round_number($price, $rounding_value, $rounding_type, $rounding_side);

    if ($price < 0)
        $price = 0;

    return strval($price);
}

function mnswmc_update_product_obj_price($object_id)
{

    if (!mnswmc_check_activation())
        return;

    // Get an instance of the WC_Product object
    $product = wc_get_product($object_id);

    $price = mnswmc_generate_rate_based_price($object_id, '_regular_price');
    $sale_price = mnswmc_generate_rate_based_price($object_id, '_sale_price');

    $product->set_regular_price($price);
    $product->set_sale_price($sale_price);
    // $product->set_price($price);

    $product->save();

    wc_delete_product_transients($object_id);
}

function mnswmc_is_on_sale($product)
{

    $regular_price = mnswmc_generate_rate_based_price($product->get_id(), '_regular_price');
    $sale_price = mnswmc_generate_rate_based_price($product->get_id(), '_sale_price');

    if ($sale_price and $regular_price > $sale_price) {

        $on_sale = true;

        if ($product->get_date_on_sale_from() and $product->get_date_on_sale_from()->getTimestamp() > current_time('timestamp', true)) {
            $on_sale = false;
        }
        if ($product->get_date_on_sale_to() and $product->get_date_on_sale_to()->getTimestamp() < current_time('timestamp', true)) {
            $on_sale = false;
        }
    } else {
        $on_sale = false;
    }

    return $on_sale;
}

function mnswmc_delete_sale_meta($product_id)
{

    $dependence = mnswmc_get_product_dependence_type($product_id);

    if ($dependence == 'simple') {

        delete_post_meta($product_id, '_mnswmc_sale_price');

    } elseif ($dependence == 'advanced') {

        $variables = mnswmc_get_product_formula_variables($product_id);

        foreach ($variables as $key => $variable) {
            $variables[$key]['sale'] = '';
        }

        update_post_meta($product_id, '_mnswmc_variables', $variables);
    }
}

function mnswmc_end_sale($product_ids)
{

    foreach ($product_ids as $product_id) {

        mnswmc_delete_sale_meta($product_id);
    }
}

add_action('wc_before_products_ending_sales', 'mnswmc_end_sale', 99, 1);

function mnswmc_get_raw_rate_from_webservice($currency_id, $value, $update_type, $lowest_rate, $relation, $connection)
{

    if ($update_type == 'none')
        return $value;

    $webservices = mnswmc_get_webservices();

    if (!isset($webservices[$update_type]))
        return $value;

    $webservice = $webservices[$update_type];
    $api_data = mnswmc_get_option_webservice_data($update_type);

    if (!isset($api_data[$relation]['price']) or !is_numeric($api_data[$relation]['price']))
        return $value;

    $rate = esc_attr($api_data[$relation]['price']);

    if ($webservice['connection'] and is_numeric($connection) and $connection != $currency_id) {

        $connection_rate = mnswmc_get_currency_rate($connection);
        $rate = mnswmc_sanitize_number($rate) * $connection_rate;

    } else {

        $rate = mnswmc_currency_division($rate);
    }

    return $rate;
}

function mnswmc_update_currency_rate($currency_id)
{

    if (!mnswmc_check_activation())
        return false;

    $rates = mnswmc_get_currency_rates();
    $rates[$currency_id] = isset($rates[$currency_id]) ? $rates[$currency_id] : null;

    $value = mnswmc_get_currency_value($currency_id);
    $profit = mnswmc_get_currency_profit($currency_id);
    $fee = mnswmc_get_currency_fee($currency_id);
    $fixed = mnswmc_get_currency_fixed($currency_id);
    $ratio = mnswmc_get_currency_ratio($currency_id);
    $update_type = mnswmc_get_currency_update_type($currency_id);
    $lowest_rate = mnswmc_get_currency_lowest_rate($currency_id);
    $relation = mnswmc_get_currency_relation($currency_id);
    $connection = mnswmc_get_currency_connection($currency_id);

    $raw = mnswmc_get_raw_rate_from_webservice($currency_id, $value, $update_type, $lowest_rate, $relation, $connection);

    $profit = (($profit + $fee) / 100) + 1;

    $rate = mnswmc_sanitize_number($raw * $ratio * $profit);
    $rate = $rate + $fixed;

    if ($rate < $lowest_rate)
        $rate = $lowest_rate;

    $rate = apply_filters('mnswmc_currency_rate_update', $rate, $currency_id);

    do_action('mnswmc_currency_rate_before_update', $currency_id, $rate);

    if ($rates[$currency_id] == $rate)
        return;

    update_post_meta($currency_id, '_mnswmc_currency_value', mnswmc_sanitize_number($raw));
    update_post_meta($currency_id, '_mnswmc_currency_rate', $rate);

    // Update rates option
    $rates[$currency_id] = $rate;
    update_option('mnswmc_currency_rates', $rates);

    // Update connections
    if (!$connection) {

        $connected_currencies = get_posts(array(
            'post_type' => 'mnswmc',
            'post_status' => 'publish',
            'post__not_in' => array($currency_id),
            'nopaging' => true,
            'meta_query' => array(array(
                'key' => '_mnswmc_currency_connection',
                'value' => $currency_id
            ))
        ));

        foreach ($connected_currencies as $connected_currency) {
            mnswmc_update_currency_rate($connected_currency->ID);
        }
    }

    do_action('mnswmc_currency_rate_updated', $currency_id, $rate);

    return $rate;
}

function mnswmc_get_currency_value($currency_id)
{

    $value = esc_attr(get_post_meta($currency_id, '_mnswmc_currency_value', true));

    if (empty($value) or !is_numeric($value) or $value <= 0)
        $value = 1;

    return $value;
}

function mnswmc_get_currency_lowest_rate($currency_id)
{

    $lowest_rate = esc_attr(get_post_meta($currency_id, '_mnswmc_currency_lowest_rate', true));

    if (empty($lowest_rate) or !is_numeric($lowest_rate) or $lowest_rate < 0)
        $lowest_rate = 0;

    return $lowest_rate;
}

function mnswmc_get_currency_profit($currency_id)
{

    $profit = esc_attr(get_post_meta($currency_id, '_mnswmc_currency_profit', true));

    if (empty($profit) or !is_numeric($profit))
        $profit = '0.00';

    if ($profit < -100)
        $profit = -100;

    return $profit;
}

function mnswmc_get_currency_fee($currency_id)
{

    $fee = esc_attr(get_post_meta($currency_id, '_mnswmc_currency_fee', true));

    if (empty($fee) or !is_numeric($fee))
        $fee = '0.00';

    if ($fee < -100)
        $fee = -100;

    return $fee;
}

function mnswmc_get_currency_fixed($currency_id)
{

    $fixed = esc_attr(get_post_meta($currency_id, '_mnswmc_currency_fixed', true));

    if (empty($fixed) or !is_numeric($fixed))
        $fixed = '0';

    return $fixed;
}

function mnswmc_get_currency_ratio($currency_id)
{

    $ratio = esc_attr(get_post_meta($currency_id, '_mnswmc_currency_ratio', true));

    if (empty($ratio) or !is_numeric($ratio))
        $ratio = '1.00';

    if ($ratio <= 0)
        $ratio = '1.00';

    return $ratio;
}

function mnswmc_get_currency_update_type($currency_id)
{

    $update_type = esc_attr(get_post_meta($currency_id, '_mnswmc_currency_update_type', true));

    if (empty($update_type))
        $update_type = 'none';

    return $update_type;
}

function mnswmc_get_currency_relation($currency_id)
{

    $relation = esc_attr(get_post_meta($currency_id, '_mnswmc_currency_relation', true));

    if (empty($relation))
        $relation = esc_attr(get_post_meta($currency_id, '_mnswmc_currency_rel_to', true));

    return $relation;
}

function mnswmc_get_currency_connection($currency_id)
{

    $connection = esc_attr(get_post_meta($currency_id, '_mnswmc_currency_connection', true));

    if (empty($connection) or get_post_type($connection) != 'mnswmc')
        $connection = false;

    return $connection;
}

function mnswmc_get_currency_rate($currency_id)
{

    /*
    * 3.5.0
    * get rates option
    */

    $rates = mnswmc_get_currency_rates();

    if (isset($rates[$currency_id]) and !empty($rates[$currency_id]) and is_numeric($rates[$currency_id]))
        return $rates[$currency_id];

    $rate = esc_attr(get_post_meta($currency_id, '_mnswmc_currency_rate', true));

    if (empty($rate) or !is_numeric($rate) or $rate <= 0)
        $rate = mnswmc_get_currency_value($currency_id);

    $rates[$currency_id] = $rate;
    update_option('mnswmc_currency_rates', $rates);

    return $rate;
}

function mnswmc_get_currency_update_time($currency_id)
{

    $time = get_post_meta($currency_id, '_mnswmc_currency_update_time', true);
    $time = is_numeric($time) ? $time : 0;

    return $time;
}

function mnswmc_update_currency_update_time($currency_id, $time)
{

    update_post_meta($currency_id, '_mnswmc_currency_update_time', $time);
}

add_action('mnswmc_currency_rate_updated', function ($currency_id, $rate) {

    mnswmc_update_currency_update_time($currency_id, time());

}, 10, 2);

function mnswmc_get_currency_max_records()
{

    return apply_filters('mnswmc_currency_max_records', 30);
}

function mnswmc_get_currency_records($currency_id)
{

    $records = get_post_meta($currency_id, '_mnswmc_currency_records', true);
    $records = is_array($records) ? $records : array();

    return $records;
}

function mnswmc_update_currency_records($currency_id, $time, $values)
{

    // re-create ts for day
    $time = strtotime(date("Y-m-d", $time));

    $records = mnswmc_get_currency_records($currency_id);

    if (isset($records[$time])) {

        $records[$time] = $values;

    } else {

        $records = array($time => $values) + $records;

        if (count($records) > mnswmc_get_currency_max_records()) {
            array_pop($records);
        }
    }

    update_post_meta($currency_id, '_mnswmc_currency_records', $records);

    return $records;
}

function mnswmc_get_currency_prev_record($currency_id)
{

    $records = mnswmc_get_currency_records($currency_id);
    $records_times = array_keys($records);

    $last_value = isset($records_times[1], $records[$records_times[1]]['value']) ? $records[$records_times[1]]['value'] : false;
    $last_rate = isset($records_times[1], $records[$records_times[1]]['rate']) ? $records[$records_times[1]]['rate'] : false;

    return array(
        'value' => $last_value,
        'rate' => $last_rate
    );
}

add_action('mnswmc_currency_rate_before_update', function ($currency_id, $rate) {

    mnswmc_update_currency_records($currency_id, time(), array(
        'value' => mnswmc_get_currency_value($currency_id),
        'profit' => mnswmc_get_currency_profit($currency_id),
        'fee' => mnswmc_get_currency_fee($currency_id),
        'fixed' => mnswmc_get_currency_fixed($currency_id),
        'ratio' => mnswmc_get_currency_ratio($currency_id),
        'lowest_rate' => mnswmc_get_currency_lowest_rate($currency_id),
        'rate' => $rate,
    ));

}, 10, 2);

function mnswmc_get_currency_rates()
{

    $rates = get_option('mnswmc_currency_rates', array());
    $rates = is_array($rates) ? $rates : array();

    return $rates;
}

function mnswmc_get_formula_formul($formula_id)
{

    $formul = esc_attr(get_post_meta($formula_id, '_mnswmc_formula_formul', true));

    if (empty($formul))
        $formul = '';

    return $formul;
}

function mnswmc_get_formula_variables($formula_id)
{

    $variables = get_post_meta($formula_id, '_mnswmc_formula_variables', true);

    if (!is_array($variables))
        $variables = array();

    return $variables;
}

function mnswmc_get_formula_variables_counter($formula_id)
{

    $counter = esc_attr(get_post_meta($formula_id, '_mnswmc_formula_variables_counter', true));

    if (!is_numeric($counter) or $counter < 1)
        $counter = '1';

    return $counter;
}

function mnswmc_get_formula_data($formula_id)
{

    $data = get_option('mnswmc_formula_data', array());
    $data = is_array($data) ? $data : array();
    $changed = false;

    if (!isset($data[$formula_id]['formul']) or empty($data[$formula_id]['formul'])) {
        $data[$formula_id]['formul'] = mnswmc_get_formula_formul($formula_id);
        $changed = true;
    }

    if (!isset($data[$formula_id]['variables']) or !is_array($data[$formula_id]['variables'])) {
        $data[$formula_id]['variables'] = mnswmc_get_formula_variables($formula_id);
        $changed = true;
    }

    if (!isset($data[$formula_id]['variables_counter']) or !is_numeric($data[$formula_id]['variables_counter'])) {
        $data[$formula_id]['variables_counter'] = mnswmc_get_formula_variables_counter($formula_id);
        $changed = true;
    }

    if ($changed)
        update_option('mnswmc_formula_data', $data);

    return $data[$formula_id];
}

function mnswmc_update_formula_data($formula_id)
{

    if (!mnswmc_check_activation())
        return false;

    $data = get_option('mnswmc_formula_data', array());
    $data = is_array($data) ? $data : array();

    $data[$formula_id]['formul'] = mnswmc_get_formula_formul($formula_id);
    $data[$formula_id]['variables'] = mnswmc_get_formula_variables($formula_id);
    $data[$formula_id]['variables_counter'] = mnswmc_get_formula_variables_counter($formula_id);

    update_option('mnswmc_formula_data', $data);

    return true;
}

// product

function mnswmc_get_product_active($product_id)
{

    $value = get_post_meta($product_id, '_mnswmc_active', true);
    $value = mnswmc_conver_if_not_check_variable($product_id, '_mnswmc_active', $value);
    $value = $value == 'yes' ? $value : false;

    return $value;
}

function mnswmc_get_product_price_alert($product_id)
{

    $value = get_post_meta($product_id, '_mnswmc_price_alert', true);
    $value = $value == 'yes' ? $value : false;

    return $value;
}

function mnswmc_get_product_dependence_type($product_id)
{

    $value = get_post_meta($product_id, '_mnswmc_dependence_type', true);
    $value = mnswmc_conver_if_not_check_variable($product_id, '_mnswmc_dependence_type', $value);

    if (!in_array($value, array('simple', 'advanced')))
        $value = 'simple';

    return $value;
}

function mnswmc_get_product_rounding_value($product_id)
{

    $value = get_post_meta($product_id, '_mnswmc_price_rounding', true);
    $value = mnswmc_conver_if_not_check_variable($product_id, '_mnswmc_price_rounding', $value);

    if (!is_numeric($value) or $value < 0)
        $value = 0;

    return $value;
}

function mnswmc_get_product_rounding_type($product_id)
{

    $value = get_post_meta($product_id, '_mnswmc_rounding_type', true);
    $value = mnswmc_conver_if_not_check_variable($product_id, '_mnswmc_rounding_type', $value);

    if (!in_array($value, array('zero', 'nine', 'hybrid')))
        $value = 'zero';

    return $value;
}

function mnswmc_get_product_rounding_side($product_id)
{

    $value = get_post_meta($product_id, '_mnswmc_rounding_side', true);
    $value = mnswmc_conver_if_not_check_variable($product_id, '_mnswmc_rounding_side', $value);

    if (!in_array($value, array('close', 'up', 'down')))
        $value = 'close';

    return $value;
}

function mnswmc_get_product_currency_id($product_id)
{

    $value = get_post_meta($product_id, '_mnswmc_currency_id', true);
    $value = mnswmc_conver_if_not_check_variable($product_id, '_mnswmc_currency_id', $value);

    if (!is_numeric($value))
        $value = 0;

    return $value;
}

function mnswmc_get_product_regular_price($product_id)
{

    $value = get_post_meta($product_id, '_mnswmc_regular_price', true);
    $value = mnswmc_conver_if_not_check_variable($product_id, '_mnswmc_regular_price', $value);

    if (!is_numeric($value))
        return null;

    if ($value < 0)
        $value = 0;

    return $value;
}

function mnswmc_get_product_sale_price($product_id)
{

    $value = get_post_meta($product_id, '_mnswmc_sale_price', true);
    $value = mnswmc_conver_if_not_check_variable($product_id, '_mnswmc_sale_price', $value);

    if (!is_numeric($value))
        return null;

    if ($value < 0)
        $value = 0;

    return $value;
}

function mnswmc_get_product_profit_margin($product_id)
{

    $value = get_post_meta($product_id, '_mnswmc_profit_margin', true);
    $value = mnswmc_conver_if_not_check_variable($product_id, '_mnswmc_profit_margin', $value);

    if (!is_numeric($value))
        $value = 0;

    return $value;
}

function mnswmc_get_product_profit_type($product_id)
{

    $value = get_post_meta($product_id, '_mnswmc_profit_type', true);
    $value = mnswmc_conver_if_not_check_variable($product_id, '_mnswmc_profit_type', $value);

    if (!in_array($value, array('fixed', 'percent')))
        $value = 'percent';

    return $value;
}

function mnswmc_get_product_formula_id($product_id)
{

    $value = get_post_meta($product_id, '_mnswmc_formula_id', true);
    $value = mnswmc_conver_if_not_check_variable($product_id, '_mnswmc_formula_id', $value);

    if (!is_numeric($value))
        return '0';

    return $value;
}

function mnswmc_get_product_formula_variables($product_id)
{

    $value = get_post_meta($product_id, '_mnswmc_variables', true);
    $value = mnswmc_conver_if_not_check_variable($product_id, '_mnswmc_variables', $value);
    $value = is_array($value) ? $value : array();
    return $value;
}

function mnswmc_get_product_formula_text($product_id)
{

    $value = get_post_meta($product_id, '_mnswmc_formula', true);
    $value = mnswmc_conver_if_not_check_variable($product_id, '_mnswmc_formula', $value);
    return $value;
}

function mnswmc_get_product_variables_counter($product_id)
{

    $value = get_post_meta($product_id, '_mnswmc_variables_counter', true);
    $value = mnswmc_conver_if_not_check_variable($product_id, '_mnswmc_variables_counter', $value);
    $value = ($value) ? $value : '1';
    return $value;
}
