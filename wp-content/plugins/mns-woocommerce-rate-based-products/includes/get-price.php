<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function mnswmc_check_product_regular_price($price, $product) {

	if(!mnswmc_is_active($product, 'product'))
		return $price;

	do_action( 'mnswmc_before_product_price_check', $product->get_id(), 'product', $price);

	$live_regular_price = mnswmc_generate_rate_based_price($product->get_id(), '_regular_price');
	$live_sale_price = mnswmc_generate_rate_based_price($product->get_id(), '_sale_price');
	$current_regular_price = get_post_meta($product->get_id(), '_regular_price', true);
	$current_sale_price = get_post_meta($product->get_id(), '_sale_price', true);

	if($live_sale_price == '' or $live_sale_price == null or $live_sale_price == false)
		$live_sale_price = null;

	if($live_sale_price == $live_regular_price)
		$live_sale_price = null;

	if($current_sale_price == '' or $current_sale_price == null or $current_sale_price == false)
		$current_sale_price = null;
	
	if($live_regular_price == $current_regular_price and $live_sale_price == $current_sale_price)
		return $price;

	if($live_regular_price != $current_regular_price) {
		$product->set_regular_price($live_regular_price);
	}

	if($live_sale_price != $current_sale_price) {
		$product->set_sale_price($live_sale_price);
	}

	do_action('mnswmc_product_price_updated', $product->get_id(), 'product', $live_regular_price, $live_sale_price);

	$product->save();
	wc_delete_product_transients( $product->get_id() );

	return $price;
}

function mnswmc_check_product_variation_regular_price($price, $product) {

	if(!mnswmc_is_active($product, 'product_variation'))
		return $price;

	do_action( 'mnswmc_before_product_price_check', $product->get_id(), 'product_variation', $price);

	$live_regular_price = mnswmc_generate_rate_based_price($product->get_id(), '_regular_price');
	$live_sale_price = mnswmc_generate_rate_based_price($product->get_id(), '_sale_price');
	$current_regular_price = get_post_meta($product->get_id(), '_regular_price', true);
	$current_sale_price = get_post_meta($product->get_id(), '_sale_price', true);

	if($live_sale_price == '' or $live_sale_price == null or $live_sale_price == false)
		$live_sale_price = null;

	if($live_sale_price == $live_regular_price)
		$live_sale_price = null;

	if($current_sale_price == '' or $current_sale_price == null or $current_sale_price == false)
		$current_sale_price = null;

	if($live_regular_price == $current_regular_price and $live_sale_price == $current_sale_price)
		return $price;

	if($live_regular_price != $current_regular_price) {
		$product->set_regular_price($live_regular_price);
	}
	
	if($live_sale_price != $current_sale_price) {
		$product->set_sale_price($live_sale_price);
	}

	do_action('mnswmc_product_price_updated', $product->get_id(), 'product_variation', $live_regular_price, $live_sale_price);

	$product->save();
	wc_delete_product_transients( $product->get_id() );
	wc_delete_product_transients( $product->get_parent_id() );

	return $price;
}

function mnswmc_variation_prices_regular_price( $price, $variation, $product ) {

	if(!mnswmc_is_active($variation, 'product_variation'))
		return $price;

	do_action( 'mnswmc_before_product_price_check', $variation->get_id(), 'product_variation', $price);

	$live_regular_price = mnswmc_generate_rate_based_price($variation->get_id(), '_regular_price');
	$live_sale_price = mnswmc_generate_rate_based_price($variation->get_id(), '_sale_price');
	$current_regular_price = get_post_meta($variation->get_id(), '_regular_price', true);
	$current_sale_price = get_post_meta($variation->get_id(), '_sale_price', true);

	if($live_sale_price == '' or $live_sale_price == null or $live_sale_price == false)
		$live_sale_price = null;

	if($live_sale_price == $live_regular_price)
		$live_sale_price = null;

	if($current_sale_price == '' or $current_sale_price == null or $current_sale_price == false)
		$current_sale_price = null;

	if($live_regular_price == $current_regular_price and $live_sale_price == $current_sale_price)
		return $price;

	if($live_regular_price != $current_regular_price) {
		$variation->set_regular_price($live_regular_price);
	}
	
	if($live_sale_price != $current_sale_price) {
		$variation->set_sale_price($live_sale_price);
	}

	$variation->save();
	wc_delete_product_transients( $variation->get_id() );
	wc_delete_product_transients( $variation->get_parent_id() );

	return $price;
}