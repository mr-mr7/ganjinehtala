<?php

if(!isset($_GET['webhookUrl']))
	exit();

$results = file_get_contents("php://input");
$webhook = $_GET['webhookUrl'];

file_get_contents($webhook . '?getUpdate=' . urlencode($results));