<?php

function mnswmc_api_get_data_parsijoo($webservice_id, $key = false) {
	
	$url_xml_fiat = 'http://parsijoo.ir/api?serviceType=price-API&query=Currency';
	$url_xml_gold = 'http://parsijoo.ir/api?serviceType=price-API&query=Gold';
	
	$file_xml_fiat = file_get_contents($url_xml_fiat);
	$file_xml_gold = file_get_contents($url_xml_gold);
	
	if(!$file_xml_fiat or !$file_xml_gold) {
		mnswmc_add_webservice_errors($webservice_id);
		return false;
	}
	
	$file_xml_fiat = simplexml_load_string($file_xml_fiat, "SimpleXMLElement", LIBXML_NOCDATA);
	$file_xml_gold = simplexml_load_string($file_xml_gold, "SimpleXMLElement", LIBXML_NOCDATA);
	
	if(!$file_xml_fiat or !$file_xml_gold) {
		mnswmc_add_webservice_errors($webservice_id);
		return false;
	}
	
	$data_json_fiat = json_encode($file_xml_fiat);
	$data_json_gold = json_encode($file_xml_gold);
	
	if(!$data_json_fiat or !$data_json_gold) {
		mnswmc_add_webservice_errors($webservice_id);
		return false;
	}
	
	$data_array_fiat = json_decode($data_json_fiat, true);
	$data_array_gold = json_decode($data_json_gold, true);
	
	if(!$data_array_fiat or !$data_array_gold) {
		mnswmc_add_webservice_errors($webservice_id);
		return false;
	}
	
	if(!isset($data_array_fiat['sadana-services']['price-service']['item']) or !isset($data_array_gold['sadana-services']['price-service']['item'])) {
		mnswmc_add_webservice_errors($webservice_id);
		return false;
	}
	
	if(!isset($data_array_fiat['sadana-services']['price-service']['status']) or $data_array_fiat['sadana-services']['price-service']['status'] != 'active') {
		mnswmc_add_webservice_errors($webservice_id);
		return false;
	}
	if(!isset($data_array_gold['sadana-services']['price-service']['status']) or $data_array_gold['sadana-services']['price-service']['status'] != 'active') {
		mnswmc_add_webservice_errors($webservice_id);
		return false;
	}
	
	$data = array_merge($data_array_fiat['sadana-services']['price-service']['item'], $data_array_gold['sadana-services']['price-service']['item']);

	foreach($data as $key => $value) {
		
		if(isset($value['price']) and isset($value['name'])) {
			$data[$key] = array('name' => sanitize_text_field($value['name']), 'price' => mnswmc_sanitize_number(str_replace(',', '', mnswmc_fa_num_to_en($value['price']))));
		} else {
			mnswmc_add_webservice_errors($webservice_id);
			return false;
			break;
		}
	}

	mnswmc_delete_webservice_errors($webservice_id);
	return $data;
}