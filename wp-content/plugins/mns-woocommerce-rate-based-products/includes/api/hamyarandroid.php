<?php

function mnswmc_api_get_data_hamyarandroid($webservice_id, $key = false) {

	$data_json_fiat = wp_remote_get(esc_url('https://hamyarandroid.com/api?t=currency'));
	$data_json_gold = wp_remote_get(esc_url('https://hamyarandroid.com/api?t=gold'));

	if(is_wp_error($data_json_fiat) or is_wp_error($data_json_gold)) {
		mnswmc_add_webservice_errors($webservice_id, (is_wp_error($data_json_fiat) ? $data_json_fiat->get_error_message() : $data_json_gold->get_error_message()));
		return false;
	}

	$data_array_fiat = json_decode($data_json_fiat['body'], true);
	$data_array_gold = json_decode($data_json_gold['body'], true);

	if(!isset($data_array_fiat['ok']) or !isset($data_array_gold['ok'])) {
		mnswmc_add_webservice_errors($webservice_id);
		return false;
	}

	if($data_array_fiat['ok'] == false or $data_array_gold['ok'] == false) {
		mnswmc_add_webservice_errors($webservice_id);
		return false;
	}

	// Set unique index and reorder data
	$data = array();

	foreach($data_array_fiat['list'] as $key => $value) {

		if(isset($value['id']) and isset($value['nameFa']) and isset($value['price'])) {
			$data[sanitize_title('currency' . $value['id'])] = array('name' => sanitize_text_field($value['nameFa']), 'price' => mnswmc_sanitize_number($value['price']));
		} else {
			mnswmc_add_webservice_errors($webservice_id);
			return false;
			break;
		}
	}
	foreach($data_array_gold['list'] as $key => $value) {

		if(isset($value['id']) and isset($value['name']) and isset($value['price'])) {
			$data[sanitize_title('gold' . $value['id'])] = array('name' => sanitize_text_field($value['name']), 'price' => mnswmc_sanitize_number($value['price']));
		} else {
			mnswmc_add_webservice_errors($webservice_id);
			return false;
			break;
		}
	}

	mnswmc_delete_webservice_errors($webservice_id);
	return $data;
}