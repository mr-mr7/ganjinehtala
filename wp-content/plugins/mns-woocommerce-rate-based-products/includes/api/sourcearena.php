<?php

function mnswmc_api_get_data_sourcearena($webservice_id, $key) {

	$data_json = wp_remote_get(('https://sourcearena.ir/api/?token=' . $key . '&currency'));

	if(is_wp_error($data_json)) {
		mnswmc_add_webservice_errors($webservice_id, $data_json->get_error_message());
		return false;
	}

	$data_array = json_decode($data_json['body'], true);

	if(empty($data_array)) {

		if(isset($data_json['response']['code']) and isset($data_json['response']['message'])) {
			mnswmc_add_webservice_errors($webservice_id, sanitize_text_field($data_json['response']['code'] . ' ' . $data_json['response']['message'] . ' - ' . $data_json['body']));
			return false;
		} else {
			mnswmc_add_webservice_errors($webservice_id);
			return false;
		}
	}

	if(!isset($data_array['data'])) {
		if(isset($data_array['Error'])) {
			mnswmc_add_webservice_errors($webservice_id, sanitize_text_field($data_array['Error']));
			return false;
		} else {
			mnswmc_add_webservice_errors($webservice_id, 'Undefined currencies');
			return false;
		}
	}
	
	// Reform data
	$data = array();
	foreach($data_array['data'] as $key => $value) {
		if(isset($value['slug']) and isset($value['name']) and isset($value['price'])) {
			$data[sanitize_text_field($value['slug'])] = array('name' => sanitize_text_field($value['name']), 'price' => mnswmc_sanitize_number($value['price']));
		} else {
			mnswmc_add_webservice_errors($webservice_id, 'Undefined price value form webservice');
			return false;
			break;
		}
	}

	mnswmc_delete_webservice_errors($webservice_id);
	return $data;
}