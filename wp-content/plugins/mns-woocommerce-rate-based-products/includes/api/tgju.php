<?php

function mnswmc_api_get_data_tgju($webservice_id, $key = false) {

	$data_json = wp_remote_get(esc_url('https://api.tgju.online/v1/data/sana/json'));

	if(is_wp_error($data_json)) {
		mnswmc_add_webservice_errors($webservice_id, $data_json->get_error_message());
		return false;
	}

	$data_array = json_decode($data_json['body'], true);

	if(!isset($data_array['sana']['data'])) {
		mnswmc_add_webservice_errors($webservice_id, 'API data is null!');
		return false;
	}
	
	if(!is_array($data_array['sana']['data']) or empty($data_array['sana']['data'])) {
		mnswmc_add_webservice_errors($webservice_id, 'API data is empty!');
		return false;
	}
	
	// Reform data
	$data = array();
	foreach($data_array['sana']['data'] as $key => $value) {
		if(isset($value['p']) and isset($value['title']) and isset($value['slug'])) {
			$data[sanitize_text_field($value['slug'])] = array('name' => sanitize_text_field($value['title']), 'price' => mnswmc_sanitize_number($value['p']));
		} else {
			mnswmc_add_webservice_errors($webservice_id, 'API data is corrupted!');
			return false;
			break;
		}
	}

	mnswmc_delete_webservice_errors($webservice_id);
	return $data;
}