<?php

function mnswmc_api_get_data_nerkhapi($webservice_id, $key) {

	$data_json_fiat = wp_remote_get(esc_url('http://nerkh-api.ir/api/' . $key . '/currency/'));
	$data_json_gold = wp_remote_get(esc_url('http://nerkh-api.ir/api/' . $key . '/gold/'));

	if(is_wp_error($data_json_fiat) or is_wp_error($data_json_gold)) {
		mnswmc_add_webservice_errors($webservice_id, (is_wp_error($data_json_fiat) ? $data_json_fiat->get_error_message() : $data_json_gold->get_error_message()));
		return false;
	}

	$data_array_fiat = json_decode($data_json_fiat['body'], true);
	$data_array_gold = json_decode($data_json_gold['body'], true);

	if(!isset($data_array_fiat['data']['status']) or !isset($data_array_gold['data']['status'])) {
		mnswmc_add_webservice_errors($webservice_id);
		return false;
	}

	if($data_array_fiat['data']['status'] != '200' or $data_array_gold['data']['status'] != '200') {

		if(isset($data_array_fiat['data']['message']) or isset($data_array_gold['data']['message'])) {
			mnswmc_add_webservice_errors($webservice_id, ($data_array_fiat['data']['status'] != '200' ? $data_array_fiat['data']['message'] : $data_array_gold['data']['message']) );
		} else {
			mnswmc_add_webservice_errors($webservice_id);
		}
		return false;
	}

	$data = array_merge($data_array_fiat['data']['prices'], $data_array_gold['data']['prices']);
	
	foreach($data as $key => $value) {
		
		if(isset($value['current'])) {
			$data[$key] = array('name' => sanitize_text_field($key), 'price' => mnswmc_sanitize_number($value['current']));
		} else {
			mnswmc_add_webservice_errors($webservice_id);
			return false;
			break;
		}
	}

	mnswmc_delete_webservice_errors($webservice_id);
	return $data;
}