<?php

function mnswmc_api_get_data_ratesbox($webservice_id, $key = false) {

	$data_json = wp_remote_get(esc_url('http://api.ratebox.ir/apijson.php?token=' . $key));

	if(is_wp_error($data_json)) {
		mnswmc_add_webservice_errors($webservice_id, $data_json->get_error_message());
		return false;
	}

	$data_array = json_decode($data_json['body'], true);

	if(!isset($data_array['sana_buy_usd'])) {
		mnswmc_add_webservice_errors($webservice_id, 'Unauthorized or Too Many Requests');
		return false;
	}
	
	// Reform data
	$data = array();
	foreach($data_array as $key => $value) {
		if(isset($value['p'])) {
			$data[sanitize_text_field($key)] = array('name' => sanitize_text_field(str_replace('_', ' ', strtoupper($key))), 'price' => mnswmc_sanitize_number(str_replace(',', '', $value['p'])));
		} else {
			mnswmc_add_webservice_errors($webservice_id, 'Corrupted data');
			return false;
			break;
		}
	}

	mnswmc_delete_webservice_errors($webservice_id);
	return $data;
}