<?php

function mnswmc_api_get_data_coinmarketcap($webservice_id, $key) {

	$coins = array('BTC' => 'BTC', 'ETH' => 'ETH', 'XRP' => 'XRP', 'BCH' => 'BCH', 'BSV' => 'BSV', 'LTC' => 'LTC', 'EOS' => 'EOS', 'BNB' => 'BNB', 'ADA' => 'ADA', 'XLM' => 'XLM', 'XMR' => 'XLM', 'TRX' => 'TRX', 'ETC' => 'ETC', 'DASH' => 'DASH', 'NEO' => 'NEO', 'MIOTA' => 'MIOTA', 'ZEC' => 'ZEC', 'XEM' => 'XEM', 'VET' => 'VET', 'BTG' => 'BTG');
	$coins = apply_filters('mnswmc_webservice_coinmarketcap_coins', $coins);
	$coins_str = implode(',', $coins);
	$coins_data = array();

	$coins_data_json = wp_remote_get('https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?&aux=date_added&symbol=' . $coins_str, array(
		'headers' => array(
			'X-CMC_PRO_API_KEY' => $key
		)
	));

	if(is_wp_error($coins_data_json)) {
		mnswmc_add_webservice_errors($webservice_id, $coins_data_json->get_error_message());
		return false;
	}

	$coins_data_array = json_decode($coins_data_json['body'], true);

	if(!isset($coins_data_array['status'])) {
		mnswmc_add_webservice_errors($webservice_id);
		return false;
	}

	if(!isset($coins_data_array['data']) or !is_array($coins_data_array['data'])) {
		mnswmc_add_webservice_errors($webservice_id, $coins_data_array['status']['error_message']);
		return false;
	}

	foreach ($coins_data_array['data'] as $key => $value) {
		
		$coins_data[$value['id']] = array('name' => sanitize_text_field($value['name']), 'price' => mnswmc_sanitize_number($value['quote']['USD']['price']));
	}

	mnswmc_delete_webservice_errors($webservice_id);
	return $coins_data;
}