<?php

function mnswmc_api_get_data_sunnyweb($webservice_id, $key) {

	$data_json = wp_remote_get(esc_url('https://api.sunnyweb.ir/api/' . $key));

	if(is_wp_error($data_json)) {
		mnswmc_add_webservice_errors($webservice_id, $data_json->get_error_message());
		return false;
	}

	$data_array = json_decode($data_json['body'], true);

	if(empty($data_array)) {

		if(isset($data_json['response']['code']) and isset($data_json['response']['message'])) {
			mnswmc_add_webservice_errors($webservice_id, sanitize_text_field($data_json['response']['code'] . ' ' . $data_json['response']['message'] . ' - ' . $data_json['body']));
			return false;
		} else {
			mnswmc_add_webservice_errors($webservice_id);
			return false;
		}
	}

	if(!isset($data_array['0'])) {

		if(isset($data_json['body']) and $data_json['body'] != '') {
			mnswmc_add_webservice_errors($webservice_id, sanitize_text_field(strval($data_json['body'])));
			return false;
		} else {
			mnswmc_add_webservice_errors($webservice_id);
			return false;
		}
	}
	
	// Reform data
	$data = array();
	unset($data_array['0']['id'], $data_array['0']['timee']);

	foreach($data_array['0'] as $key => $value) {
		$data[$key] = array('name' => sanitize_text_field(strtoupper($key)), 'price' => mnswmc_sanitize_number($value));
	}

	mnswmc_delete_webservice_errors($webservice_id);
	return $data;
}