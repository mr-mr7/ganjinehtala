<?php

function mnswmc_api_get_data_openexchangerates($webservice_id, $key) {

	$data_json_names = wp_remote_get(esc_url('https://openexchangerates.org/api/currencies.json?prettyprint=false'));
	$data_json = wp_remote_get(esc_url('https://openexchangerates.org/api/latest.json?app_id=' . $key . '&base=USD&prettyprint=false'));

	if(is_wp_error($data_json_names)) {
		mnswmc_add_webservice_errors($webservice_id, $data_json_names->get_error_message());
		return false;
	}
	if(is_wp_error($data_json)) {
		mnswmc_add_webservice_errors($webservice_id, $data_json->get_error_message());
		return false;
	}

	$data_array_names = json_decode($data_json_names['body'], true);
	$data_array = json_decode($data_json['body'], true);

	if(empty($data_array_names) or empty($data_array)) {

		if(isset($data_json_names['response']['code']) and isset($data_json_names['response']['message'])) {
			mnswmc_add_webservice_errors($webservice_id, sanitize_text_field($data_json_names['response']['code'] . ' ' . $data_json_names['response']['message'] . ' - ' . $data_json_names['body']));
			return false;
		} else {
			mnswmc_add_webservice_errors($webservice_id);
			return false;
		}
		if(isset($data_json['response']['code']) and isset($data_json['response']['message'])) {
			mnswmc_add_webservice_errors($webservice_id, sanitize_text_field($data_json['response']['code'] . ' ' . $data_json['response']['message'] . ' - ' . $data_json['body']));
			return false;
		} else {
			mnswmc_add_webservice_errors($webservice_id);
			return false;
		}
	}

	if(isset($data_array['error']) and $data_array['error'] == true) {

		if(isset($data_array['description'])) {
			mnswmc_add_webservice_errors($webservice_id, sanitize_text_field($data_array['description']));
		} else {
			mnswmc_add_webservice_errors($webservice_id);
		}
		return false;
	}

	if(!isset($data_array['rates'])) {
		mnswmc_add_webservice_errors($webservice_id, 'No data returned from webservice');
		return false;
	}
	
	// Reform data
	$data = array();
	foreach($data_array['rates'] as $key => $value) {

		$data[sanitize_title($key)] = array('name' => (isset($data_array_names[$key]) ? sanitize_text_field($data_array_names[$key]) : sanitize_text_field($key)), 'price' => mnswmc_sanitize_number(1 / $value));
	}

	mnswmc_delete_webservice_errors($webservice_id);
	return $data;
}