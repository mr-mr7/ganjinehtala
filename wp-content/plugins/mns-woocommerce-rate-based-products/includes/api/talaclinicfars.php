<?php

function mnswmc_api_get_data_talaclinicfars($webservice_id, $key) {

	$data_json = wp_remote_get(('http://panel.talaclinicfars.com/api/prices.php?token=' . $key));

	if(is_wp_error($data_json)) {
		mnswmc_add_webservice_errors($webservice_id, $data_json->get_error_message());
		return false;
	}

	$data_array = json_decode($data_json['body'], true);

	if(!isset($data_array['prices']) or !is_array($data_array['prices']) or empty($data_array['prices'])) {
		mnswmc_add_webservice_errors($webservice_id, 'API data is empty!');
		return false;
	}
	
	// Reform data
	$data = array();
	foreach($data_array['prices'] as $key => $value) {
		if(isset($value['name']) and isset($value['price']) and isset($value['title'])) {
			$data[sanitize_text_field($value['name'])] = array('name' => sanitize_text_field($value['title']), 'price' => (mnswmc_sanitize_number(str_replace(',', '', mnswmc_fa_num_to_en($value['price']))) * 10));
		} else {
			mnswmc_add_webservice_errors($webservice_id, 'API data is corrupted!');
			return false;
			break;
		}
	}

	mnswmc_delete_webservice_errors($webservice_id);
	return $data;
}