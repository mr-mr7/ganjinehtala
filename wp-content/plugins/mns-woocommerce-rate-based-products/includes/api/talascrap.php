<?php

function mnswmc_api_get_data_talascrap($webservice_id, $key = false) {

	// example of how to use basic selector to retrieve HTML contents
	require_once (MNSWMC_DIR . '/includes/simple-html-dom.php');

	// get DOM from URL or file
	$html = wp_remote_get(esc_url('https://www.tala.ir/webservice/price_live.php?new=1&demo=1'));

	if(is_wp_error($html)) {
		mnswmc_add_webservice_errors($webservice_id, $html->get_error_message());
		return false;
	}

	$html = str_get_html($html['body']);

	$items = array(
		'gold_18k' => 'طلاي 18 عيار',
		'gold_24k' => 'طلاي 24 عيار',
		'gold_ounce' => 'اونس طلا',
		'sekke-gad' => 'سکه قدیم',
		'sekke-jad' => 'سکه جدید',
		'sekke-nim' => 'سکه نيم',
		'sekke-rob' => 'ربع سکه',
		'sekke-grm' => 'سکه گرمي',
	);
	$data = array();

	try {

		if(!is_object($html))
			return false;

		if(!method_exists($html, 'find'))
			return false;
		
		foreach ($items as $key => $name) {
			
			if(is_null($html->find('a#' . $key, 0))) {

				mnswmc_add_webservice_errors($webservice_id, 'مقادیر از وب‌سرویس دریافت نشد!');
				return false;

			} else {

				$data[$key] = array(
					'name' => $name,
					'price' => (mnswmc_sanitize_number(str_replace(',', '', mnswmc_fa_num_to_en($html->find('a#' . $key, 0)->plaintext))) * 10),
				);
			}
		}

		mnswmc_delete_webservice_errors($webservice_id);
		return $data;

	} catch (Exception $e) {

		mnswmc_add_webservice_errors($webservice_id, 'وب‌سرویس در دسترس نیست!');
		return false;
	}
}