<?php

function mnswmc_api_get_data_exchangerateapi($webservice_id, $key) {

	$data_json = wp_remote_get(esc_url('https://prime.exchangerate-api.com/v5/' . $key . '/latest/USD'));

	if(is_wp_error($data_json)) {
		mnswmc_add_webservice_errors($webservice_id, $data_json->get_error_message());
		return false;
	}

	$data_array = json_decode($data_json['body'], true);

	if(empty($data_array)) {

		if(isset($data_json['response']['code']) and isset($data_json['response']['message'])) {
			mnswmc_add_webservice_errors($webservice_id, sanitize_text_field($data_json['response']['code'] . ' ' . $data_json['response']['message'] . ' - ' . $data_json['body']));
			return false;
		} else {
			mnswmc_add_webservice_errors($webservice_id);
			return false;
		}
	}

	if(isset($data_array['result']) and $data_array['result'] == 'error') {

		if(isset($data_array['error'])) {
			mnswmc_add_webservice_errors($webservice_id, sanitize_text_field($data_array['error']));
		} else {
			mnswmc_add_webservice_errors($webservice_id);
		}
		return false;
	}

	if(!isset($data_array['conversion_rates'])) {
		mnswmc_add_webservice_errors($webservice_id, 'No data returned from webservice');
		return false;
	}
	
	// Reform data
	$data = array();
	foreach($data_array['conversion_rates'] as $key => $value) {

		$data[sanitize_title($key)] = array('name' => sanitize_text_field($key), 'price' => mnswmc_sanitize_number(1 / $value));
	}

	mnswmc_delete_webservice_errors($webservice_id);
	return $data;
}