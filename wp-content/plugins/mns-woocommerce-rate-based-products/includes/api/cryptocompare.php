<?php

function mnswmc_api_get_data_cryptocompare($webservice_id, $key) {

	$coins = array('BTC' => 'BTC', 'ETH' => 'ETH', 'XRP' => 'XRP', 'BCH' => 'BCH', 'BSV' => 'BSV', 'LTC' => 'LTC', 'EOS' => 'EOS', 'BNB' => 'BNB', 'ADA' => 'ADA', 'XLM' => 'XLM', 'XMR' => 'XLM', 'TRX' => 'TRX', 'ETC' => 'ETC', 'DASH' => 'DASH', 'NEO' => 'NEO', 'MIOTA' => 'MIOTA', 'ZEC' => 'ZEC', 'XEM' => 'XEM', 'VET' => 'VET', 'BTG' => 'BTG');
	$coins = apply_filters('mnswmc_webservice_cryptocompare_coins', $coins);
	$coins_splitted = array_chunk($coins, 50);
	$coins_data = array();

	foreach($coins_splitted as $coins_cluster) {

		$coins_cluster_str = implode(',', $coins_cluster);
		$coins_data_json = wp_remote_get('https://min-api.cryptocompare.com/data/pricemulti?fsyms=' . $coins_cluster_str . '&tsyms=USD&extraParams=MNSWMC', array(
			'headers' => array(
				'authorization' => 'Apikey ' . $key
			)
		));

		if(is_wp_error($coins_data_json)) {
			mnswmc_add_webservice_errors($webservice_id, $coins_data_json->get_error_message());
			return false;
			break;
		}

		$coins_data_array = json_decode($coins_data_json['body'], true);

		if(isset($coins_data_array['Response']) and $coins_data_array['Response'] == 'Error' and isset($coins_data_array['Message'])) {
			mnswmc_add_webservice_errors($webservice_id, $coins_data_array['Message']);
			return false;
			break;
		}

		if(!isset($coins_data_array[$coins_cluster[0]])) {
			mnswmc_add_webservice_errors($webservice_id);
			return false;
			break;
		}
				
		$coins_data = array_merge($coins_data, $coins_data_array);
	}

	foreach($coins_data as $key => $value) {

		$coins_data[$key] = array('name' => sanitize_text_field($key), 'price' => mnswmc_sanitize_number($value['USD']));
	}

	mnswmc_delete_webservice_errors($webservice_id);
	return $coins_data;
}