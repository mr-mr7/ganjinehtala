<?php

function mnswmc_api_get_data_nobitex($webservice_id, $key = false) {

	$data_json = wp_remote_get('https://api.nobitex.ir/market/stats?srcCurrency=usdt&dstCurrency=rls');

	if(is_wp_error($data_json)) {
		mnswmc_add_webservice_errors($webservice_id, $data_json->get_error_message());
		return false;
	}

	$data_array = json_decode($data_json['body'], true);

	if(!isset($data_array['status'])) {
		mnswmc_add_webservice_errors($webservice_id, 'Webservice does not return any data');
		return false;
	}

	if($data_array['status'] != 'ok') {
		mnswmc_add_webservice_errors($webservice_id, $data_array['message']);
		return false;
	}

	$usd_rate = $data_array['stats']['usdt-rls']['latest'];
	
	// Reform data
	$data = array();
	foreach($data_array['global']['binance'] as $key => $value) {
		$data[sanitize_text_field($key)] = array('name' => sanitize_text_field(strtoupper($key)), 'price' => mnswmc_sanitize_number($value * $usd_rate));
	}

	mnswmc_delete_webservice_errors($webservice_id);
	return $data;
}