<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function mnswmc_get_webservices() {
	
	$webservices = array();
	$webservices = apply_filters('mnswmc_api_edit_webservices', $webservices);
	return $webservices;
}

// Add or remove webservices
function mnswmc_api_custom_webservices($webservices) {
	
	$webservices['nerkhapi'] = array(
		'name' => 'Nerkh-API',
		'license' => true,
		'free' => false,
		'connection' => false,
		'currency' => false,
		'path' => false,
		'url' => 'https://nerkh-api.ir/',
		'callback' => 'mnswmc_api_get_data_nerkhapi'
	);
	$webservices['navasan'] = array(
		'name' => 'Navasan',
		'license' => true,
		'free' => false,
		'connection' => false,
		'currency' => false,
		'path' => false,
		'url' => 'https://www.navasan.net/api/',
		'callback' => 'mnswmc_api_get_data_navasan'
	);
	$webservices['sunnyweb'] = array(
		'name' => 'SunnyWeb',
		'license' => true,
		'free' => false,
		'connection' => false,
		'currency' => false,
		'path' => false,
		'url' => 'https://api.sunnyweb.ir/',
		'callback' => 'mnswmc_api_get_data_sunnyweb'
	);
	$webservices['ratesbox'] = array(
		'name' => 'RatesBox',
		'license' => true,
		'free' => false,
		'connection' => false,
		'currency' => false,
		'path' => false,
		'url' => 'https://ratesbox.ir/%d8%ae%d8%af%d9%85%d8%a7%d8%aa-%d9%88%d8%a8-%d8%b3%d8%b1%d9%88%db%8c%d8%b3-%d8%b7%d9%84%d8%a7-%d8%b3%da%a9%d9%87-%d9%88-%d8%a7%d8%b1%d8%b2-api/',
		'callback' => 'mnswmc_api_get_data_ratesbox'
	);
	$webservices['sourcearena'] = array(
		'name' => 'Sourcearena',
		'license' => true,
		'free' => false,
		'connection' => false,
		'currency' => false,
		'path' => false,
		'url' => 'https://www.sourcearena.ir/',
		'callback' => 'mnswmc_api_get_data_sourcearena'
	);
	$webservices['apimaster'] = array(
		'name' => 'API Master',
		'license' => true,
		'free' => false,
		'connection' => false,
		'currency' => false,
		'path' => false,
		'url' => 'https://www.apimaster.ir/',
		'callback' => 'mnswmc_api_get_data_apimaster'
	);
	$webservices['talaclinicfars'] = array(
		'name' => 'Tala Clinic Fars',
		'license' => true,
		'free' => false,
		'connection' => false,
		'currency' => false,
		'path' => false,
		'url' => 'http://talaclinicfars.ir/',
		'callback' => 'mnswmc_api_get_data_talaclinicfars'
	);
	$webservices['tgjupaid'] = array(
		'name' => 'TGJU Paid',
		'license' => true,
		'free' => false,
		'connection' => false,
		'currency' => false,
		'path' => false,
		'url' => 'https://www.tgju.org/api',
		'callback' => 'mnswmc_api_get_data_tgjupaid'
	);
	$webservices['tgju'] = array(
		'name' => 'TGJU Free',
		'license' => false,
		'free' => true,
		'connection' => false,
		'currency' => false,
		'path' => false,
		'url' => 'https://www.tgju.org/sanarate-service',
		'callback' => 'mnswmc_api_get_data_tgju'
	);
	$webservices['parsijoo'] = array(
		'name' => 'Parsijoo',
		'license' => false,
		'free' => true,
		'connection' => false,
		'currency' => false,
		'path' => false,
		'url' => 'http://csi.parsijoo.ir/',
		'callback' => 'mnswmc_api_get_data_parsijoo'
	);
	$webservices['talascrap'] = array(
		'name' => 'Tala.ir Scrap',
		'license' => false,
		'free' => true,
		'connection' => false,
		'currency' => false,
		'path' => false,
		'url' => 'https://www.tala.ir/',
		'callback' => 'mnswmc_api_get_data_talascrap'
	);
	$webservices['hamyarandroid'] = array(
		'name' => 'Hamyar Android',
		'license' => false,
		'free' => true,
		'connection' => false,
		'currency' => false,
		'path' => false,
		'url' => 'https://hamyarandroid.com/api/currency',
		'callback' => 'mnswmc_api_get_data_hamyarandroid'
	);
	$webservices['nobitex'] = array(
		'name' => 'Nobitex',
		'license' => false,
		'free' => true,
		'connection' => false,
		'currency' => false,
		'path' => false,
		'url' => 'https://nobitex.ir/',
		'callback' => 'mnswmc_api_get_data_nobitex'
	);
	$webservices['openexchangerates'] = array(
		'name' => 'Open Exchange Rates',
		'license' => true,
		'free' => true,
		'connection' => true,
		'currency' => 'USD',
		'path' => false,
		'url' => 'https://openexchangerates.org/',
		'callback' => 'mnswmc_api_get_data_openexchangerates'
	);
	$webservices['exchangerateapi'] = array(
		'name' => 'ExchangeRate-API',
		'license' => true,
		'free' => true,
		'connection' => true,
		'currency' => 'USD',
		'path' => false,
		'url' => 'https://www.exchangerate-api.com/',
		'callback' => 'mnswmc_api_get_data_exchangerateapi'
	);
	$webservices['cryptocompare'] = array(
		'name' => 'CryptoCompare',
		'license' => true,
		'free' => true,
		'connection' => true,
		'currency' => 'USD',
		'path' => false,
		'url' => 'https://min-api.cryptocompare.com/',
		'callback' => 'mnswmc_api_get_data_cryptocompare'
	);
	$webservices['coinmarketcap'] = array(
		'name' => 'CoinMarketCap',
		'license' => true,
		'free' => true,
		'connection' => true,
		'currency' => 'USD',
		'path' => false,
		'url' => 'https://coinmarketcap.com/api/',
		'callback' => 'mnswmc_api_get_data_coinmarketcap'
	);
	return $webservices;
}
add_filter('mnswmc_api_edit_webservices', 'mnswmc_api_custom_webservices');

function mnswmc_get_webservice_errors($webservice = false) {

	$errors = get_option('mnswmc_webservice_errors');
	$errors = is_array($errors) ? $errors : array();

	if($webservice) {
		if(isset($errors[$webservice]))
			return $errors[$webservice];

		return false;
	}

	return $errors;
}

function mnswmc_add_webservice_errors($webservice, $error_message = '') {
	
	$errors = mnswmc_get_webservice_errors();
	$errors[$webservice] = ($error_message != '') ? sanitize_text_field($error_message) : __('Unknown error', 'mnswmc');

	return update_option('mnswmc_webservice_errors', $errors);
}

function mnswmc_delete_webservice_errors($webservice = false) {
	
	if(!$webservice) {
		delete_option('mnswmc_webservice_errors');
		return true;
	}

	$errors = mnswmc_get_webservice_errors();

	if(isset($errors[$webservice]))
		unset($errors[$webservice]);

	return update_option('mnswmc_webservice_errors', $errors);
}

function mnswmc_load_license() {

	require_once (MNSWMC_DIR . '/includes/license.php');

	if(!mnswmc_check_activation())
		add_action( 'admin_notices', 'mnswmc_admin_activation_notice__error' );
}
add_action( 'init', 'mnswmc_load_license' );

function mnswmc_is_webservice_interval_passed($webservice_id) {

	$interval = mnswmc_get_webservice_option($webservice_id, 'interval') * 60;
	$passed = time() - mnswmc_get_webservice_time($webservice_id);

	if($passed >= $interval)
		return true;

	return false;
}

function mnswmc_get_webservice_time($webservice_id) {

	$times = mnswmc_get_option_webservices_times();
	$time = isset($times[$webservice_id]) ? $times[$webservice_id] : 0;

	return $time;
}

function mnswmc_get_webservice_option($webservice_id, $option) {

	$webservices = mnswmc_get_webservices();

	if(!isset($webservices[$webservice_id]))
		return false;

	$webservices_options = mnswmc_get_option_webservices_options();

	if(!isset($webservices_options[$webservice_id]))
		return false;

	$webservice = $webservices_options[$webservice_id];

	switch ($option) {
		
		case 'active':
			$data = (isset($webservice['active']) and ($webservice['active'] == 'yes' or $webservice['active'] == '1')) ? 'yes' : false;
			break;

		case 'apikey':
			$data = isset($webservice['apikey']) ? $webservice['apikey'] : '';
			break;
			
		case 'interval':
			$data = (isset($webservice['interval']) and is_numeric($webservice['interval'])) ? $webservice['interval'] : '60';
			break;
					
		default:
			$data = false;
			break;
	}

	return $data;
}

function mnswmc_api_check_interval() {

	foreach(mnswmc_get_webservices() as $webservice_id => $webservice) {

		if(!mnswmc_get_webservice_option($webservice_id, 'active'))
			continue;

		if(!mnswmc_is_webservice_interval_passed($webservice_id))
			continue;

		if($webservice['path'])
			require_once ($webservice['path']);
		else
			require_once (MNSWMC_DIR . '/includes/api/' . $webservice_id . '.php');

		$callback = $webservice['callback'];

		if(!function_exists($callback)) {
			mnswmc_add_webservice_errors($webservice_id, 'The function "' . $callback . '" does not exists.');
			continue;
		}

		$api_key = mnswmc_get_webservice_option($webservice_id, 'apikey');
		$data = $callback($webservice_id, $api_key);
		mnswmc_api_update_related_currencies($data, $webservice_id);
	}
}
add_action( 'wp_loaded', 'mnswmc_api_check_interval' );

function mnswmc_api_update_related_currencies($data, $webservice) {

	mnswmc_update_webservice_time($webservice, time());

	if(!mnswmc_check_activation())
		return false;

	if(!$data)
		return;

	mnswmc_update_webservice_data($webservice, $data);

	// query for currencies that use web services
	$currencies_args = array(
		'post_type' => 'mnswmc',
		'post_status' => 'publish',
		'nopaging' => true,
		'meta_key' => '_mnswmc_currency_update_type',
		'meta_value' => $webservice
	);
	$currencies = get_posts( $currencies_args );

	foreach ($currencies as $currency) {
		$relation = mnswmc_get_currency_relation($currency->ID);

		if(isset($data[$relation]))
			mnswmc_do_update_currency($currency->ID);
	}
}

function mnswmc_webservice_notice_error() {

	$errors = mnswmc_get_webservice_errors();

	if(empty($errors) or !is_array($errors))
		return;

	$webservices = mnswmc_get_webservices();

	foreach ($errors as $webservice => $message) {

		if(!isset($webservices[$webservice])) {
			mnswmc_delete_webservice_errors($webservice);
			continue;
		}
		
		if(!mnswmc_get_webservice_option($webservice, 'active')) {
			mnswmc_delete_webservice_errors($webservice);
			continue;
		}

		$message = sprintf( __( 'Webservice Error (%s): %s <a href="%s">Check now!</a>', 'mnswmc' ), $webservices[$webservice]['name'], esc_attr($message), mnswmc_settings_url() );
		printf( '<div class="notice notice-error"><p>%1$s</p></div>', $message );
	}
}
add_action( 'admin_notices', 'mnswmc_webservice_notice_error' );

// rest api

function mnswmc_rest_api() {

	register_rest_route('mnswmc/v1', '/currency/(?P<token>[a-zA-Z0-9-]+)', array(
		'methods' => WP_REST_Server::READABLE,
		'callback' => 'mnswmc_rest_api_currency',
		'args' => array(
			'token' => array(
				'validate_callback' => function($param, $request, $key) {
					return true;
				}
			),
		),
		//'permission_callback' => '__return_true',
		'permission_callback' => function () {
			return true;
		}
	));
}
add_action( 'rest_api_init', 'mnswmc_rest_api');

function mnswmc_rest_api_request_url($method, $token, $args = array()) {

	return rest_url('mnswmc/v1/' . $method . '/' . $token);
}

function mnswmc_rest_api_main_token() {

	$token = get_option('mnswmc_rest_api_main_token', '9f8e7adfcdb7c395d33d08fcd968ade8');
	return $token;
}

function mnswmc_rest_api_currency($request) {

	if(get_option('mnswmc_rest_api_active') != 'yes') {
		return new WP_Error( 'not-found', 'Endpoint Not Found.', array( 'status' => 404 ) );
	}

	$main_token = mnswmc_rest_api_main_token();
	$token = sanitize_text_field($request['token']);

	if($main_token != $token) {
		return new WP_Error( 'invalid-token', 'Entered Token is not valid.', array( 'status' => 301 ) );
	}

	$currencies = mnswmc_get_currencies();
	$return = array();

	foreach ($currencies as $currency) {
		$return[$currency->ID] = array(
			'name' => get_the_title($currency->ID),
			'rate' => mnswmc_get_currency_rate($currency->ID)
		);
	}

	return $return;
}