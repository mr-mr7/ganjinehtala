<?php

function mnswmc_api_get_data_tgjupaid($webservice_id, $key) {

	$data_json = wp_remote_get('https://gateway.accessban.com/public/web-service/list/common?limit=600&format=json', array(
		'headers' => array(
			'Authorization' => 'Bearer ' . $key
		)
	));

	if(is_wp_error($data_json)) {
		mnswmc_add_webservice_errors($webservice_id, $data_json->get_error_message());
		return false;
	}

	$data_array = json_decode($data_json['body'], true);

	if(!isset($data_array['data'])) {
		mnswmc_add_webservice_errors($webservice_id, 'API data is null!');
		return false;
	}
	
	if(!is_array($data_array['data']) or empty($data_array['data'])) {
		mnswmc_add_webservice_errors($webservice_id, 'API data is empty!');
		return false;
	}
		
	// Reform data
	$data = array();
	foreach($data_array['data'] as $key => $value) {
		if(isset($value['p']) and isset($value['title']) and isset($value['slug'])) {
			$data[sanitize_text_field($value['slug'])] = array('name' => sanitize_text_field($value['title']), 'price' => mnswmc_sanitize_number(str_replace(',', '', mnswmc_fa_num_to_en($value['p']))));
		} else {
			continue;
		}
	}

	mnswmc_delete_webservice_errors($webservice_id);
	return $data;
}