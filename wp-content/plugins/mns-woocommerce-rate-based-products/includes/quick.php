<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


function mnswmc_add_inline_data_product($post, $post_type_object) {

	if($post->post_type != 'product')
		return;

	$product = wc_get_product($post);

	if(!$product->is_type('simple') and !$product->is_type('external'))
		return;
	
	echo '<div class="mnswmc_active">' . mnswmc_get_product_active($post->ID) . '</div>';
	echo '<div class="mnswmc_dependence_type">' . mnswmc_get_product_dependence_type($post->ID) . '</div>';
	echo '<div class="mnswmc_currency_id">' . mnswmc_get_product_currency_id($post->ID) . '</div>';
	echo '<div class="mnswmc_regular_price">' . mnswmc_get_product_regular_price($post->ID) . '</div>';
	echo '<div class="mnswmc_sale_price">' . mnswmc_get_product_sale_price($post->ID) . '</div>';
	echo '<div class="mnswmc_rounding_type">' . mnswmc_get_product_rounding_type($post->ID) . '</div>';
	echo '<div class="mnswmc_rounding_side">' . mnswmc_get_product_rounding_side($post->ID) . '</div>';
	echo '<div class="mnswmc_price_rounding">' . mnswmc_get_product_rounding_value($post->ID) . '</div>';
	echo '<div class="mnswmc_profit_margin">' . mnswmc_get_product_profit_margin($post->ID) . '</div>';
	echo '<div class="mnswmc_profit_type">' . mnswmc_get_product_profit_type($post->ID) . '</div>';
}

// Backward compatibility
if(version_compare(get_bloginfo('version'), '4.9.8', '>=')) {
	add_action( 'add_inline_data', 'mnswmc_add_inline_data_product', 10, 2 );
} else {
	add_action( 'manage_product_posts_custom_column', function($column, $post_id) {

		switch($column) {
			case 'name' : 
				echo '<div class="hidden" id="mnswmc_inline_' . $post_id . '">';
				mnswmc_add_inline_data_product(get_post($post_id), null);
				echo '</div>';
				break;
			default :
				break;
		}	
	}, 99, 2);
}

function mnswmc_quick_edit_box_product($column_name, $post_type) {

	if($column_name != 'name' or $post_type != 'product')
		return;

	// wp_nonce_field('mnswmc_quick_edit_product_nonce', '_mnswmc_quick_edit_product_nonce'); // not used while on wc hook!
	$currencies = mnswmc_get_currencies();

	?>

<div class="clear"></div>
<fieldset class="inline-edit-col-left mnswmc-inline-edit mnswmc-inline-quick-edit">
	<div class="inline-edit-col">
		<h4><?php _e('Rate Based Info', 'mnswmc'); ?></h4>
		<div class="inline-edit-group wp-clearfix">
			<label>
				<span class="title"><?php _e('Rate Based', 'mnswmc'); ?></span>
				<span class="input-text-wrap">
					<input type="checkbox" class="mnswmc_active mnswmc_simple_inline_field" name="_mnswmc_active" value="yes">
					<span class="checkbox-title"><?php _e( 'Check this if current product price is rate based', 'mnswmc' ); ?></span>
				</span>
			</label>
		</div>
		<div class="inline-edit-group wp-clearfix">
			<label>
				<span class="title"><?php _e('Dependency Type', 'mnswmc'); ?></span>
				<span class="input-text-wrap">
					<select class="mnswmc_dependence_type mnswmc_simple_inline_field" name="_mnswmc_dependence_type" disabled>
						<option value="simple"><?php _e('Simple', 'mnswmc'); ?></option>
						<option value="advanced"><?php _e('Advanced (Formulation)', 'mnswmc'); ?></option>
					</select>
				</span>
			</label>
		</div>
		<div class="inline-edit-group wp-clearfix show-if-advanced-dependence">
			<label>
				<span class="input-text-wrap">
					<?php _e('Dependence type of this product is advanced (Formulation) and cannot be edited through quick edit.', 'mnswmc'); ?>
				</span>
			</label>
		</div>
		<div class="inline-edit-group wp-clearfix show-if-simple-dependence">
			<label>
				<span class="title"><?php _e('Choose Currency', 'mnswmc'); ?></span>
				<span class="input-text-wrap">
					<select class="mnswmc_currency_id mnswmc_simple_inline_field" name="_mnswmc_currency_id">
						<?php foreach ($currencies as $currency) {
							echo '<option value="' . $currency->ID . '" data-rate="' . mnswmc_get_currency_rate($currency->ID) . '">' . $currency->post_title . '</option>';
						} ?>
					</select>
				</span>
			</label>
		</div>
		<div class="inline-edit-group wp-clearfix show-if-simple-dependence">
			<label>
				<span class="title"><?php _e('Rate Price', 'mnswmc'); ?></span>
				<span class="input-text-wrap">
					<input type="text" class="mnswmc_regular_price mnswmc_simple_inline_field" name="_mnswmc_regular_price" value="">
				</span>
			</label>
		</div>
		<div class="inline-edit-group wp-clearfix show-if-simple-dependence">
			<label>
				<span class="title" style="width: 5.5em; line-height: 1.25"><?php _e('Sale Rate Price', 'mnswmc'); ?></span>
				<span class="input-text-wrap">
					<input type="text" class="mnswmc_sale_price mnswmc_simple_inline_field" name="_mnswmc_sale_price" value="">
				</span>
			</label>
		</div>
		<div class="inline-edit-group wp-clearfix show-if-simple-dependence">
			<label class="alignleft">
				<span class="title"><?php _e('Profit Margin', 'mnswmc'); ?></span>
				<span class="input-text-wrap">
					<input type="text" class="mnswmc_profit_margin mnswmc_simple_inline_field" name="_mnswmc_profit_margin" value="">
				</span>
			</label>
			<label class="alignleft">
				<select class="mnswmc_profit_type mnswmc_simple_inline_field" name="_mnswmc_profit_type">
					<option value="percent"><?php _e('Percent', 'mnswmc'); ?></option>
					<option value="fixed"><?php echo __('Fixed', 'mnswmc') . ' (' . get_woocommerce_currency_symbol() . ')'; ?></option>
				</select>
			</label>
		</div>
		<div class="inline-edit-group wp-clearfix show-if-simple-dependence">
			<label>
				<span class="title"><?php _e('Rounding', 'mnswmc'); ?></span>
				<span class="input-text-wrap">
					<select class="mnswmc_price_rounding mnswmc_simple_inline_field" name="_mnswmc_price_rounding">
						<?php foreach(mnswmc_price_rounding_items() as $key => $value) {
							echo '<option value="' . $key . '">' . $value . ($key == 1 ? '' : ' ' . get_woocommerce_currency_symbol()) . '</option>';
						} ?>
					</select>
					<select class="mnswmc_rounding_type mnswmc_simple_inline_field" name="_mnswmc_rounding_type">
						<option value="zero"><?php _e('To zero', 'mnswmc'); ?></option>
						<option value="nine"><?php _e('To nine', 'mnswmc'); ?></option>
						<option value="hybrid"><?php _e('Hybrid', 'mnswmc'); ?></option>
					</select>
					<select class="mnswmc_rounding_side mnswmc_simple_inline_field" name="_mnswmc_rounding_side">
						<option value="close"><?php _e('Close', 'mnswmc'); ?></option>
						<option value="up"><?php _e('Up', 'mnswmc'); ?></option>
						<option value="down"><?php _e('Down', 'mnswmc'); ?></option>
					</select>
				</span>
			</label>
		</div>
		<div class="inline-edit-group mnswmc-inline-currency-rate-preview wp-clearfix" style="display: none;">
			<label>
				<span class="title"><?php _e('Currency Rate', 'mnswmc'); ?></span>
				<span class="input-text-wrap">
					<code>0</code> <?php echo get_woocommerce_currency_symbol(); ?>
				</span>
			</label>
		</div>
		<div class="inline-edit-group mnswmc-inline-regular-price-preview wp-clearfix" style="display: none;">
			<label>
				<span class="title"><?php _e('Final Price', 'mnswmc'); ?></span>
				<span class="input-text-wrap">
					<code>0</code> <?php echo get_woocommerce_currency_symbol(); ?>
				</span>
			</label>
		</div>
		<div class="inline-edit-group mnswmc-inline-sale-price-preview wp-clearfix" style="display: none;">
			<label>
				<span class="title" style="width: 5.5em; line-height: 1.25"><?php _e('Final Sale Price', 'mnswmc'); ?></span>
				<span class="input-text-wrap">
					<code>0</code> <?php echo get_woocommerce_currency_symbol(); ?>
				</span>
			</label>
		</div>
		
	</div>
</fieldset>

	<?php
}
add_action( 'quick_edit_custom_box', 'mnswmc_quick_edit_box_product', 10, 2 );


function mnswmc_product_quick_edit_save($product) {

	if(!$product->is_type('simple') and !$product->is_type('external'))
		return;

	$product_id = $product->get_id();

	// check active
	if( !empty( $_POST['_mnswmc_active'] ) ) {
		update_post_meta( $product_id, '_mnswmc_active', 'yes' );
	} else {
		mnswmc_delete_all_product_price_fields($product_id, 'product');
		return;
	}

	// check dependence type
	$dependence_type = mnswmc_get_product_dependence_type($product_id);

	if($dependence_type != 'simple')
		return;
	
	// check price rounding
	if( !empty($_POST['_mnswmc_price_rounding']) and is_numeric($_POST['_mnswmc_price_rounding']) and $_POST['_mnswmc_price_rounding'] >= '0' ) {	
		update_post_meta( $product_id, '_mnswmc_price_rounding', sanitize_text_field( $_POST['_mnswmc_price_rounding'] ) );
	} else {
		update_post_meta( $product_id, '_mnswmc_price_rounding', '0' );
	}
	
	// check rounding type
	if( !empty($_POST['_mnswmc_rounding_type']) and in_array($_POST['_mnswmc_rounding_type'], array('zero', 'nine', 'hybrid')) ) {
		update_post_meta( $product_id, '_mnswmc_rounding_type', sanitize_text_field( $_POST['_mnswmc_rounding_type'] ) );
	} else {
		update_post_meta( $product_id, '_mnswmc_rounding_type', 'zero' );
	}

	// check rounding side
	if( !empty($_POST['_mnswmc_rounding_side']) and in_array($_POST['_mnswmc_rounding_side'], array('close', 'up', 'down')) ) {
		update_post_meta( $product_id, '_mnswmc_rounding_side', sanitize_text_field( $_POST['_mnswmc_rounding_side'] ) );
	} else {
		update_post_meta( $product_id, '_mnswmc_rounding_side', 'close' );
	}
	
	// check currency id
	if( !empty($_POST['_mnswmc_currency_id']) and is_numeric($_POST['_mnswmc_currency_id']) ) {
		update_post_meta( $product_id, '_mnswmc_currency_id', sanitize_text_field( $_POST['_mnswmc_currency_id'] ) );
	} else {
		mnswmc_delete_all_product_price_fields($product_id, 'product');
		return;
	}

	// check regular price
	if( !empty($_POST['_mnswmc_regular_price']) and is_numeric($_POST['_mnswmc_regular_price']) and $_POST['_mnswmc_regular_price'] > 0 ) {
		update_post_meta( $product_id, '_mnswmc_regular_price', sanitize_text_field( $_POST['_mnswmc_regular_price'] ) );
	} else {
		update_post_meta( $product_id, '_mnswmc_regular_price', '0' );
	}

	// check sale price
	if( isset($_POST['_mnswmc_sale_price']) and is_numeric($_POST['_mnswmc_sale_price']) and $_POST['_mnswmc_sale_price'] >= 0 and $_POST['_mnswmc_sale_price'] < get_post_meta($product_id, '_mnswmc_regular_price', true) ) {
		update_post_meta( $product_id, '_mnswmc_sale_price', sanitize_text_field( $_POST['_mnswmc_sale_price'] ) );
	} else {
		delete_post_meta( $product_id, '_mnswmc_sale_price' );
	}

	// check profit type
	if( !empty($_POST['_mnswmc_profit_type']) and in_array($_POST['_mnswmc_profit_type'], array('percent', 'fixed')) ) {
		update_post_meta( $product_id, '_mnswmc_profit_type', sanitize_text_field( $_POST['_mnswmc_profit_type'] ) );
	} else {
		update_post_meta( $product_id, '_mnswmc_profit_type', 'fixed' );
	}

	// check profit margin
	if( !empty($_POST['_mnswmc_profit_margin']) and is_numeric($_POST['_mnswmc_profit_margin']) ) {

		if(get_post_meta($product_id, '_mnswmc_profit_type', true) == 'percent') {
			if($_POST['_mnswmc_profit_margin'] < -100)
				$_POST['_mnswmc_profit_margin'] = -100;
		}
		update_post_meta( $product_id, '_mnswmc_profit_margin', sanitize_text_field( $_POST['_mnswmc_profit_margin'] ) );
		
	} else {
		update_post_meta( $product_id, '_mnswmc_profit_margin', '0' );
	}
	
	mnswmc_update_product_obj_price($product_id);
	return;
}
add_action('woocommerce_product_quick_edit_save', 'mnswmc_product_quick_edit_save', 10, 1);


function mnswmc_bulk_edit_box_product($column_name, $post_type) {

	if($column_name != 'name' or $post_type != 'product')
		return;

	wp_nonce_field('mnswmc_bulk_edit_product_nonce', '_mnswmc_bulk_edit_product_nonce');
	$currencies = mnswmc_get_currencies();

	?>

<div class="clear"></div>
<fieldset class="inline-edit-col-right mnswmc-inline-edit mnswmc-inline-bulk-edit">
	<div class="inline-edit-col">
		<h4><?php _e('Rate Based Info', 'mnswmc'); ?></h4>
		<div class="inline-edit-group wp-clearfix">
			<label>
				<span class="title"><?php _e('Rate Based', 'mnswmc'); ?></span>
				<span class="input-text-wrap">
					<select class="mnswmc_active" name="_mnswmc_active">
						<option value="none"><?php _e('— No change —', 'mnswmc'); ?></option>
						<option value="yes"><?php _e('Yes', 'mnswmc'); ?></option>
						<option value="no"><?php _e('No', 'mnswmc'); ?></option>
					</select>
				</span>
			</label>
		</div>
		<div class="inline-edit-group wp-clearfix">
			<span class="input-text-wrap">
				<?php _e('The dependence type of the selected products from the bulk edit section is not editable, and if there is an advanced dependency (formulation) among the selected products, changes will not be considered on it (except rounding).', 'mnswmc'); ?>
			</span>
		</div>
		<div class="inline-edit-group wp-clearfix">
			<label>
				<span class="title"><?php _e('Choose Currency', 'mnswmc'); ?></span>
				<span class="input-text-wrap">
					<select class="mnswmc_currency_id" name="_mnswmc_currency_id">
						<option value="none"><?php _e('— No change —', 'mnswmc'); ?></option>
						<?php foreach ($currencies as $currency) {
							echo '<option value="' . $currency->ID . '">' . $currency->post_title . '</option>';
						} ?>
					</select>
				</span>
			</label>
		</div>
		<div class="inline-edit-group wp-clearfix">
			<label class="change-action">
				<span class="title"><?php _e('Rate Price', 'mnswmc'); ?></span>
				<span class="input-text-wrap">
					<select class="change_mnswmc_regular_price change_to" name="change_mnswmc_regular_price">
						<option value="none"><?php _e('— No change —', 'mnswmc'); ?></option>
						<option value="1"><?php _e('Change to', 'mnswmc'); ?></option>
						<option value="2"><?php _e('Price increase (Fixed)', 'mnswmc'); ?></option>
						<option value="3"><?php _e('Price increase (Percent)', 'mnswmc'); ?></option>
						<option value="4"><?php _e('Price decrease (Fixed)', 'mnswmc'); ?></option>
						<option value="5"><?php _e('Price decrease (Percent)', 'mnswmc'); ?></option>
					</select>
				</span>
			</label>
			<label class="change-input" style="display: none;">
				<span class="input-text-wrap">
					<input type="text" class="mnswmc_regular_price" name="_mnswmc_regular_price" value="">
				</span>
			</label>
		</div>
		<div class="inline-edit-group wp-clearfix">
			<label class="change-action">
				<span class="title" style="width: 5.5em; line-height: 1.25"><?php _e('Sale Rate Price', 'mnswmc'); ?></span>
				<span class="input-text-wrap">
					<select class="change_mnswmc_sale_price change_to" name="change_mnswmc_sale_price">
						<option value="none"><?php _e('— No change —', 'mnswmc'); ?></option>
						<option value="1"><?php _e('Change to', 'mnswmc'); ?></option>
						<option value="2"><?php _e('Price increase (Fixed)', 'mnswmc'); ?></option>
						<option value="3"><?php _e('Price increase (Percent)', 'mnswmc'); ?></option>
						<option value="4"><?php _e('Price decrease (Fixed)', 'mnswmc'); ?></option>
						<option value="5"><?php _e('Price decrease (Percent)', 'mnswmc'); ?></option>
					</select>
				</span>
			</label>
			<label class="change-input" style="display: none;">
				<span class="input-text-wrap">
					<input type="text" class="mnswmc_sale_price" name="_mnswmc_sale_price" value="">
				</span>
			</label>
		</div>
		<div class="inline-edit-group wp-clearfix">
			<label>
				<span class="title"><?php _e('Profit Margin', 'mnswmc'); ?></span>
				<span class="input-text-wrap">
					<input type="text" class="mnswmc_profit_margin" name="_mnswmc_profit_margin" placeholder="<?php _e('Empty for no change', 'mnswmc'); ?>" value="" style="max-width: 15em;">
					<select class="mnswmc_profit_type" name="_mnswmc_profit_type">
						<option value="none"><?php _e('— No change —', 'mnswmc'); ?></option>
						<option value="percent"><?php _e('Percent', 'mnswmc'); ?></option>
						<option value="fixed"><?php echo __('Fixed', 'mnswmc') . ' (' . get_woocommerce_currency_symbol() . ')'; ?></option>
					</select>
				</span>
			</label>
		</div>
		<div class="inline-edit-group wp-clearfix">
			<label>
				<span class="title"><?php _e('Rounding', 'mnswmc'); ?></span>
				<span class="input-text-wrap">
					<select class="mnswmc_price_rounding" name="_mnswmc_price_rounding">
						<option value="none"><?php _e('— No change —', 'mnswmc'); ?></option>
						<?php foreach(mnswmc_price_rounding_items() as $key => $value) {
							echo '<option value="' . $key . '">' . $value . ($key == 1 ? '' : ' ' . get_woocommerce_currency_symbol()) . '</option>';
						} ?>
					</select>
					<select class="mnswmc_rounding_type" name="_mnswmc_rounding_type">
						<option value="none"><?php _e('— No change —', 'mnswmc'); ?></option>
						<option value="zero"><?php _e('To zero', 'mnswmc'); ?></option>
						<option value="nine"><?php _e('To nine', 'mnswmc'); ?></option>
						<option value="hybrid"><?php _e('Hybrid', 'mnswmc'); ?></option>
					</select>
					<select class="mnswmc_rounding_side" name="_mnswmc_rounding_side">
						<option value="none"><?php _e('— No change —', 'mnswmc'); ?></option>
						<option value="close"><?php _e('Close', 'mnswmc'); ?></option>
						<option value="up"><?php _e('Up', 'mnswmc'); ?></option>
						<option value="down"><?php _e('Down', 'mnswmc'); ?></option>
					</select>
				</span>
			</label>
		</div>
	</div>
</fieldset>

	<?php
}
add_action( 'bulk_edit_custom_box', 'mnswmc_bulk_edit_box_product', 10, 2 );


function mnswmc_product_bulk_edit_save() {

	if(!wp_doing_ajax())
		exit();

	if(!isset($_POST['nonce']))
		exit();

	if(!wp_verify_nonce($_POST['nonce'], 'mnswmc_bulk_edit_product_nonce'))
		exit();

	if( empty( $_POST['post_ids'] ) or !is_array( $_POST[ 'post_ids' ] ) ) {
		exit();
	}

	foreach( $_POST['post_ids'] as $id ) {

		if( get_post_type($id) != 'product' )
			continue;

		$product = wc_get_product($id);

		if(!$product->is_type('simple') and !$product->is_type('external'))
			continue;

		$product_id = $product->get_id();

		// check active
		if( isset( $_POST['_mnswmc_active'] ) and $_POST['_mnswmc_active'] != 'none' ) {
			if( $_POST['_mnswmc_active'] == 'yes' ) {
				update_post_meta( $product_id, '_mnswmc_active', 'yes' );
			} else {
				mnswmc_delete_all_product_price_fields($product_id, 'product');
				continue;
			}
		}

		// check dependence type
		$dependence_type = mnswmc_get_product_dependence_type($product_id);

		if($dependence_type != 'simple')
			continue;

		// check price rounding
		if( isset($_POST['_mnswmc_price_rounding']) and $_POST['_mnswmc_price_rounding'] != 'none' ) {
			if( is_numeric($_POST['_mnswmc_price_rounding']) and $_POST['_mnswmc_price_rounding'] >= '0' ) {	
				update_post_meta( $product_id, '_mnswmc_price_rounding', sanitize_text_field( $_POST['_mnswmc_price_rounding'] ) );
			} else {
				update_post_meta( $product_id, '_mnswmc_price_rounding', '0' );
			}
		}
		
		// check rounding type
		if( isset($_POST['_mnswmc_rounding_type']) and $_POST['_mnswmc_rounding_type'] != 'none' ) {
			if( in_array($_POST['_mnswmc_rounding_type'], array('zero', 'nine', 'hybrid')) ) {
				update_post_meta( $product_id, '_mnswmc_rounding_type', sanitize_text_field( $_POST['_mnswmc_rounding_type'] ) );
			} else {
				update_post_meta( $product_id, '_mnswmc_rounding_type', 'zero' );
			}
		}

		// check rounding side
		if( isset($_POST['_mnswmc_rounding_side']) and $_POST['_mnswmc_rounding_side'] != 'none' ) {
			if( in_array($_POST['_mnswmc_rounding_side'], array('close', 'up', 'down')) ) {
				update_post_meta( $product_id, '_mnswmc_rounding_side', sanitize_text_field( $_POST['_mnswmc_rounding_side'] ) );
			} else {
				update_post_meta( $product_id, '_mnswmc_rounding_side', 'close' );
			}
		}
		
		// check currency id
		if( isset($_POST['_mnswmc_currency_id']) and $_POST['_mnswmc_currency_id'] != 'none' ) {
			if( is_numeric($_POST['_mnswmc_currency_id']) ) {
				update_post_meta( $product_id, '_mnswmc_currency_id', sanitize_text_field( $_POST['_mnswmc_currency_id'] ) );
			} else {
				mnswmc_delete_all_product_price_fields($product_id, 'product');
				continue;
			}
		}

		// check profit type
		if( isset($_POST['_mnswmc_profit_type']) and $_POST['_mnswmc_profit_type'] != 'none' ) {
			if( in_array($_POST['_mnswmc_profit_type'], array('percent', 'fixed')) ) {
				update_post_meta( $product_id, '_mnswmc_profit_type', sanitize_text_field( $_POST['_mnswmc_profit_type'] ) );
			} else {
				update_post_meta( $product_id, '_mnswmc_profit_type', 'fixed' );
			}
		}

		// check profit margin
		if( isset($_POST['_mnswmc_profit_margin']) and $_POST['_mnswmc_profit_margin'] != '' ) {
			if( is_numeric($_POST['_mnswmc_profit_margin']) ) {

				if(get_post_meta($product_id, '_mnswmc_profit_type', true) == 'percent') {
					if($_POST['_mnswmc_profit_margin'] < -100)
						$_POST['_mnswmc_profit_margin'] = -100;
				}
				update_post_meta( $product_id, '_mnswmc_profit_margin', sanitize_text_field( $_POST['_mnswmc_profit_margin'] ) );
				
			} else {
				update_post_meta( $product_id, '_mnswmc_profit_margin', '0' );
			}
		}

		// check regular price
		if( isset($_POST['change_mnswmc_regular_price']) and $_POST['change_mnswmc_regular_price'] != 'none' ) {

			$current_regular_price = sanitize_text_field(get_post_meta($product_id, '_mnswmc_regular_price', true));

			switch (sanitize_text_field( $_POST['change_mnswmc_regular_price'] )) {
				case '1':// Change to
					if( !empty($_POST['_mnswmc_regular_price']) and is_numeric($_POST['_mnswmc_regular_price']) and $_POST['_mnswmc_regular_price'] > 0 ) {
						update_post_meta( $product_id, '_mnswmc_regular_price', sanitize_text_field( $_POST['_mnswmc_regular_price'] ) );
					} else {
						update_post_meta( $product_id, '_mnswmc_regular_price', '0' );
					}
					break;
				
				case '2':// Price increase (Fixed)
					if( !empty($_POST['_mnswmc_regular_price']) and is_numeric($_POST['_mnswmc_regular_price']) and $_POST['_mnswmc_regular_price'] > 0 ) {
						$new_regular_price = $current_regular_price + sanitize_text_field($_POST['_mnswmc_regular_price']);
						update_post_meta( $product_id, '_mnswmc_regular_price', $new_regular_price );
					}
					break;

				case '3':// Price increase (Percent)
					if( !empty($_POST['_mnswmc_regular_price']) and is_numeric($_POST['_mnswmc_regular_price']) and $_POST['_mnswmc_regular_price'] > 0 ) {
						$new_regular_price = $current_regular_price + ($current_regular_price * sanitize_text_field($_POST['_mnswmc_regular_price']) / 100);
						update_post_meta( $product_id, '_mnswmc_regular_price', $new_regular_price );
					}
					break;

				case '4':// Price decrease (Fixed)
					if( !empty($_POST['_mnswmc_regular_price']) and is_numeric($_POST['_mnswmc_regular_price']) and $_POST['_mnswmc_regular_price'] > 0 ) {
						$new_regular_price = $current_regular_price - sanitize_text_field($_POST['_mnswmc_regular_price']);
						$new_regular_price = ($new_regular_price > 0) ? $new_regular_price : '0';
						update_post_meta( $product_id, '_mnswmc_regular_price', $new_regular_price );
					}
					break;

				case '5':// Price decrease (Percent)
					if( !empty($_POST['_mnswmc_regular_price']) and is_numeric($_POST['_mnswmc_regular_price']) and $_POST['_mnswmc_regular_price'] > 0 ) {
						$new_regular_price = $current_regular_price - ($current_regular_price * sanitize_text_field($_POST['_mnswmc_regular_price']) / 100);
						$new_regular_price = ($new_regular_price > 0) ? $new_regular_price : '0';
						update_post_meta( $product_id, '_mnswmc_regular_price', $new_regular_price );
					}
					break;
									
				default:
					// nothing
					break;
			}
		}

		// check sale price
		if( isset($_POST['change_mnswmc_sale_price']) and $_POST['change_mnswmc_sale_price'] != 'none' ) {

			$current_regular_price = sanitize_text_field(get_post_meta($product_id, '_mnswmc_regular_price', true));
			$current_sale_price = sanitize_text_field(get_post_meta($product_id, '_mnswmc_sale_price', true));

			switch (sanitize_text_field( $_POST['change_mnswmc_sale_price'] )) {
				case '1':// Change to
					if( !empty($_POST['_mnswmc_sale_price']) and is_numeric($_POST['_mnswmc_sale_price']) and $_POST['_mnswmc_sale_price'] >= 0 and $_POST['_mnswmc_sale_price'] < $current_regular_price ) {
						update_post_meta( $product_id, '_mnswmc_sale_price', sanitize_text_field( $_POST['_mnswmc_sale_price'] ) );
					} else {
						delete_post_meta( $product_id, '_mnswmc_sale_price' );
					}
					break;
				
				case '2':// Price increase (Fixed)
					if( !empty($_POST['_mnswmc_sale_price']) and is_numeric($_POST['_mnswmc_sale_price']) and $_POST['_mnswmc_sale_price'] > 0 ) {
						$new_sale_price = $current_sale_price + sanitize_text_field($_POST['_mnswmc_sale_price']);
						if($current_regular_price > $new_sale_price) {
							update_post_meta( $product_id, '_mnswmc_sale_price', $new_sale_price );
						} else {
							delete_post_meta( $product_id, '_mnswmc_sale_price' );
						}
					}
					break;

				case '3':// Price increase (Percent)
					if( !empty($_POST['_mnswmc_sale_price']) and is_numeric($_POST['_mnswmc_sale_price']) and $_POST['_mnswmc_sale_price'] > 0 ) {
						$new_sale_price = $current_sale_price + ($current_sale_price * sanitize_text_field($_POST['_mnswmc_sale_price']) / 100);
						if($current_regular_price > $new_sale_price) {
							update_post_meta( $product_id, '_mnswmc_sale_price', $new_sale_price );
						} else {
							delete_post_meta( $product_id, '_mnswmc_sale_price' );
						}
					}
					break;

				case '4':// Price decrease (Fixed)
					if( !empty($_POST['_mnswmc_sale_price']) and is_numeric($_POST['_mnswmc_sale_price']) and $_POST['_mnswmc_sale_price'] > 0 ) {
						$new_sale_price = $current_sale_price - sanitize_text_field($_POST['_mnswmc_sale_price']);
						$new_sale_price = ($new_sale_price > 0) ? $new_sale_price : '0';
						if($current_regular_price > $new_sale_price) {
							update_post_meta( $product_id, '_mnswmc_sale_price', $new_sale_price );
						} else {
							delete_post_meta( $product_id, '_mnswmc_sale_price' );
						}
					}
					break;

				case '5':// Price decrease (Percent)
					if( !empty($_POST['_mnswmc_sale_price']) and is_numeric($_POST['_mnswmc_sale_price']) and $_POST['_mnswmc_sale_price'] > 0 ) {
						$new_sale_price = $current_sale_price - ($current_sale_price * sanitize_text_field($_POST['_mnswmc_sale_price']) / 100);
						$new_sale_price = ($new_sale_price > 0) ? $new_sale_price : '0';
						if($current_regular_price > $new_sale_price) {
							update_post_meta( $product_id, '_mnswmc_sale_price', $new_sale_price );
						} else {
							delete_post_meta( $product_id, '_mnswmc_sale_price' );
						}
					}
					break;
									
				default:
					// nothing
					break;
			}
		}

		mnswmc_update_product_obj_price($product_id);
	}
	exit();
}
add_action( 'wp_ajax_mnswmc_product_bulk_edit_save', 'mnswmc_product_bulk_edit_save' ); 
//add_action( 'woocommerce_product_bulk_edit_save', 'mnswmc_product_bulk_edit_save', 10, 1 );