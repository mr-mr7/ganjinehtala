<?php

function mnswmc_template_currency_chart($currency_id, $labels = 'day', $data_count = 30) {

	$data = mnswmc_get_currency_records($currency_id);

	if(count($data) < 2) {
		echo '<p>' . __('This currency must have at least two days records to display chart.', 'mnswmc') . '</p>';
		return;
	}

	wp_enqueue_style('mnswmc_chart_css');
	wp_enqueue_script('mnswmc_chart_js');
	wp_enqueue_script('mnswmc_number_js');

	// sort array from small index to large
	ksort($data);

	//$data = array_reverse($data, true);

	$data_count = $data_count = -1 * abs($data_count);
	$data = array_slice($data, $data_count, null, true);

	$labels = array();
	$values = array();
	$rates = array();

	foreach ($data as $rate_time => $rate_values) {
		$labels[] = date_i18n('j F Y', $rate_time);
		$values[] = $rate_values['value'];
		$rates[] = $rate_values['rate'];
	}

	$labels = json_encode($labels);
	$values = json_encode($values);
	$rates = json_encode($rates);

?>

<div class="mnswmc mnswmc-currency-chart-meta-box wp-clearfix" data-symbol="<?php echo get_woocommerce_currency_symbol(); ?>" style="height: 400px;">
	<input type="hidden" class="mnswmc-currency-chart-labels" value='<?php echo $labels; ?>'>
	<input type="hidden" class="mnswmc-currency-chart-values" value='<?php echo $values; ?>'>
	<input type="hidden" class="mnswmc-currency-chart-rates" value='<?php echo $rates; ?>'>
	<canvas class="mnswmc-currency-chart" id="chart-<?php echo $currency_id; ?>"></canvas>
</div>

<?php
}