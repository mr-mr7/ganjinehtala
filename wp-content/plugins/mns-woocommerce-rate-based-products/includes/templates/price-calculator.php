<?php

if (!defined('ABSPATH')) {
	exit;
}

function mnswmc_price_calculator_template($c = '0', $a = '0', $s = 'l') {

	$a = $a == '0' ? true : current_user_can('manage_options');

	if (!$a) {

		_e('You do not have access to this section.', 'mnswmc');
		return;
	}

	wp_enqueue_style('mnswmc_public_css');
	wp_enqueue_script('mnswmc_public_js');
	wp_enqueue_script('mnswmc_number_js');

	$unique = rand(1000, 9999);
	$c = $c == '0' ? array() : explode(',', $c);
	$currencies = mnswmc_get_currencies($c); ?>

	<div class="mnswmc mnswmc-price-calculator <?php echo $s == '0' ? 'small' : 'large'; ?>" data-unique="<?php echo $unique; ?>">
		<?php if(count($currencies) == 1) { ?>
		<p class="mnswmc_calculator_currency_id" style="display: none;">
			<?php mnswmc_currencies_list($currencies, 'mnswmc_calculator_currency_id_' . $unique, 'mnswmc_calculator_field', '', $currencies[0]->ID); ?>
		</p>
		<?php } else { ?>
		<p class="mnswmc_calculator_currency_id">
			<label for="mnswmc_calculator_currency_id_<?php echo $unique; ?>"><span><?php _e('Choose Currency', 'mnswmc'); ?></span></label>
			<?php mnswmc_currencies_list($currencies, 'mnswmc_calculator_currency_id_' . $unique, 'mnswmc_calculator_field', ''); ?>
		</p>
		<?php } ?>
		<p class="mnswmc_calculator_regular_price show_if_mnswmc_currency_selected">
			<label for="mnswmc_calculator_regular_price_<?php echo $unique; ?>"><span><?php _e('Rate Price', 'mnswmc'); ?></span></label>
			<input type="text" id="mnswmc_calculator_regular_price_<?php echo $unique; ?>" class="mnswmc_calculator_field" value=""> <span class="description"></span>
		</p>
		<p class="mnswmc_calculator_sale_price show_if_mnswmc_currency_selected">
			<label for="mnswmc_calculator_sale_price_<?php echo $unique; ?>"><span><?php _e('Sale Rate Price', 'mnswmc'); ?></span></label>
			<input type="text" id="mnswmc_calculator_sale_price_<?php echo $unique; ?>" class="mnswmc_calculator_field" value=""> <span class="description"></span>
		</p>
		<p class="mnswmc_calculator_profit_margin show_if_mnswmc_currency_selected">
			<label for="mnswmc_calculator_profit_margin_<?php echo $unique; ?>"><?php _e('Profit Margin', 'mnswmc'); ?></label>
			<input type="text" id="mnswmc_calculator_profit_margin_<?php echo $unique; ?>" class="mnswmc_calculator_field" value="">
			<select class="mnswmc_calculator_field" id="mnswmc_calculator_profit_type_<?php echo $unique; ?>">
				<option value="percent"><?php _e('Percent', 'mnswmc'); ?></option>
				<option value="fixed"><?php echo __('Fixed', 'mnswmc') . ' (' . get_woocommerce_currency_symbol() . ')'; ?></option>
			</select>
		</p>
		<p class="mnswmc_calculator_price_rounding show_if_mnswmc_currency_selected">
			<label for="mnswmc_calculator_price_rounding_<?php echo $unique; ?>"><span><?php _e('Rounding', 'mnswmc'); ?></span></label>
			<select class="mnswmc_calculator_field" id="mnswmc_calculator_rounding_type_<?php echo $unique; ?>">
				<option value="zero"><?php _e('To zero', 'mnswmc'); ?></option>
				<option value="nine"><?php _e('To nine', 'mnswmc'); ?></option>
				<option value="hybrid"><?php _e('Hybrid', 'mnswmc'); ?></option>
			</select>
			<select class="mnswmc_calculator_field" id="mnswmc_calculator_price_rounding_<?php echo $unique; ?>">
				<?php foreach (mnswmc_price_rounding_items() as $key => $value) {
					echo '<option value="' . $key . '">' . $value . ($key == 1 ? '' : ' ' . get_woocommerce_currency_symbol()) . '</option>';
				} ?>
			</select>
			<select class="mnswmc_calculator_field" id="mnswmc_calculator_rounding_side_<?php echo $unique; ?>">
				<option value="close"><?php _e('Close', 'mnswmc'); ?></option>
				<option value="up"><?php _e('Up', 'mnswmc'); ?></option>
				<option value="down"><?php _e('Down', 'mnswmc'); ?></option>
			</select>
		</p>
		<p class="mnswmc_currency_rate_preview show_if_mnswmc_currency_selected">
			<label><?php _e('Currency Rate', 'mnswmc'); ?></label>
			<code>0</code> <?php echo get_woocommerce_currency_symbol(); ?>
		</p>
		<p class="mnswmc_regular_price_preview show_if_mnswmc_regular_price_filled">
			<label><?php _e('Final Price', 'mnswmc'); ?></label>
			<code>0</code> <?php echo get_woocommerce_currency_symbol(); ?>
		</p>
		<p class="mnswmc_sale_price_preview show_if_mnswmc_sale_price_filled">
			<label><?php _e('Final Sale Price', 'mnswmc'); ?></label>
			<code>0</code> <?php echo get_woocommerce_currency_symbol(); ?>
		</p>
	</div>

<?php }