<?php

if (!defined('ABSPATH')) {
	exit;
}

function mnswmc_rate_converter_template($currencies = '0', $admin = '0', $size = 'large') {
	
	$admin = $admin == '0' ? true : current_user_can('manage_options');

	if (!$admin) {
		_e('You do not have access to this section.', 'mnswmc');
		return;
	}

	wp_enqueue_style('mnswmc_public_css');
	wp_enqueue_script('mnswmc_public_js');
	wp_enqueue_script('mnswmc_number_js');

	$currencies = $currencies == '0' ? array() : explode(',', $currencies);
	$currencies = mnswmc_get_currencies($currencies);
	$symbol = get_woocommerce_currency_symbol();

	?>

<div class="mnswmc mnswmc-rate-converter <?php echo $size == 'small' ? 'small' : 'large'; ?> wp-clearfix mnswmc-clearfix">
	<div class="mnswmc-converter-side converter-from">
		<div class="converter-select">
			<select class="mnswmc-rate-converter-field">
				<option value="1"><?php echo $symbol; ?></option>
				<?php foreach ($currencies as $currency) {
					echo '<option value="' . mnswmc_get_currency_rate($currency->ID) . '">' . get_the_title($currency->ID) . '</option>';
				} ?>
			</select>
		</div>
		<div class="converter-input">
			<input type="text" class="mnswmc-rate-converter-field" value="1">
		</div>
	</div>
	<div class="mnswmc-converter-exchange">
		<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="exchange-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M0 168v-16c0-13.255 10.745-24 24-24h360V80c0-21.367 25.899-32.042 40.971-16.971l80 80c9.372 9.373 9.372 24.569 0 33.941l-80 80C409.956 271.982 384 261.456 384 240v-48H24c-13.255 0-24-10.745-24-24zm488 152H128v-48c0-21.314-25.862-32.08-40.971-16.971l-80 80c-9.372 9.373-9.372 24.569 0 33.941l80 80C102.057 463.997 128 453.437 128 432v-48h360c13.255 0 24-10.745 24-24v-16c0-13.255-10.745-24-24-24z"></path></svg>
	</div>
	<div class="mnswmc-converter-side converter-to">
		<div class="converter-select">
			<select class="mnswmc-rate-converter-field">
				<option value="1"><?php echo $symbol; ?></option>
				<?php foreach ($currencies as $currency) {
					echo '<option value="' . mnswmc_get_currency_rate($currency->ID) . '">' . get_the_title($currency->ID) . '</option>';
				} ?>
			</select>
		</div>
		<div class="converter-input">
			<input type="text" class="mnswmc-rate-converter-field" value="1">
		</div>
	</div>
</div>

	<?php
}