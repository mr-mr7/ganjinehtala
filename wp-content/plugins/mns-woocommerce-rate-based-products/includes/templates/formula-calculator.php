<?php

if (!defined('ABSPATH')) {
	exit;
}

function mnswmc_formula_calculator_metabox_template($post_id, $post_type, $data = array(), $fields = array()) {

	$currencies = mnswmc_get_currencies();
	$formulas = mnswmc_get_formulas();
	$currency_symbol = get_woocommerce_currency_symbol();

	$allow_types = array('product', 'formula');
	$post_type = in_array($post_type, $allow_types) ? $post_type : 'product';

	$fields['formul'] = isset($fields['formul']) ? $fields['formul'] : '_mnswmc_formula_formul';
	$fields['variables'] = isset($fields['variables']) ? $fields['variables'] : '_mnswmc_formula_variables';
	$fields['variables_counter'] = isset($fields['variables_counter']) ? $fields['variables_counter'] : '_mnswmc_formula_variables_counter';

	$data['formul'] = isset($data['formul']) ? $data['formul'] : '';
	$data['variables'] = (isset($data['variables']) and is_array($data['variables'])) ? $data['variables'] : array();
	$data['variables_counter'] = (isset($data['variables_counter']) and is_numeric($data['variables_counter'])) ? $data['variables_counter'] : '1';

	$placeholder_data = array();
	$placeholder_data[0] = $data;
	if($post_type == 'product') {
		foreach ($formulas as $formula) {
			$placeholder_data[$formula->ID] = mnswmc_get_formula_data($formula->ID);
		}
	}
?>

	<label for="<?php echo $fields['formul']; ?>"><?php _e('Formul', 'mnswmc'); ?></label>
	<textarea class="mnswmc-formula-formul mnswmc_formula_field code" id="<?php echo $fields['formul']; ?>" name="<?php echo $fields['formul']; ?>"><?php echo $data['formul']; ?></textarea>
	<span class="clear" style="display: block;"></span>

	<span class="mnswmc-formula-results wp-clearfix">
		<span class="mnswmc-formula-validation" style="display: none;">
			<i class="dashicons dashicons-yes"></i>
			<span class="log" data-valid="<?php _e('The formula is correct!', 'mnswmc'); ?>" data-error="<?php _e('The formula is not correct!', 'mnswmc'); ?>" data-empty="<?php _e('The formula is empty!', 'mnswmc'); ?>"></span>
		</span>
		<span class="mnswmc-formula-result" style="display: none;"><?php _e('Final Price:', 'mnswmc'); ?> <code>0</code> <?php echo $currency_symbol; ?></span>
		<span class="mnswmc-formula-sale-result" style="display: none;"><?php _e('Final Sale Price:', 'mnswmc'); ?> <code>0</code> <?php echo $currency_symbol; ?></span>
	</span>

	<span class="mnswmc-formula-variables-new">
		<?php

		echo wc_help_tip(__('Enter variables related to your product. You can use a variable multiple times', 'mnswmc'));
		mnswmc_currencies_list($currencies, '', 'mnswmc-formula-add-currency-variable', '', null, __('+ Add Currency Variable', 'mnswmc'));

		if ($post_type == 'formula')
			echo ' <a class="button mnswmc-formula-add-custom-variable">' . __('+ Add Custom Variable', 'mnswmc') . '</a>';

		?>
		
		<span class="variable variable-placeholder wp-clearfix" data-rate="" data-field-name="<?php echo $fields['variables']; ?>" data-symbol="<?php echo $currency_symbol; ?>" style="display: none;">
			<span class="column variable-name">
				<strong><?php _e('Variable Name:', 'mnswmc'); ?></strong>
				<span><input type="text" class="mnswmc_formula_field" value=""></span>
			</span>
			<span class="column variable-rate">
				<strong><?php _e('Rate:', 'mnswmc'); ?></strong>
				<span><input type="text" class="mnswmc_formula_field" value="" placeholder="1"></span>
			</span>
			<span class="column variable-unit">
				<strong><?php $post_type == 'formula' ? _e('Default Value:', 'mnswmc') : _e('Value:', 'mnswmc'); ?></strong>
				<span><input type="text" class="mnswmc_formula_field" value="" placeholder="0"></span>
			</span>
			<span class="column variable-sale">
				<strong><?php $post_type == 'formula' ? _e('Default Sale Value:', 'mnswmc') : _e('Sale Value:', 'mnswmc'); ?></strong>
				<span><input type="text" class="mnswmc_formula_field" value=""></span>
			</span>
			<span class="column variable-code"><strong><?php _e('Code:', 'mnswmc'); ?></strong><span><code></code></span></span>
			<span class="column variable-actions">
				<strong><?php _e('Actions:', 'mnswmc'); ?></strong>
				<span class="dashicons var-remove dashicons-no"></span>
			</span>
		</span>
		<input id="<?php echo $fields['variables_counter']; ?>" class="mnswmc-formula-variables-counter" name="<?php echo $fields['variables_counter']; ?>" type="hidden" value="<?php echo $data['variables_counter']; ?>">
	</span>

	<input class="mnswmc-formula-data-placeholder" type="hidden" data-type="<?php echo $post_type; ?>" value='<?php echo json_encode($placeholder_data); ?>'>

	<span class="mnswmc-formula-variables-rows"></span>

<?php
}