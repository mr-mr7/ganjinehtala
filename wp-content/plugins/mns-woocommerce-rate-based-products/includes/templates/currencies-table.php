<?php

function mnswmc_template_currencies_table($currencies = '0', $columns = 'all') {

	wp_enqueue_style('mnswmc_public_css');
	wp_enqueue_script('mnswmc_public_js');
	wp_enqueue_script('mnswmc_number_js');

	$currencies = $currencies == '0' ? array() : explode(',', $currencies);
	$currencies = mnswmc_get_currencies($currencies);

	echo '<table class="mnswmc mnswmc-currencies-table">';
	echo '<thead><tr>';
	echo '<th>' . __('Currency', 'mnswmc') . '</th>';
	echo '<th>' . __('Rate', 'mnswmc') . '</th>';
	echo '<th>' . __('Changes', 'mnswmc') . '</th>';
	echo '<th>' . __('Update Time', 'mnswmc') . '</th>';
	echo '</tr></thead>';
	echo '<tbody>';

	foreach ($currencies as $currency) {

		$update_time = mnswmc_get_currency_update_time($currency->ID);
		echo '<tr>';
		echo '<td>' . get_the_title($currency->ID) . '</td>';
		echo '<td>' . mnswmc_display_decimal_number(mnswmc_get_currency_rate($currency->ID)) . ' ' . get_woocommerce_currency_symbol() . '</td>';
		echo '<td>';
		mnswmc_template_percentage_change(mnswmc_get_currency_prev_record($currency->ID)['rate'], mnswmc_get_currency_rate($currency->ID));
		echo '</td>';
		echo '<td>' . str_replace(array('%DATE%', '%TIME%'), array(date_i18n('j F Y', $update_time), date_i18n('H:i:s', $update_time)), __('%DATE% at %TIME%', 'mnswmc')) . '</td>';
		echo '</tr>';
	}

	echo '</tbody>';
	echo '</table>';
}