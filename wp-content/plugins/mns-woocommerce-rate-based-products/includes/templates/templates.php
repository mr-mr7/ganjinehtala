<?php

if (!defined('ABSPATH')) {
	exit;
}

require_once (MNSWMC_DIR . '/includes/templates/currency-chart.php');
require_once (MNSWMC_DIR . '/includes/templates/price-calculator.php');
require_once (MNSWMC_DIR . '/includes/templates/rate-converter.php');
require_once (MNSWMC_DIR . '/includes/templates/formula-calculator.php');
require_once (MNSWMC_DIR . '/includes/templates/currencies-table.php');


function mnswmc_template_percentage_change($from, $to) {

	$change = mnswmc_calculate_change($from, $to);
	
	if($change > 0) {
		$direction = 'up';
		$sign = '<span class="dashicons dashicons-arrow-up-alt2"></span>';
	} else if($change < 0) {
		$direction = 'down';
		$sign = '<span class="dashicons dashicons-arrow-down-alt2"></span>';
	} else {
		$direction = 'none';
		$sign = '<span class="dashicons dashicons-minus"></span>';
	}

	echo '<span class="mnswmc-percentage-change mnswmc-change-' . $direction . '">' . $sign . mnswmc_sanitize_number($change) . '%</span>';
}