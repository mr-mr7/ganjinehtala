<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require_once (MNSWMC_DIR . '/includes/columns/formula.php');

// Add the custom columns
function mnswmc_set_currencies_columns($columns) {

	unset($columns['date']);
	$columns['currency_id'] = __( 'ID', 'mnswmc' );
	$columns['currency_value'] = __( 'Base Rate', 'mnswmc' );
	$columns['currency_lowest_rate'] = __( 'Lowest Rate', 'mnswmc' );
	$columns['currency_profit'] = __( 'Profit', 'mnswmc' );
	$columns['currency_fee'] = __( 'Fee', 'mnswmc' );
	$columns['currency_rate'] = __( 'Final Rate', 'mnswmc' );
	$columns['currency_attributes'] = __( 'Attributes', 'mnswmc' );

	return $columns;
}
add_filter( 'manage_mnswmc_posts_columns', 'mnswmc_set_currencies_columns' );

// Add the custom sortable columns
function mnswmc_set_currencies_sortable_columns( $columns ) {

	$columns['currency_value'] = 'currency_value';
	$columns['currency_lowest_rate'] = 'currency_lowest_rate';
	$columns['currency_profit'] = 'currency_profit';
	$columns['currency_fee'] = 'currency_fee';
	$columns['currency_rate'] = 'currency_rate';

	return $columns;
}
add_filter( 'manage_edit-mnswmc_sortable_columns', 'mnswmc_set_currencies_sortable_columns' );

// Add the data to the custom columns
function mnswmc_currencies_columns( $column, $post_id ) {

	$prev_records = mnswmc_get_currency_prev_record($post_id);

	switch ( $column ) {
		case 'currency_id' :
			echo '<p><code>' . $post_id . '</code>';
			break;
		case 'currency_value' :
			echo '<p><code>' . mnswmc_display_decimal_number(mnswmc_get_currency_value($post_id)) . '</code> <span>' . get_woocommerce_currency_symbol() . '</span></p>';
			echo '<p>' . mnswmc_template_percentage_change($prev_records['value'], mnswmc_get_currency_value($post_id)) . '</p>';
			break;
		case 'currency_lowest_rate' :
			echo '<p><code>' . mnswmc_display_decimal_number(mnswmc_get_currency_lowest_rate($post_id)) . '</code> <span>' . get_woocommerce_currency_symbol() . '</span></p>';
			break;
		case 'currency_profit' :
			echo '<p><code>' . mnswmc_sanitize_number(mnswmc_get_currency_profit($post_id), 2) . '</code> <span>%</span></p>';
			break;
		case 'currency_fee' :
			echo '<p><code>' . mnswmc_sanitize_number(mnswmc_get_currency_fee($post_id), 2) . '</code> <span>%</span></p>';
			break;
		case 'currency_rate' :
			echo '<p><code>' . mnswmc_display_decimal_number(mnswmc_get_currency_rate($post_id)) . '</code> <span>' . get_woocommerce_currency_symbol() . '</span></p>';
			echo '<p>' . mnswmc_template_percentage_change($prev_records['rate'], mnswmc_get_currency_rate($post_id)) . '</p>';
			break;
		case 'currency_attributes' :
			$update_type = mnswmc_get_currency_update_type($post_id);
			$connection = mnswmc_get_currency_connection($post_id);

			if($update_type == 'none') {
				echo '<p><span class="dashicons dashicons-dismiss"></span> ' . __('Manual update', 'mnswmc') . '</p>';
			} else {
				if(isset(mnswmc_get_webservices()[$update_type])) {
					echo '<p><span class="dashicons dashicons-yes-alt"></span> ' . __('Auto update', 'mnswmc') . ': ' . mnswmc_get_webservices()[$update_type]['name'] . '</p>';
				} else {
					echo '<p><span class="dashicons dashicons-warning"></span> ' . __('Removed webservice', 'mnswmc') . '</p>';
				}
			}
			if($connection) {
				echo '<p><span class="dashicons dashicons-admin-links"></span> ' . __('Connection with', 'mnswmc') . ': ' . get_the_title($connection) . '</p>';
			} else {
				echo '<p><span class="dashicons dashicons-editor-unlink"></span> ' . __('No connection', 'mnswmc') . ': ' . __('Independent currency', 'mnswmc') . '</p>';
			}
			break;
	}
}
add_action( 'manage_mnswmc_posts_custom_column' , 'mnswmc_currencies_columns', 10, 2 );

// edit row action items
function mnswmc_currencies_row_actions($actions, $post) {

	if($post->post_type != 'mnswmc' and $post->post_type != 'mnswmc-formula')
		return $actions;

	unset($actions['inline hide-if-no-js']);

	if($post->post_type == 'mnswmc') {
		$actions['base_currency'] = '<a href="' . admin_url('edit.php?&post_type=product&base_currency=' . $post->ID) . '">' . __('View products related to this currency', 'mnswmc') . '</a>';
	} elseif($post->post_type == 'mnswmc-formula') {
		$actions['base_formula'] = '<a href="' . admin_url('edit.php?&post_type=product&base_formula=' . $post->ID) . '">' . __('View products related to this formula', 'mnswmc') . '</a>';
	}

	return $actions;
}
add_filter('post_row_actions','mnswmc_currencies_row_actions', 10, 2);

// Fix query string for custom sortable columns
function mnswmc_custom_orderby( $query ) {

	if(!is_admin() or !$query->is_admin)
		return;
	
	$orderby = $query->get('orderby');
	switch ($orderby) {
		case 'currency_value':
			$query->set( 'meta_key', '_mnswmc_currency_value' );
			$query->set( 'orderby', 'meta_value_num' );
			break;

		case 'currency_lowest_rate':
			$query->set( 'meta_key', '_mnswmc_currency_lowest_rate' );
			$query->set( 'orderby', 'meta_value_num' );
			break;

		case 'currency_profit':
			$query->set( 'meta_key', '_mnswmc_currency_profit' );
			$query->set( 'orderby', 'meta_value_num' );
			break;

		case 'currency_fee':
			$query->set( 'meta_key', '_mnswmc_currency_fee' );
			$query->set( 'orderby', 'meta_value_num' );
			break;

		case 'currency_rate':
			$query->set( 'meta_key', '_mnswmc_currency_rate' );
			$query->set( 'orderby', 'meta_value_num' );
			break;
												
		default:
			break;
	}
	
	if ( isset( $_GET['base_currency'] ) and is_numeric($_GET['base_currency']) ) {
		$meta_key_query = array(
			array(
				'key'     => '_mnswmc_currency_ids',
				'value'   => sanitize_text_field($_GET['base_currency']),
				'compare' => 'LIKE',
				'type'    => 'CHAR'
			)
		);
		$query->set( 'meta_query', $meta_key_query );
	}

	if ( isset( $_GET['base_formula'] ) and is_numeric($_GET['base_formula']) ) {
		$meta_key_query = array(
			array(
				'key'     => '_mnswmc_formula_ids',
				'value'   => sanitize_text_field($_GET['base_formula']),
				'compare' => 'LIKE',
				'type'    => 'CHAR'
			)
		);
		$query->set( 'meta_query', $meta_key_query );
	}
	
	return $query;
}
add_action( 'pre_get_posts', 'mnswmc_custom_orderby' );