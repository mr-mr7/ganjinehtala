<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Add the custom columns
function mnswmc_set_formula_columns($columns) {

	unset($columns['date']);
	$columns['formula_id'] = __( 'ID', 'mnswmc' );
	$columns['formula_variable_count'] = __( 'Variable Count', 'mnswmc' );

	return $columns;
}
add_filter( 'manage_mnswmc-formula_posts_columns', 'mnswmc_set_formula_columns' );

// Add the data to the custom columns
function mnswmc_formula_columns( $column, $post_id ) {
	switch ( $column ) {
		case 'formula_id' :
			echo '<p><code>' . $post_id . '</code>';
			break;
		case 'formula_variable_count' :
			$data = mnswmc_get_formula_data($post_id);
			$count = count($data['variables']);
			echo ($count > 0) ? $count : __('No variables', 'mnswmc');
			break;
	}
}
add_action( 'manage_mnswmc-formula_posts_custom_column' , 'mnswmc_formula_columns', 10, 2 );