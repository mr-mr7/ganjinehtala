<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function mnswmc_settings_slug() {

	return 'mns-rate-based';
}

function mnswmc_settings_url() {

	return esc_url(admin_url('admin.php?page=' . mnswmc_settings_slug()));
}

function mnswmc_set_wc_screen_ids($screen) {

	$screen[] = sanitize_title(__( 'WooCommerce', 'woocommerce' )) . '_page_' . mnswmc_settings_slug();
	return $screen;
}
add_filter('woocommerce_screen_ids', 'mnswmc_set_wc_screen_ids');

function mnswmc_settings_menu_item() {
	
	add_submenu_page('woocommerce', __('Rate Based Products Settings', 'mnswmc'), __('Rate Based', 'mnswmc'), 'manage_options', mnswmc_settings_slug(), 'mnswmc_settings_page', 99999);
}
add_action("admin_menu", "mnswmc_settings_menu_item");

function mnswmc_settings_sections() {

	// array: tab name, custom page, subs
	$settings_sections = array(
		'general' => array(__('General', 'mnswmc'), false, array()),
		'webservices' => array(__('Webservices', 'mnswmc'), false, array()),
		'more' => array(__('More Products', 'mnswmc'), true, array()),
	);

	$settings_sections = apply_filters('mnswmc_settings_tabs', $settings_sections);
	return $settings_sections;
}

function mnswmc_settings_page() {

	$active_tab = isset($_GET['tab']) ? sanitize_text_field($_GET['tab']) : 'general';
	$active_sub = isset($_GET['sub']) ? sanitize_text_field($_GET['sub']) : 'general';

	$settings_sections = mnswmc_settings_sections();

	echo '<div class="wrap woocommerce mnswmc mns-rate-based-settings">';
	echo '<h1>' . __('MNS Rate Based Settings', 'mnswmc') . '</h1>';

	settings_errors();

	echo '<nav class="nav-tab-wrapper">';
	foreach ($settings_sections as $tab_key => $tab_val) {
		echo '<a href="?page=' . mnswmc_settings_slug() . '&tab=' . $tab_key . '" class="nav-tab' . ($active_tab == $tab_key ? ' nav-tab-active' : '') . '">' . $tab_val[0] . '</a>';
	}
	echo '</nav>';

	echo '<form method="post" action="options.php">';

	if(isset($settings_sections[$active_tab])) {

		$this_tab = $settings_sections[$active_tab];

		if(is_array($this_tab[2]) and count($this_tab[2]) > 0) {
			echo '<ul class="subsubsub">';
			$index = 1;
			foreach ($this_tab[2] as $sub_key => $sub_val) {
				echo '<li><a href="?' . http_build_query(array(
					'page' => mnswmc_settings_slug(),
					'tab' => $active_tab,
					'sub' => $sub_key
				)) . '"' . (($active_sub == $sub_key) ? ' class="current"' : '') .'>' . $sub_val[0] . '</a></li>';
				echo ((count($this_tab[2]) > $index) ? ' | ' : '');
				$index++;
			}
			echo '</ul>';
			echo '<br class="clear">';
		}

		$custom_page = isset($this_tab[2][$active_sub]) ? $this_tab[2][$active_sub][1] : $this_tab[1];

		if($custom_page === true) {

			if(file_exists(MNSWMC_DIR . '/includes/settings/pages/' . $active_tab . '-' . $active_sub . '.php'))
				require_once (MNSWMC_DIR . '/includes/settings/pages/' . $active_tab . '-' . $active_sub . '.php');
			
		} elseif(is_string($custom_page)) {

			if(file_exists($custom_page))
				require_once ($custom_page);

		} elseif($custom_page === false) {

			settings_fields('mnswmc_settings_' . $active_tab . '_' . $active_sub . '_group');
			do_settings_sections('mnswmc_settings_' . $active_tab . '_' . $active_sub . '_page');
			submit_button();
		}
	}

	echo '</form>';
	echo '</div>';
}

require_once (MNSWMC_DIR . '/includes/settings/register.php');


function mnswmc_admin_footer_text($content) {

	if(get_current_screen()->id != sanitize_title(__( 'WooCommerce', 'woocommerce' )) . '_page_' . mnswmc_settings_slug())
		return $content;
	
	$five_star = '<a href="' . mnswmc_info('url') . '" target="_blank">★★★★★</a>';
	$message = sprintf( __( 'If you like %s, please give us %s rating. Thank you in advance :)', 'mnswmc' ), mnswmc_info('name'), $five_star );
    return $message;
}
add_filter( 'admin_footer_text', 'mnswmc_admin_footer_text', 9999 );