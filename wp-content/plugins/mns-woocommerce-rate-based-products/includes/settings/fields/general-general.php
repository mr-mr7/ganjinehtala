<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function mnswmc_settings_license_output() {

	if(mnswmc_check_activation()) {
		if(mnswmc_is_conditional_activation_activated()) {
			$license_token = '';
			$license_token_attr = '';
			$submit_class = 'install';
			$submit_value = __('Install license', 'mnswmc');
			$notice_class = 'notice-warning';
			$notice_value = __('The plugin is temporarily activated and will be deactivated after a while if the license is not installed.', 'mnswmc');
		} else {
			$license_token = mnswmc_asterisk_license_key(esc_attr(get_option('mnswmc_license_token')));
			$license_token_attr = ' disabled="disabled"';
			$submit_class = 'uninstall';
			$submit_value = __('Uninstall license', 'mnswmc');
			$notice_class = 'notice-success';
			$notice_value = __('License key registered!', 'mnswmc');
		}
	} else {
		$license_token = '';
		$license_token_attr = '';
		$submit_class = 'install';
		$submit_value = __('Install license', 'mnswmc');
		$notice_class = 'notice-warning';
		$notice_value = __('Enter your license key', 'mnswmc');
	}
	?>

<div class="mnswmc-license-form wp-clearfix" data-nonce="<?php echo wp_create_nonce('mnswmc_license_nonce'); ?>">
	<input type="text" id="mnswmc_license_token" class="regular-text code" value="<?php echo $license_token; ?>" placeholder="xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"<?php echo $license_token_attr; ?>>
	<a href="#submit-license" id="mnswmc_license_submit" class="button <?php echo $submit_class; ?>" data-install="<?php _e('Install license', 'mnswmc'); ?>" data-loading="<?php _e('Wait...', 'mnswmc'); ?>" data-uninstall="<?php _e('Uninstall license', 'mnswmc'); ?>"><?php echo $submit_value; ?></a>
	<div class="clear"></div>
	<p class="notice notice-alt <?php echo $notice_class; ?> message"><span><?php echo $notice_value; ?></span></p>
	<p class="description"><?php _e( 'Enter your license key to enable the plugin fully. You can get the license key from your Panel on Zhaket.', 'mnswmc' ); ?></p>
</div>	
<?php }

function mnswmc_settings_rest_api_output() {
	
	$token = mnswmc_rest_api_main_token();
	?>
	
	<div class="mnswmc-rest-api-option" data-nonce="<?php echo wp_create_nonce('mnswmc_change_rest_api_main_token_nonce'); ?>">
		<label for="mnswmc_rest_api_active">
			<input type="checkbox" id="mnswmc_rest_api_active" name="mnswmc_rest_api_active" value="yes" <?php checked('yes', get_option('mnswmc_rest_api_active')); ?>> <?php _e( 'Active', 'mnswmc' ); ?>
		</label>
		<p class="description"><?php _e('Access Token', 'mnswmc'); ?>: <code class="token"><?php echo $token; ?></code> <a href="#" id="mnswmc-change-rest-api-main-token"><?php _e('Change Token', 'mnswmc'); ?></a></p>
		<p class="description"><?php _e('Request URL', 'mnswmc'); ?>: <code class="url"><?php echo mnswmc_rest_api_request_url('currency', $token); ?></code></p>
		<br>
		<p class="description"><?php _e( 'By REST API you can call currencies you created in this website in your other applications or websites. This section needs technical knowledge. Turn off this option if you do not know or do not need it.', 'mnswmc' ); ?></p>
	</div>

	<?php }

function mnswmc_settings_telegram_bot_output() {

	$token = mnswmc_get_telegram_bot_token();
	$account_code = mnswmc_telegram_bot_add_account_code();

	$bot_submit_display = $token ? 'none' : 'block';
	$bot_account_display = $token ? 'block' : 'none';

	?>

<div class="mnswmc mnswmc-telegram-bot-option" data-nonce="<?php echo wp_create_nonce('mnswmc_telegram_bot_nonce'); ?>">

	<div class="telegram-bot-submit" style="display: <?php echo $bot_submit_display; ?>">
		<div class="button" id="mnswmc-active-telegram-bot-button"><?php _e('Active', 'mnswmc'); ?></div>
		<input class="code mnswmc-telegram-fields" type="text" id="mnswmc-telegram-bot-token-input" placeholder="<?php _e('Bot Token', 'mnswmc'); ?>" style="display: none;">
		<div class="button mnswmc-telegram-fields" id="mnswmc-telegram-bot-token-submit" data-text="<?php _e('Submit Token', 'mnswmc'); ?>" data-wait="<?php _e('Connecting...', 'mnswmc'); ?>" style="display: none;"><?php _e('Submit Token', 'mnswmc'); ?></div>
		<div class="button mnswmc-telegram-fields" id="mnswmc-telegram-bot-token-cancel" style="display: none;"><?php _e('Cancel', 'mnswmc'); ?></div>
		<div class="message" style="display: none"></div>
	</div>

	<div class="telegram-bot-account" style="display: <?php echo $bot_account_display; ?>">
		<p class="description"><?php _e('Bot Token', 'mnswmc'); ?>: <code class="token"><?php echo $token; ?></code>
		<a href="#" id="mnswmc-telegram-bot-token-delete" data-text="<?php _e('Delete Token', 'mnswmc'); ?>" data-wait="<?php _e('Deleting...', 'mnswmc'); ?>"><?php _e('Delete Token', 'mnswmc'); ?></a></p>
		<p class="description"><?php _e('Add Account Code', 'mnswmc'); ?>: <code class="account-code"><?php echo $account_code; ?></code></p>
		<br>
		<p class="description"><?php _e('To add a telegram account to bot list, you have to send unique "Add Account Code" to this bot. In each account addition, account code will be changed.', 'mnswmc'); ?></p>
	</div>
</div>
<p class="description"><?php _e('By activing Telegram bot, on each currency rate change, a notification to accounts you add will be sent.', 'mnswmc'); ?></p>

	<?php
}

function mnswmc_settings_telegram_bot_product_alert_output() {
	
	$active = mnswmc_get_option_telegram_bot_alert();
	?>
	
	<label for="mnswmc_telegram_bot_product_alert">
		<input type="checkbox" id="mnswmc_telegram_bot_product_alert" name="mnswmc_telegram_bot_product_alert" value="yes" <?php checked('yes', $active); ?>> <?php _e( 'Active', 'mnswmc' ); ?>
	</label>
	<p class="description"><?php _e('By activating this option, if the Telegram bot is launched, the price changes of the selected products will be notified.<br>Note: If the number of selected products is very high, it may have a negative impact on the loading speed of your site.', 'mnswmc'); ?></p>

	<?php
}

function mnswmc_settings_global_rounding_output() {
	
	$rounding = mnswmc_get_option_global_rounding();
	
	?>
	
	<select name="mnswmc_global_rounding[value]" style="width: auto;">
		<?php foreach(mnswmc_price_rounding_items() as $key => $value) {
			echo '<option value="' . $key . '" ' . selected($key, $rounding['value'], false) . '>' . $value . ($key == '0' ? '' : ' ' . get_woocommerce_currency_symbol()) . '</option>';
		} ?>
	</select>
	<select name="mnswmc_global_rounding[type]" style="width: auto;">
		<option value="zero" <?php selected('zero', $rounding['type']); ?>><?php _e('To zero', 'mnswmc'); ?></option>
		<option value="nine" <?php selected('nine', $rounding['type']); ?>><?php _e('To nine', 'mnswmc'); ?></option>
		<option value="hybrid" <?php selected('hybrid', $rounding['type']); ?>><?php _e('Hybrid', 'mnswmc'); ?></option>
	</select>
	<select name="mnswmc_global_rounding[side]" style="width: auto;">
		<option value="close" <?php selected('close', $rounding['side']); ?>><?php _e('Close', 'mnswmc'); ?></option>
		<option value="up" <?php selected('up', $rounding['side']); ?>><?php _e('Up', 'mnswmc'); ?></option>
		<option value="down" <?php selected('down', $rounding['side']); ?>><?php _e('Down', 'mnswmc'); ?></option>
	</select>

	<p class="description"><?php _e( 'If rounding values are defined here, these values will be used in products where the rounding values are not defined. If you do not need this feature, you can disable it.', 'mnswmc' ); ?></p>
		
	<?php }