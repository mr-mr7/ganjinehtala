<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function mnswmc_settings_webservices_output() {
	
	?>
	<table class="mnswmc-webservices-table">
		<thead>
			<tr>
				<th class="active">
					<?php _e( 'Activation', 'mnswmc' ); ?> <?php echo wc_help_tip(__('Enable or disable web services', 'mnswmc')); ?>
				</th>
				<th class="api">
					<?php _e( 'API Key', 'mnswmc' ); ?> <?php echo wc_help_tip(__('The web service API key you bought', 'mnswmc')); ?>
				</th>
				<th class="schedule">
					<?php _e( 'Schedule', 'mnswmc' ); ?> <?php echo wc_help_tip(__('When is the currencies rate updated? Enter an interval according to your web service plan. Low amounts may be associated with slow loading on some servers!', 'mnswmc')); ?>
				</th>
				<th class="update">
					<?php _e( 'Last update', 'mnswmc' ); ?> <?php echo wc_help_tip(__('When was the last update?', 'mnswmc')); ?>
				</th>
			</tr>
		</thead>
	<?php foreach(mnswmc_get_webservices() as $webservice_id => $webservice) {
		
		$time = mnswmc_get_webservice_time($webservice_id);
		$passed = time() - $time;
		$passed_min = floor($passed / 60);
		$passed_sec = floor(fmod($passed / 60, 1) * 60);
		?>
	
	<tr>
		<td>
			<label for="mnswmc_webservices_options_<?php echo $webservice_id; ?>_active">
				<input type="checkbox" id="mnswmc_webservices_options_<?php echo $webservice_id; ?>_active" name="mnswmc_webservices_options[<?php echo $webservice_id; ?>][active]" value="yes" <?php checked('yes', mnswmc_get_webservice_option($webservice_id, 'active')); ?>>
				<?php echo $webservice['name']; ?>
			</label>
			<?php if($webservice['free']) { ?>
			<span class="api-lock tips dashicons dashicons-unlock" data-tip="<?php _e('Free web service', 'mnswmc'); ?>"></span>
			<?php } else { ?>
			<span class="api-lock tips dashicons dashicons-lock" data-tip="<?php _e('Paid web service', 'mnswmc'); ?>"></span>
			<?php } ?>
			<?php if(!empty($webservice['url'])) { ?>
			<a class="api-link tips" data-tip="<?php _e('Go to webservice website', 'mnswmc'); ?>" rel="nofollow" target="_blank" href="<?php echo esc_url($webservice['url']); ?>"><span class="dashicons dashicons-external"></span></a>
			<?php } ?>
		</td>
		<td>
			<?php if($webservice['license']) { ?>
			<input type="text" class="mnswmc-webservice-api code" name="mnswmc_webservices_options[<?php echo $webservice_id; ?>][apikey]" value="<?php echo mnswmc_get_webservice_option($webservice_id, 'apikey'); ?>">
			<?php } else { ?>
			<input type="text" class="mnswmc-webservice-api" value="<?php _e('No needs API key', 'mnswmc'); ?>" disabled="disabled">
			<?php } ?>
		</td>
		<td>
			<input type="number" min="1" step="1" class="mnswmc-webservice-interval" name="mnswmc_webservices_options[<?php echo $webservice_id; ?>][interval]" value="<?php echo mnswmc_get_webservice_option($webservice_id, 'interval'); ?>"> <?php _e( 'Minutes', 'mnswmc' ); ?>
		</td>
		<td>
			<?php echo mnswmc_get_webservice_option($webservice_id, 'active') ? sprintf(__( '<code>%s</code> minutes and <code>%s</code> seconds ago', 'mnswmc' ), $passed_min, $passed_sec) : __( 'Disabled', 'mnswmc' ); ?>
		</td>
	</tr>
	
	<?php }
	echo '</table>';
}