<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function mnswmc_settings_register() {

	require_once (MNSWMC_DIR . '/includes/settings/fields/general-general.php');

	add_settings_section('mnswmc_settings_general_general_section', __( 'General Settings', 'mnswmc' ), function() {
		echo '<p>' . __( 'MNS Rate Based general settings', 'mnswmc' ) . '</p>';
	}, 'mnswmc_settings_general_general_page');
	

	add_settings_field('mnswmc_license_field', __( 'License', 'mnswmc' ), 'mnswmc_settings_license_output', 'mnswmc_settings_general_general_page', 'mnswmc_settings_general_general_section', array( 'label_for' => ''));
	add_settings_field('mnswmc_global_rounding_field', __( 'Global Rounding', 'mnswmc' ), 'mnswmc_settings_global_rounding_output', 'mnswmc_settings_general_general_page', 'mnswmc_settings_general_general_section', array( 'label_for' => ''));
	add_settings_field('mnswmc_telegram_bot_field', __( 'Telegram Bot', 'mnswmc' ), 'mnswmc_settings_telegram_bot_output', 'mnswmc_settings_general_general_page', 'mnswmc_settings_general_general_section', array( 'label_for' => ''));
	add_settings_field('mnswmc_telegram_bot_product_alert_field', __( 'Product Alert', 'mnswmc' ), 'mnswmc_settings_telegram_bot_product_alert_output', 'mnswmc_settings_general_general_page', 'mnswmc_settings_general_general_section', array( 'label_for' => ''));
	add_settings_field('mnswmc_rest_api_field', __( 'REST API', 'mnswmc' ), 'mnswmc_settings_rest_api_output', 'mnswmc_settings_general_general_page', 'mnswmc_settings_general_general_section', array( 'label_for' => ''));
	
	register_setting('mnswmc_settings_general_general_group', 'mnswmc_telegram_bot_product_alert');
	register_setting('mnswmc_settings_general_general_group', 'mnswmc_rest_api_active');
	register_setting('mnswmc_settings_general_general_group', 'mnswmc_global_rounding');
	
	require_once (MNSWMC_DIR . '/includes/settings/fields/webservices-general.php');

	add_settings_section('mnswmc_settings_webservices_general_section', __( 'Webservices Settings', 'mnswmc' ), function() {
		echo '<p>' . __( 'MNS Rate Based webservices settings', 'mnswmc' ) . '</p>';
	}, 'mnswmc_settings_webservices_general_page');

	add_settings_field('mnswmc_webservices_field', __( 'Webservices', 'mnswmc' ), 'mnswmc_settings_webservices_output', 'mnswmc_settings_webservices_general_page', 'mnswmc_settings_webservices_general_section', array( 'label_for' => ''));

	register_setting('mnswmc_settings_webservices_general_group', 'mnswmc_webservices_options');
}

add_action('admin_init', 'mnswmc_settings_register');