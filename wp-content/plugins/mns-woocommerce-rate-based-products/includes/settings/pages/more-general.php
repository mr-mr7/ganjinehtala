<?php

if(!defined('ABSPATH'))
	exit;

function mnswmc_settings_mns_products() {

	echo '<div class="mns-container mns-container-products mnswmc" data-nonce="1">';

	echo '<div class="mns-product mns-product-placeholder wp-clearfix" style="display: none;">';
	echo '<a class="mns-product-url" href="" rel=""nofollow" target="_blank">';
	echo '<div class="mns-product-cover"></div>';
	echo '<h3 class="mns-product-title"></h3>';
	echo '<div class="mns-product-price"><span></span> ' . __('Toman', 'mnswmc') . '</div>';
	echo '</a>';
	echo '</div>';

	echo '</div>';
}

mnswmc_settings_mns_products();