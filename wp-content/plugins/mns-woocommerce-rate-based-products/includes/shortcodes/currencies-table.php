<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function mnswmc_currencies_table_shortcode( $atts ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'currencies' => '0',
		),
		$atts,
		'mnswmc_currencies_table'
	);

	ob_start();
	mnswmc_template_currencies_table($atts['currencies']);
	$return = ob_get_clean();

	return $return;
}
add_shortcode( 'mnswmc_currencies_table', 'mnswmc_currencies_table_shortcode' );