<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require_once (MNSWMC_DIR . '/includes/shortcodes/price-calculator.php');
require_once (MNSWMC_DIR . '/includes/shortcodes/rate-converter.php');
require_once (MNSWMC_DIR . '/includes/shortcodes/currency-update-time.php');
require_once (MNSWMC_DIR . '/includes/shortcodes/currency-chart.php');
require_once (MNSWMC_DIR . '/includes/shortcodes/currency-change.php');
require_once (MNSWMC_DIR . '/includes/shortcodes/currencies-table.php');

// Add Products Shortcode
function mnswmc_product_shortcode( $atts ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'id' => '0',
			'param' => 'rate_price',
		),
		$atts
	);
	$allowed_values = array('currency_id', 'rate_price', 'rate_sale_price', 'round', 'profit', 'profit_type');
	
	if(get_post_type($atts['id']) != 'product')
		return;
	
	if(!in_array($atts['param'], $allowed_values))
		return;
	
	switch ($atts['param']) {
		case 'currency_id':
			$param = '_mnswmc_currency_id';
			break;
		case 'rate_price':
			$param = '_mnswmc_regular_price';
			break;
		case 'rate_sale_price':
			$param = '_mnswmc_sale_price';
			break;
		case 'round':
			$param = '_mnswmc_price_rounding';
			break;
		case 'profit':
			$param = '_mnswmc_profit_margin';
			break;
		case 'profit_type':
			$param = '_mnswmc_profit_type';
			break;
		default:
			$param = null;
			break;
	}

	return esc_attr(get_post_meta($atts['id'], $param, true));
}
add_shortcode( 'mnswmc_product', 'mnswmc_product_shortcode' );

// Add Product Formula Variables Shortcode
function mnswmc_product_formula_variables_shortcode( $atts ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'id' => '0',
			'code' => null,
			'param' => 'rate_price'
		),
		$atts
	);
	$allowed_params = array('rate_price', 'rate_sale_price');

	if(!$atts['code'])
		return;
	
	if(get_post_type($atts['id']) != 'product')
		return;
	
	if(!in_array($atts['param'], $allowed_params))
		return;
	
	$variables = get_post_meta($atts['id'], '_mnswmc_variables', true);

	if(!is_array($variables) or !isset($variables[$atts['code']]))
		return;

	if($atts['param'] == 'rate_price') {
		if(isset($variables[$atts['code']]['value'])) {
			return $variables[$atts['code']]['value'];
		} else {
			return;
		}
	} else {
		if(isset($variables[$atts['code']]['sale'])) {
			return $variables[$atts['code']]['sale'];
		} else {
			return;
		}
	}
}
add_shortcode( 'mnswmc_product_formula_variables', 'mnswmc_product_formula_variables_shortcode' );

// Add Currencies Shortcode
function mnswmc_currency_shortcode( $atts ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'id' => '0',
			'param' => 'rate',
			'format' => '0',
			'fa' => '0'
		),
		$atts
	);
	$allowed_values = array('value', 'profit', 'fee', 'rate');
	
	if(get_post_type($atts['id']) != 'mnswmc')
		return;
	
	if(!in_array($atts['param'], $allowed_values))
		return;

	switch ($atts['param']) {
		case 'value':
			$value = mnswmc_get_currency_value($atts['id']);
			break;

		case 'profit':
			$value = mnswmc_get_currency_profit($atts['id']);
			break;

		case 'fee':
			$value = mnswmc_get_currency_fee($atts['id']);
			break;

		case 'rate':
			$value = mnswmc_get_currency_rate($atts['id']);
			break;
								
		default:
			break;
	}

	if($atts['format'] == '1') {
		$value = mnswmc_display_number($value);
	}

	if($atts['fa'] == '1') {
		$value = mnswmc_en_num_to_fa($value);
	}
	
	return $value;
}
add_shortcode( 'mnswmc_currency', 'mnswmc_currency_shortcode' );