<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Add Currencies Shortcode
function mnswmc_price_calculator_shortcode( $atts ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'c' => '0',
			'a' => '0',
			's' => '0'
		),
		$atts,
		'mnswmc_price_calculator'
	);
	
	ob_start();
	mnswmc_price_calculator_template($atts['c'], $atts['a'], $atts['s']);
	return ob_get_clean();
}
add_shortcode( 'mnswmc_price_calculator', 'mnswmc_price_calculator_shortcode' );