<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Add Currencies Shortcode
function mnswmc_rate_converter_shortcode( $atts ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'currencies' => '0',
			'admin' => '0',
			'size' => 'large'
		),
		$atts,
		'mnswmc_rate_converter'
	);
	
	ob_start();
	mnswmc_rate_converter_template($atts['currencies'], $atts['admin'], $atts['size']);
	return ob_get_clean();
}
add_shortcode( 'mnswmc_rate_converter', 'mnswmc_rate_converter_shortcode' );