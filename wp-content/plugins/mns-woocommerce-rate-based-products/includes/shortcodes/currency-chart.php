<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Add Currencies Shortcode
function mnswmc_currency_chart_shortcode( $atts ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'id' => '0',
		),
		$atts,
		'mnswmc_currency_chart'
	);

	wp_enqueue_style( 'mnswmc_chart_css' );
	wp_enqueue_script( 'mnswmc_chart_js' );
	wp_enqueue_script( 'mnswmc_public_js' );

	ob_start();
	mnswmc_template_currency_chart($atts['id'], 'day', mnswmc_get_currency_max_records());
	$return = ob_get_clean();

	return $return;
}
add_shortcode( 'mnswmc_currency_chart', 'mnswmc_currency_chart_shortcode' );