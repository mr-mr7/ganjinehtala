<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Add Currencies Shortcode
function mnswmc_currency_change_shortcode( $atts ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'id' => '0',
			'param' => 'rate'
		),
		$atts,
		'mnswmc_currency_change'
	);

	wp_enqueue_style( 'mnswmc_public_css' );

	$prev_records = mnswmc_get_currency_prev_record($atts['id']);

	if($atts['param'] == 'rate') {
		$return = mnswmc_template_percentage_change($prev_records['rate'], mnswmc_get_currency_rate($atts['id']));
	} else {
		$return = mnswmc_template_percentage_change($prev_records['value'], mnswmc_get_currency_value($atts['id']));
	}

	return $return;
}
add_shortcode( 'mnswmc_currency_change', 'mnswmc_currency_change_shortcode' );