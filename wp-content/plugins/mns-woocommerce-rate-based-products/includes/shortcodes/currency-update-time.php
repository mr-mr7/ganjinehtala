<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Add Currencies Shortcode
function mnswmc_currency_update_time_shortcode( $atts ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'id' => '0',
			'format' => __('%DATE% at %TIME%', 'mnswmc')
		),
		$atts,
		'mnswmc_currency_update_time'
	);
	
	$update = mnswmc_get_currency_update_time($atts['id']);
	$date = date_i18n('j F Y', $update);
	$time = date_i18n('H:i:s', $update);

	$return = str_replace(array('%DATE%', '%TIME%'), array($date, $time), $atts['format']);

	return $return;
}
add_shortcode( 'mnswmc_currency_update_time', 'mnswmc_currency_update_time_shortcode' );