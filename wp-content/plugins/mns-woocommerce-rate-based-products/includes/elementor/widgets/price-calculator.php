<?php
/**
 * Elementor Price Calculator Widget.
 *
 * @since 1.0.0
 */
class MNSWMC_Elementor_Price_Calculator_Widget extends \Elementor\Widget_Base {

	public function get_script_depends() {
		return [ 'mnswmc_public_js', 'mnswmc_number_js' ];
	}

	public function get_style_depends() {
		return [ 'mnswmc_public_css' ];
	}

	/**
	 * Get widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'mnswmc-price-calculator';
	}

	/**
	 * Get widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Price Calculator', 'mnswmc' );
	}

	/**
	 * Get widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-calculator';
	}

	/**
	 * Get widget categories.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'mnswmc' ];
	}

	/**
	 * Register widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Options', 'mnswmc' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'currencies',
			[
				'label' => __( 'Currencies', 'mnswmc' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'input_type' => 'text',
				'placeholder' => '123,234,345 or 0',
			]
		);

		$this->add_control(
			'admin',
			[
				'label' => __( 'Only Admin View', 'mnswmc' ),
				'type' => \Elementor\Controls_Manager::CHOOSE,
				'options' => [
					'1' => [
						'title' => __( 'Yes', 'mnswmc' ),
						'icon' => 'fa fa-check',
					],
					'0' => [
						'title' => __( 'No', 'mnswmc' ),
						'icon' => 'fa fa-times',
					],
				],
				'default' => '0',
			]
		);

		$this->add_control(
			'size',
			[
				'label' => __( 'Size', 'mnswmc' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => [
					'l' => __( 'Large', 'mnswmc' ),
					'0' => __( 'Small', 'mnswmc' ),
				],
				'default' => 'l',
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {

		$settings = $this->get_settings_for_display();

		mnswmc_price_calculator_template($settings['currencies'], $settings['admin'], $settings['size']);

		return;
	}

}