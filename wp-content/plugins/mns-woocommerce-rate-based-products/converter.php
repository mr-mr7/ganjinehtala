<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$mnswmc_converted_product_count = 0;

// run for 6-12 months
function mnswmc_conver_if_not_check_variable($product_id, $meta_key, $value) {

	if($value != '')
		return $value;

	$old_meta_value = get_post_meta($product_id, '_variable' . $meta_key, true);

	if($old_meta_value != '')
		return $old_meta_value;

	return $value;
}

function mnswmc_convert_five_product_metas($product_id) {

	global $mnswmc_converted_product_count;

	if($mnswmc_converted_product_count >= 10)
		return;

	$mnswmc_converted_product_count++;

	$meta_keys = array(
		'_mnswmc_active',
		'_mnswmc_dependence_type',
		'_mnswmc_price_rounding',
		'_mnswmc_rounding_type',
		'_mnswmc_rounding_side',
		'_mnswmc_formula_id',
		'_mnswmc_formula',
		'_mnswmc_variables',
		'_mnswmc_variables_counter',
		'_mnswmc_currency_id',
		'_mnswmc_regular_price',
		'_mnswmc_sale_price',
		'_mnswmc_profit_margin',
		'_mnswmc_profit_type',	
	);

	foreach ($meta_keys as $meta_key) {

		$value = get_post_meta($product_id, '_variable' . $meta_key, true);

		if($value == '')
			continue;

		update_post_meta($product_id, $meta_key, $value);
		delete_post_meta($product_id, '_variable' . $meta_key);
	}
}

function mnswmc_convert_delete_old_metas($product_id) {

	if(get_post_meta($product_id, '_variable_mnswmc_active', true ) != 'yes')
		return;

	$meta_keys = array(
		'_mnswmc_active',
		'_mnswmc_dependence_type',
		'_mnswmc_price_rounding',
		'_mnswmc_rounding_type',
		'_mnswmc_rounding_side',
		'_mnswmc_formula_id',
		'_mnswmc_formula',
		'_mnswmc_variables',
		'_mnswmc_variables_counter',
		'_mnswmc_currency_id',
		'_mnswmc_regular_price',
		'_mnswmc_sale_price',
		'_mnswmc_profit_margin',
		'_mnswmc_profit_type',	
	);

	foreach ($meta_keys as $meta_key) {
		delete_post_meta($product_id, '_variable' . $meta_key);
	}
}

function mnswmc_convert_webservices_values() {

	$options = mnswmc_get_option_webservices_options();

	if(!empty($options))
		return;

	$webservices = mnswmc_get_webservices();
	$options = array();
	$times = array();

	foreach ($webservices as $webservice_id => $value) {

		$options[$webservice_id] = array(
			'active' => get_option('mnswmc_settings_field_webservice_active_' . $webservice_id),
			'apikey' => get_option('mnswmc_settings_field_webservice_api_' . $webservice_id),
			'interval' => get_option('mnswmc_settings_field_webservice_interval_' . $webservice_id)
		);

		$times[$webservice_id] = get_option('mnswmc_api_update_time_' . $webservice_id, 0);

		$data = get_option('mnswmc_api_data_' . $webservice_id);
		$data = is_array($data) ? $data : array();
		update_option('mnswmc_webservice_data_' . $webservice_id, $data);
	}

	update_option('mnswmc_webservices_options', $options);
	update_option('mnswmc_webservices_times', $times);
}

mnswmc_convert_webservices_values();