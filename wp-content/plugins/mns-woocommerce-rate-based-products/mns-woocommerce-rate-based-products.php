<?php

/**
 * Plugin Name: MNS WooCommerce Rate Based Products
 * Plugin URI: https://www.zhaket.com/web/mns-woocommerce-rate-based-products
 * Description: This plugin adds extra dynamics currencies and can add variable prices!
 * Version: 5.2.0
 * Author: Hooman Mansuri
 * Author URI: https://hoomanmns.com/
 * Developer: Hooman Mansuri
 * Developer URI: https://www.zhaket.com/store/web/mnsdev
 * Text Domain: mnswmc
 * Domain Path: /languages
 *
 * WC requires at least: 4.0.0
 * WC tested up to: 5.4.0
 *
 * Copyright: © 2020 MNS.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// Define important values
//define( 'MNSWMC_DEMO_TUTORIAL', 'ID' );
define( 'MNSWMC_VERSION', '5.2.0' );
define( 'MNSWMC_ROOT', __FILE__ );
define( 'MNSWMC_DIR', plugin_dir_path( MNSWMC_ROOT ) );
define( 'MNSWMC_URL', plugin_dir_url( MNSWMC_ROOT ) );

// Minimum requirements
define( 'MNSWMC_WP_MIN', '5.0.0' );
define( 'MNSWMC_WC_MIN', '4.0.0' );
define( 'MNSWMC_SG_MIN', '11.4.0' );
define( 'MNSWMC_IC_MIN', '10.0.0' );
define( 'MNSWMC_PHP_MIN', '7.0.0' );

// Add plugin support for Translation
function mnswmc_load_textdomain() {
	load_plugin_textdomain( 'mnswmc', false, basename( dirname( __FILE__ ) ) . '/languages' );
}
add_action('plugins_loaded', 'mnswmc_load_textdomain');

// Add plugin row meta
function mnswmc_row_meta( $plugin_meta, $plugin_file, $plugin_data, $status ) {

	if(strpos( $plugin_file, basename(__FILE__))) {

		$plugin_meta[] = '<a href="https://t.me/codimns">' . __('Telegram Channel', 'mnswmc') . '</a>';
		$plugin_meta[] = '<a href="https://www.zhaket.com/web/mns-woocommerce-rate-based-products/support">' . __('Support', 'mnswmc') . '</a>';
	}
	return $plugin_meta;
}
add_filter( 'plugin_row_meta', 'mnswmc_row_meta', 10, 4 );

// Plugin info
function mnswmc_info($info) {

	switch ($info) {
		case 'name':
			return __('MNS WooCommerce Rate Based Products', 'mnswmc');
			break;
		case 'version':
			return MNSWMC_VERSION;
			break;
		case 'url':
			return esc_url('https://www.zhaket.com/web/mns-woocommerce-rate-based-products');
			break;
								
		default:
			return null;
			break;
	}
}

// Enqueueing Admin Assets
function mnswmc_admin_assets() {
	wp_enqueue_style( 'mnswmc_admin_css', MNSWMC_URL . 'assets/css/admin.css', false, MNSWMC_VERSION );
	wp_register_style( 'mnswmc_chart_css', MNSWMC_URL . 'assets/css/chart.min.css', false, '2.9.4' );
	wp_register_script( 'mnswmc_chart_js', MNSWMC_URL . 'assets/js/chart.min.js', array( 'jquery' ), '2.9.4', false );
	wp_enqueue_script( 'mnswmc_admin_js', MNSWMC_URL . 'assets/js/scripts.js', array( 'mnswmc_common_js' ), MNSWMC_VERSION, false );
	wp_enqueue_script( 'mnswmc_common_js', MNSWMC_URL . 'assets/js/common.js', array( 'jquery', 'wp-i18n' ), MNSWMC_VERSION, false );
	wp_enqueue_script( 'mnswmc_number_js', MNSWMC_URL . 'assets/js/jquery.number.min.js', array( 'jquery' ), '2.1.5', false );
	wp_enqueue_script( 'mnswmc_formula_parser_js', MNSWMC_URL . 'assets/js/formula-parser.min.js', array( 'mnswmc_admin_js' ), '3.0.2', false );

	wp_enqueue_style( 'mnswmc_public_css', MNSWMC_URL . 'assets/css/public.css', false, MNSWMC_VERSION );
	wp_enqueue_script( 'mnswmc_public_js', MNSWMC_URL . 'assets/js/public.js', array( 'jquery' ), MNSWMC_VERSION, false );

	wp_set_script_translations( 'mnswmc_common_js', 'mnswmc-js', plugin_dir_path( __FILE__ ) . 'languages' );
}
add_action( 'admin_enqueue_scripts', 'mnswmc_admin_assets' );

// Enqueueing Public Assets
function mnswmc_public_assets() {
	wp_register_style( 'mnswmc_public_css', MNSWMC_URL . 'assets/css/public.css', false, MNSWMC_VERSION );
	wp_register_style( 'mnswmc_chart_css', MNSWMC_URL . 'assets/css/chart.min.css', false, '2.9.4' );
	wp_register_script( 'mnswmc_public_js', MNSWMC_URL . 'assets/js/public.js', array( 'mnswmc_common_js' ), MNSWMC_VERSION, false );
	wp_register_script( 'mnswmc_common_js', MNSWMC_URL . 'assets/js/common.js', array( 'jquery', 'wp-i18n' ), MNSWMC_VERSION, false );
	wp_register_script( 'mnswmc_number_js', MNSWMC_URL . 'assets/js/jquery.number.min.js', array( 'jquery' ), '2.1.5', false );
	wp_register_script( 'mnswmc_chart_js', MNSWMC_URL . 'assets/js/chart.min.js', array( 'jquery' ), '2.9.4', false );

	wp_set_script_translations( 'mnswmc_common_js', 'mnswmc-js', plugin_dir_path( __FILE__ ) . 'languages' );
}
add_action( 'wp_enqueue_scripts', 'mnswmc_public_assets' );

// Load notices
require_once (MNSWMC_DIR . '/includes/notices.php');

// Upgrade version
function mnswmc_version_upgrade() {

	$current_version = get_option('mnswmc_current_version', '0.0.1');

	if($current_version != MNSWMC_VERSION) {

		update_option('mnswmc_previous_version', $current_version);
		update_option('mnswmc_current_version', MNSWMC_VERSION);
	}
}

// Set install timestamp
function mnswmc_set_install_time() {

	$install_time = get_option('mnswmc_install_time');

	if(!$install_time)
		update_option('mnswmc_install_time', time());
}

// Do somethings when activate or deactivate
function mnswmc_activate_plugin() {

	if(function_exists('mnswmc_version_upgrade'))
		mnswmc_version_upgrade();

	if(function_exists('mnswmc_set_install_time'))
		mnswmc_set_install_time();
}
function mnswmc_deactivate_plugin() {

	delete_option('mnswmc_install_time');
}
register_activation_hook( MNSWMC_ROOT, 'mnswmc_activate_plugin' );
register_deactivation_hook( MNSWMC_ROOT, 'mnswmc_deactivate_plugin' );


// Check requirements and run plugin!
function mnswmc_run() {

	if(version_compare(PHP_VERSION, MNSWMC_PHP_MIN, '<')) {
		add_action( 'admin_notices', 'mnswmc_admin_php_update_notice__error' );
		return;
	}

	if(version_compare(get_bloginfo('version'), MNSWMC_WP_MIN, '<')) {
		add_action( 'admin_notices', 'mnswmc_admin_wordpress_update_notice__error' );
		return;
	}

	if(!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
		add_action( 'admin_notices', 'mnswmc_admin_woocommerce_notice__error' );
		return;
	}

	if(version_compare(get_option('woocommerce_version'), MNSWMC_WC_MIN, '<')) {
		add_action( 'admin_notices', 'mnswmc_admin_woocommerce_update_notice__error' );
		return;
	}

	if(!extension_loaded('ionCube Loader') or !function_exists('ioncube_loader_iversion')) {
		add_action( 'admin_notices', 'mnswmc_admin_ioncube_loader_notice__error' );
		return;
	}

	if(version_compare(implode('.', array_map('intval', str_split(ioncube_loader_iversion(), 2))), MNSWMC_IC_MIN, '<')) {
		add_action( 'admin_notices', 'mnswmc_admin_ioncube_loader_update_notice__error' );
		return;
	}

	require_once (MNSWMC_DIR . '/includes/functions.php');
	require_once (MNSWMC_DIR . '/includes/settings/settings.php');
	require_once (MNSWMC_DIR . '/includes/post-types/post-types.php');
	require_once (MNSWMC_DIR . '/includes/metaboxes/metaboxes.php');
	require_once (MNSWMC_DIR . '/includes/widgets/widgets.php');
	require_once (MNSWMC_DIR . '/includes/shortcodes/shortcodes.php');
	require_once (MNSWMC_DIR . '/includes/templates/templates.php');
	require_once (MNSWMC_DIR . '/includes/columns/columns.php');
	require_once (MNSWMC_DIR . '/includes/api/api.php');
	require_once (MNSWMC_DIR . '/includes/ajax/ajax.php');
	require_once (MNSWMC_DIR . '/includes/quick.php');
	require_once (MNSWMC_DIR . '/includes/get-price.php');
	require_once (MNSWMC_DIR . '/includes/elementor/elementor.php');
	require_once (MNSWMC_DIR . '/converter.php');

	add_action( 'wp_loaded', 'mnswmc_version_upgrade' );
	add_action( 'wp_loaded', 'mnswmc_set_install_time' );
	add_action( 'admin_notices', 'mnswmc_admin_rating_notice__info' );
}

mnswmc_run();