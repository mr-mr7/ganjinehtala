��    	      d      �       �   	   �   �   �   
   }     �  #   �  G   �       >       Y     y  �   �     T     f  =   |    �     �  >   �                         	                     Base Rate Custom formula is deprecated and will be completely eliminated in the future. Therefore, it is recommended to define a formula for your products. Final Rate Hooman Mansuri MNS WooCommerce Rate Based Products This plugin adds extra dynamics currencies and can add variable prices! https://hoomanmns.com/ https://www.zhaket.com/web/mns-woocommerce-rate-based-products Project-Id-Version: MNS WooCommerce Rate Based Products
POT-Creation-Date: 2021-03-10 20:42+0330
PO-Revision-Date: 2021-03-10 20:43+0330
Last-Translator: Hooman Mansuri
Language-Team: Hooman Mansuri
Language: fa_IR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2
X-Poedit-Basepath: ..
X-Domain: mnswmc-js
Plural-Forms: nplurals=1; plural=0;
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: mns-woocommerce-rate-based-products.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.php
 نرخ پایه فرمول اختصاصی منسوخ شده و در آینده کاملا حذف خواهد شد. بنابراین توصیه می‌شود برای محصولات خود فرمول تعریف کنید. نرخ نهایی هومن منصوری محصولات وابسته به نرخ ارز ووکامرس با استفاده از این افزونه شما می‌توانید به تعداد دلخواه ارزهای پایه ثبت کنید تا قیمت‌گذاری محصولاتتان را برمبنای نرخ ارزهای ثبت شده تعیین کنید! https://hoomanmns.com/ https://www.zhaket.com/web/mns-woocommerce-rate-based-products 