<?php

/**
 * Plugin Name: پلاگین اختصاصی گنجینه طلا
 * Plugin URI: https://www.t-tarh.ir
 * Description: پلاگین اختصاصی تنظیمات گنجینه طلا
 * Version: 1.0
 * Author: محمدرضا نادری
 * Author URI: https://t-tarh.com/
 * Developer: Mr-mr7
 * Developer URI: https://t-tarh.com/
 * Text Domain: گنجینه طلا
 * Domain Path: /languages
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// Define important values
//define( 'GTS_DEMO_TUTORIAL', 'ID' );
define( 'GTS_VERSION', '1.0.0' );
define( 'GTS_ROOT', __FILE__ );
define( 'GTS_DIR', plugin_dir_path( GTS_ROOT ) );
define( 'GTS_URL', plugin_dir_url( GTS_ROOT ) );


// Enqueueing Admin Assets
//function GTS_admin_assets() {
//
//}
//add_action( 'admin_enqueue_scripts', 'GTS_admin_assets' );

// Enqueueing Public Assets
function GTS_public_assets() {
    wp_enqueue_style( 'GTS_style_css', GTS_URL . 'assets/default/css/style.css', false, GTS_VERSION );
    wp_register_script( 'GTS_script_js', GTS_URL . 'assets/default/js/script.js', array( 'jquery' ), '2.9.4', false );
}
add_action( 'wp_enqueue_scripts', 'GTS_public_assets' );


// Check requirements and run plugin!
function GTS_run() {
	require_once (GTS_DIR . '/includes/functions.php');
    require_once (GTS_DIR . '/includes/templates/templates.php');
    require_once (GTS_DIR . '/includes/shortcodes/shortcodes.php');
}

GTS_run();