<?php

if (!defined('ABSPATH')) {
    exit;
}

function so_43922864_add_content($prod)
{
    global $product;
    $object_id = $product->get_id();
    $variables_value = get_post_meta($object_id, '_mnswmc_variables', true);
    $formula_id = get_post_meta($object_id, '_mnswmc_formula_id', true);
    $formula_variables = get_post_meta($formula_id, '_mnswmc_formula_variables', true);

    $regular_price = number_format(get_post_meta($object_id, '_regular_price', true));
    $sale_price = number_format(get_post_meta($object_id, '_sale_price', true));

    $th = '';
    $td = '';
    $value = -1;
    foreach ($formula_variables as $key => $item) {
        $title = '';
        if ($item['type'] == 'currency') {
            $post_id = explode('-', $key);
            $title = get_post_field('post_title', $post_id[0]);
            $value = get_post_meta($post_id[0], '_mnswmc_currency_value', true);
        } else if ($item['type'] == 'custom') {
            $title = $item['name'];
            $item = $variables_value["$key"];
            $value = $item["value"];
            if ($item) {
                $value = $item["value"];
            }
            $sale= null;
            if ($item['sale'] != '') {
                $sale = $item['sale'];
            }
        }
        if ($title != '' && $value != -1) {
            if ($value > 1000)
                $value = number_format($value);
            if ($sale > 1000)
                $sale = number_format($sale);
            $th .= "<th>$title</th>";
            if (!is_null($sale)) {
                $td .= "<td><del style='color: #ffc0c0'>$value</del> <span style='padding: 0 5px;'>$sale</span></td>";
            } else
                $td .= "<td>$value</td>";
        }
    }

    echo '<style>.table-calculate-price td,.table-calculate-price th {text-align: center!important;padding: 10px;} .table-calculate-price {font-size: small}</style>';
    echo '<div class="table-responsive">';
    echo '<table class="table table-calculate-price">';
    echo '<thead><tr>';
    echo $th;
    echo '</tr></thead>';
    echo '<tbody>';
    echo '<tr>';
    echo $td;
    echo '</tr>';
    echo '</tbody>';
    echo '</tfoot>';
    echo '<tr>';
    echo "<td colspan='4'>قیمت کل: $regular_price تومان</td>";
    echo "<td colspan='4'>قیمت با تخفیف: $sale_price تومان</td>";
    echo '</tr>';
    echo '</tfoot>';
    echo '</table>';
    echo '</div>';
}

add_action('woocommerce_single_product_summary', 'so_43922864_add_content', 15);