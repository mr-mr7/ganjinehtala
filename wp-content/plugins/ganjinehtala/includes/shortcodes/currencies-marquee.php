<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function GTS_currencies_marquee_shortcode( $atts ) {
	// Attributes
	$atts = shortcode_atts(
		array(
			'currencies' => '0',
		),
		$atts,
		'GTS_currencies_marquee'
	);

	ob_start();
	GTS_template_currencies_marquee($atts['currencies']);
	$return = ob_get_clean();

	return $return;
}
add_shortcode( 'GTS_currencies_marquee', 'GTS_currencies_marquee_shortcode' );