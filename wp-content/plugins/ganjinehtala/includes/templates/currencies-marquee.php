<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
function GTS_template_currencies_marquee($currencies = '0', $columns = 'all') {

	wp_enqueue_style('GTS_style_css');

    $currencies = $currencies == '0' ? array() : explode(',', $currencies);
    $currencies = mnswmc_get_currencies($currencies);

    echo '<style>.bwp-header .header-mobile {
                padding-top: 50px;
            }
          </style>';
    echo '<marquee onmouseover="this.stop();" onmouseout="this.start();" id="marq" direction="right" behavior="scroll" loop="infinite"
    style="position: fixed;top: 0;background: #ff8200;color: #ffff;z-index: 9999999;right: -11px;height: 35px;line-height: 35px;font-size: 16px;left: 0;margin: auto; text-align: left;">';
    foreach ($currencies as $currency) {
        echo get_the_title($currency->ID).': ';
        echo '<span style="font-weight: bold;">'.mnswmc_display_decimal_number(mnswmc_get_currency_rate($currency->ID)) . ' ' . get_woocommerce_currency_symbol().'</span>';
        echo ' <span style="margin-right: 25px;"></span>  ';
    }

    echo '</marquee>';
}