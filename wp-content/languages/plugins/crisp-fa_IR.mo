��          �            h     i     |     �     �     �     �     �     �  �   �     �  H   �  )     #   -     Q    d     �     �  -   �  
   �     �  =     *   O  &   z  �   �     �  �   �  r   =  &   �     �               	                  
                                               Connect with Crisp Connect with Crisp. Connected with Crisp. Crisp Crisp Settings Crisp is a Livechat plugin Go to my Crisp settings Go to my Inbox Loving Crisp <b style="color:red">♥</b> ? Rate us on the <a target="_blank" href="https://wordpress.org/support/plugin/crisp/reviews/?filter=5">Wordpress Plugin Directory</a> Reconfigure This link will redirect you to Crisp and configure your Wordpress. Magic You can now use Crisp from your homepage. http://wordpress.org/plugins/crisp/ https://crisp.chat PO-Revision-Date: 2019-06-15 09:41:30+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: GlotPress/3.0.0-alpha.2
Language: fa
Project-Id-Version: Plugins - Crisp Live Chat - Development (trunk)
 ارتباط با کریسپ ارتباط با کریسپ ارتباط داده شده با کریسپ. کریسپ تنظیمات کریسپ کریسپ یک افزونه گفتگوی آنلاین است برو به تنظیمات کریسپ من برو به صندوق ورودی من کریسپ را دوست دارید <b style="color:red">♥</b>؟ ما را در <a target="_blank" href="https://wordpress.org/support/plugin/crisp/reviews/?filter=5">فهرست افزونه های وردپرس</a> ارزیابی نمایید پیکربندی مجدد این پیوند شما را به کریسپ منتقل می‌کند تا بتوانید آن را پیکربندی نمایید. جادو شما هم اکنون میتوانید از صفحه خانه خود از کریسپ استفاده نمایید. http://fa.wordpress.org/plugins/crisp/ https://crisp.chat 