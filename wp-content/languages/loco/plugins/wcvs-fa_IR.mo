��    !      $      ,      ,     -     5     =     J  (   ]  [   �     �     �     �     �  H        \     b     h  u   m     �     �         
   !     ,     8     E     J  
   a     l     �  	   �  "   �  j   �     )     7  E   N  �  �     :     P  )   f  !   �  4   �  �   �     �     �     �     �  �   �  
   \	  
   g	     r	  �   y	     *
     E
  D   W
     �
     �
     �
     �
  %   �
  
     %      (   F     o  8   �  �   �     V     r  E   �   Add New Add new Add new term Added successfully All attributes types have been restored. An extension of WooCommerce to make variable products be more beauty and friendly to users. Cancel Choose an image Color Dismiss this notice Found a backup of product attributes types. This backup was generated at Image Label Name No need to activate the free version of Variation Swatcher for WooCommerce plugin while the pro version is activated. Not enough data Remove image Restore product attributes types Select all Select none Select terms Slug Taxonomy is not exists ThemeAlien This term is exists Upload/Add image Use image Variation Swatcher for WooCommerce Variation Swatcher for WooCommerce is enabled but not effective. It requires WooCommerce in order to work. Wrong request http://themealien.com/ http://themealien.com/wordpress-plugin/woocommerce-variation-swatches Project-Id-Version: Variation Swatcher for WooCommerce
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-02-22 10:41+0000
PO-Revision-Date: 2020-11-26 08:25+0000
Last-Translator: 
Language-Team: فارسی
Language: fa_IR
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.6; wp-5.5.3 افزودن جدید افزودن جدید اصطلاح جدید اضافه کنید با موفقیت اضافه شد همه ویژگی ها بازیابی شده اند. توسعه WooCommerce برای ایجاد محصولات متغیر در نظر کاربران از زیبایی و کاربرپسندی بیشتری برخوردار است. لغو انتخاب تصویر رنگ رد کردن اخطار نسخه پشتیبان از انواع ویژگی های محصول پیدا شد. این نسخه پشتیبان تهیه شد در تصویر برچسپ نام در حالی که نسخه حرفه ای فعال است نیازی به فعال سازی نسخه رایگان Variation Swatcher برای افزونه WooCommerce نیست. داده کافی نیست حذف تصویر انواع ویژگی های محصول را بازیابی کنید انتخاب همه انتخاب هیچکدام انتخاب اصطلاحات Slug طبقه بندی وجود ندارد ThemeAlien این اصطلاح موجود است بارگذاری/افزودن تصویر استفاده از تصویر افزونه نمایش ویژگی های ووکامرس Variation Swatcher برای WooCommerce فعال است اما موثر نیست. برای کار کردن به WooCommerce نیاز دارد. درخواست اشتباه http://themealien.com/ http://themealien.com/wordpress-plugin/woocommerce-variation-swatches 