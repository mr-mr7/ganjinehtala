<?php
/**
 * Davici Settings Options
 */
if (!class_exists('Redux_Framework_davici_settings')) {
    class Redux_Framework_davici_settings {
        public $args        = array();
        public $sections    = array();
        public $theme;
        public $ReduxFramework;
        public function __construct() {
            if (!class_exists('ReduxFramework')) {
                return;
            }
            // This is needed. Bah WordPress bugs.  ;)
            if (  true == Redux_Helpers::isTheme(__FILE__) ) {
                $this->initSettings();
            } else {
                add_action('plugins_loaded', array($this, 'initSettings'), 10);
            }
        }
        public function initSettings() {
            $this->theme = wp_get_theme();
            // Set the default arguments
            $this->setArguments();
            // Set a few help tabs so you can see how it's done
            $this->setHelpTabs();
            // Create the sections and fields
            $this->setSections();
            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }
            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
			$custom_font = davici_get_config('custom_font',false);
			if($custom_font != 1){
				remove_action( 'wp_head', array( $this->ReduxFramework, '_output_css' ),150 );
			}
        }
        function compiler_action($options, $css, $changed_values) {
        }
        function dynamic_section($sections) {
            return $sections;
        }
        function change_arguments($args) {
            return $args;
        }
        function change_defaults($defaults) {
            return $defaults;
        }
        function remove_demo() {
        }
        public function setSections() {
            $page_layouts = davici_options_layouts();
            $sidebars = davici_options_sidebars();
            $davici_header_type = davici_options_header_types();
            $davici_banners_effect = davici_options_banners_effect();
            // General Settings  ------------
            $this->sections[] = array(
                'icon' => 'fa fa-home',
                'icon_class' => 'icon',
                'title' => esc_html__('عمومی', 'davici'),
                'fields' => array(                
                )
            );  
            // Layout Settings
            $this->sections[] = array(
                'icon' => 'icofont icofont-double-right',
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('طرح', 'davici'),
                'fields' => array(
                    array(
                        'id' => 'background_img',
                        'type' => 'media',
                        'title' => esc_html__('تصویر پس زمینه', 'davici'),
                        'sub_desc' => '',
                        'default' => ''
                    ),
                    array(
                        'id'=>'show-newletter',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش فرم خبرنامه', 'davici'),
                        'default' => false,
                        'on' => esc_html__('نمایش', 'davici'),
                        'off' => esc_html__('پنهان', 'davici'),
                    ),
                    array(
                        'id' => 'background_newletter_img',
                        'type' => 'media',
                        'title' => esc_html__('تصویر خبرنامه پاپ آپ', 'davici'),
                        'url'=> true,
                        'readonly' => false,
                        'sub_desc' => '',
                        'default' => array(
                            'url' => get_template_directory_uri() . '/images/newsletter-image.jpg'
                        )
                    ),
                    array(
                            'id' => 'back_active',
                            'type' => 'switch',
                            'title' => esc_html__('بازگشت به بالا', 'davici'),
                            'sub_desc' => '',
                            'desc' => '',
                            'default' => '1'// 1 = on | 0 = off
                            ),                          
                    array(
                            'id' => 'direction',
                            'type' => 'select',
                            'title' => esc_html__('جهت', 'davici'),
                            'options' => array( 'ltr' => esc_html__('چپ به راست', 'davici'), 'rtl' => esc_html__('راست به چپ', 'davici') ),
                            'default' => 'ltr'
                        )        
                )
            );
            // Logo & Icons Settings
            $this->sections[] = array(
                'icon' => 'icofont icofont-double-right',
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('لوگو و آیکون', 'davici'),
                'fields' => array(
                    array(
                        'id'=>'sitelogo',
                        'type' => 'media',
                        'compiler'  => 'true',
                        'mode'      => false,
                        'title' => esc_html__('لوگو', 'davici'),
                        'desc'      => esc_html__('تصویر پیش فرض لوگو را در اینجا بارگذاری کنید.', 'davici'),
                        'default' => array(
                            'url' => get_template_directory_uri() . '/images/logo/logo.png'
                        )
                    )
                )
            );
			//Vertical Menu
            $this->sections[] = array(
                'icon' => 'icofont icofont-double-right',
                'subsection' => true,
                'title' => esc_html__('منوی عمودی', 'davici'),
                'fields' => array( 
                    array(
                        'id'        => 'max_number_1530',
                        'type'      => 'text',
                        'title'     => esc_html__('حداکثر تعداد در صفحه نمایش> = 1530 پیکسل', 'davici'),
                        'default'   => '12'
                    ),
                    array(
                        'id'        => 'max_number_1200',
                        'type'      => 'text',
                        'title'     => esc_html__('حداکثر تعداد در صفحه نمایش > = 1200 پیکسل', 'davici'),
                        'default'   => '8'
                    ),
					array(
                        'id'        => 'max_number_991',
                        'type'      => 'text',
                        'title'     => esc_html__('حداکثر تعداد در صفحه نمایش> = 991px', 'davici'),
                        'default'   => '6'
                    )
                )
            );
            // Header Settings
            $this->sections[] = array(
                'icon' => 'icofont icofont-double-right',
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('هدر', 'davici'),
                'fields' => array(
                    array(
                        'id'=>'header_style',
                        'type' => 'image_select',
                        'full_width' => true,
                        'title' => esc_html__('نوع هدر', 'davici'),
                        'options' => $davici_header_type,
                        'default' => '3'
                    ),
                    array(
                        'id'=>'show-header-top',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش هدر بالا', 'davici'),
                        'default' => false,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
                    array(
                        'id'=>'show-searchform',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش فرم جستجو', 'davici'),
                        'default' => false,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
                    array(
                        'id'=>'show-ajax-search',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش جستجوی آژاکس', 'davici'),
                        'default' => false,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici')
                    ),
                    array(
                        'id'=>'limit-ajax-search',
                        'type' => 'text',
                        'title' => esc_html__('حدود نتایج جستجو', 'davici'),
						'default' => 6,
						'required' => array('show-ajax-search','equals',true)
                    ),					
                    array(
                        'id'=>'search-cats',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش دسته بندی ها', 'davici'),
                        'required' => array('search-type','equals',array('post', 'product')),
                        'default' => false,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
                    array(
                        'id'=>'show-wishlist',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش علاقه مندی ها', 'davici'),
                        'default' => false,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
					array(
                        'id'=>'show-menutop',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش منوی بالا', 'davici'),
                        'default' => false,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
					array(
                        'id'=>'show-currency',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش ارز', 'davici'),
                        'default' => false,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
					array(
                        'id'=>'show-compare',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش مقایسه', 'davici'),
                        'default' => false,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
					array(
                        'id'=>'show-minicart',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش سبد خرید کوچک', 'davici'),
                        'default' => false,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
					array(
                        'id'=>'cart-style',
						'type' => 'button_set',
                        'title' => esc_html__('استایل سبدخرید', 'davici'),
                        'options' => array('dropdown' => esc_html__('دراپ دان', 'davici'),
											'popup' => esc_html__('پاپ آپ', 'davici')),
						'default' => 'popup',
						'required' => array('show-minicart','equals',true),
                    ),
                    array(
                        'id'=>'enable-sticky-header',
                        'type' => 'switch',
                        'title' => esc_html__('فعال کردن هدر چسپان', 'davici'),
                        'default' => false,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),		
                    array(
                        'id'=>'phone',
                        'type' => 'text',
                        'title' => esc_html__('تلفن هدر', 'davici'),
                        'default' => ''
                    ),
					 array(
                        'id'=>'shipping',
                        'type' => 'text',
                        'title' => esc_html__('Header Ship', 'davici'),
                        'default' => ''
                    ),
					array(
                        'id'=>'email',
                        'type' => 'text',
                        'title' => esc_html__('آدرس ایمیل هدر', 'davici'),
                        'default' => ''
                    ),
					array(
                        'id'=>'address',
                        'type' => 'text',
                        'title' => esc_html__('آدرس هدر', 'davici'),
                        'default' => ''
                    ),
					array(
                        'id'=>'menu1',
                        'type' => 'text',
                        'title' => esc_html__('برچسپ لیست 1', 'davici'),
                        'default' => ''
                    ),
					array(
                        'id'=>'linkmenu1',
                        'type' => 'text',
                        'title' => esc_html__('لینک لیست 1', 'davici'),
                        'default' => ''
                    ),
					array(
                        'id'=>'menu2',
                        'type' => 'text',
                        'title' => esc_html__('برچسپ لیست 2', 'davici'),
                        'default' => ''
                    ),
					array(
                        'id'=>'linkmenu2',
                        'type' => 'text',
                        'title' => esc_html__('لینک لیست 2', 'davici'),
                        'default' => ''
                    ),
                )
            );
            // Footer Settings
            $footers = davici_get_footers();
            $this->sections[] = array(
                'icon' => 'icofont icofont-double-right',
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('فوتر', 'davici'),
                'fields' => array(
                    array(
                        'id' => 'footer_style',
                        'type' => 'image_select',
                        'title' => esc_html__('استایل فوتر', 'davici'),
                        'sub_desc' => esc_html__( 'انتخاب استایل فوتر', 'davici' ),
                        'desc' => '',
                        'options' => $footers,
                        'default' => '32'
                    ),
                )
            );
            // Copyright Settings
            $this->sections[] = array(
                'icon' => 'icofont icofont-double-right',
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('کپی رایت', 'davici'),
                'fields' => array(
                    array(
                        'id' => "footer-copyright",
                        'type' => 'textarea',
                        'title' => esc_html__('کپی رایت', 'davici'),
                        'default' => sprintf( wp_kses('&copy; Copyright %s. All Rights Reserved.', 'davici'), date('Y') )
                    ),
                    array(
                        'id'=>'footer-payments',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش لوگوهای پرداخت', 'davici'),
                        'default' => false,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
                    array(
                        'id'=>'footer-payments-image',
                        'type' => 'media',
                        'url'=> true,
                        'readonly' => false,
                        'title' => esc_html__('تصاویر پرداخت', 'davici'),
                        'required' => array('footer-payments','equals','1'),
                        'default' => array(
                            'url' => get_template_directory_uri() . '/images/payments.png'
                        )
                    ),
                    array(
                        'id'=>'footer-payments-image-alt',
                        'type' => 'text',
                        'title' => esc_html__('تصاویر پرداخت Alt', 'davici'),
                        'required' => array('footer-payments','equals','1'),
                        'default' => ''
                    ),
                    array(
                        'id'=>'footer-payments-link',
                        'type' => 'text',
                        'title' => esc_html__(' لینک URL پرداخت ها ', 'davici'),
                        'required' => array('footer-payments','equals','1'),
                        'default' => ''
                    )
                )
            );
            // Page Title Settings
            $this->sections[] = array(
                'icon' => 'icofont icofont-double-right',
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('عنوان صفحه', 'davici'),
                'fields' => array(
                    array(
                        'id'=>'page_title',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش عنوان صفحه', 'davici'),
                        'default' => true,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
                    array(
                        'id'=>'page_title_bg',
                        'type' => 'media',
                        'url'=> true,
                        'readonly' => false,
                        'title' => esc_html__('پس زمینه', 'davici'),
                        'required' => array('page_title','equals', true),
	                    'default' => array(
                            'url' => "",
                        )							
                    ),
                    array(
                        'id' => 'breadcrumb',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش مسیر صفحه', 'davici'),
                        'default' => true,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                        'required' => array('page_title','equals', true),
                    ),
                )
            );
            // 404 Page Settings
            $this->sections[] = array(
                'icon' => 'icofont icofont-double-right',
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('خطای 404', 'davici'),
                'fields' => array(
                    array(
                        'id'=>'title-error',
                        'type' => 'text',
                        'title' => esc_html__('عنوان صفحه 404', 'davici'),
                        'desc' => esc_html__(' عنوان صفحه 404 را وارد کنید', 'davici'),
                        'default' => '404'
                    ),
					array(
                        'id'=>'sub-title',
                        'type' => 'text',
                        'title' => esc_html__('عنوان فرعی صفحه 404', 'davici'),
                        'desc' => esc_html__('عنوان فرعی صفحه 404 را وارد کنید ', 'davici'),
                        'default' => "Oops! That page can't be found."
                    ), 					
                    array(
                        'id'=>'sub-error',
                        'type' => 'text',
                        'title' => esc_html__('محتوای صفحه 404', 'davici'),
                        'desc' => esc_html__(' محتوای صفحه 404 را وارد کنید', 'davici'),
                        'default' => 'Sorry, but the page you are looking for is not found. Please, make sure you have typed the current URL.'
                    ),               
                    array(
                        'id'=>'btn-error',
                        'type' => 'text',
                        'title' => esc_html__('دکمه صفحه 404', 'davici'),
                        'desc' => esc_html__('نام دکمه را وارد کنید', 'davici'),
                        'default' => 'Go To Home'
                    )                     
                )
            );
            // Social Share Settings
            $this->sections[] = array(
                'icon' => 'icofont icofont-double-right',
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('اشتراک گذاری', 'davici'),
                'fields' => array(
                    array(
                        'id'=>'social-share',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش لینک شبکه های اجتماعی', 'davici'),
                        'desc' => esc_html__('لینک شبکه های اجتماعی را در پست، محصول، صفحه، نمونه کارها و غیره نشان دهید', 'davici'),
                        'default' => true,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
                    array(
                        'id'=>'share-fb',
                        'type' => 'switch',
                        'title' => esc_html__('فعال کردن اشتراک گذاری در فیسبوک', 'davici'),
                        'default' => true,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
                    array(
                        'id'=>'share-tw',
                        'type' => 'switch',
                        'title' => esc_html__('فعال کردن اشتراک گذاری در توئیتر', 'davici'),
                        'default' => true,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
                    array(
                        'id'=>'share-linkedin',
                        'type' => 'switch',
                        'title' => esc_html__('فعال کردن اشتراک گذاری در لینکدین', 'davici'),
                        'default' => true,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
                    array(
                        'id'=>'share-pinterest',
                        'type' => 'switch',
                        'title' => esc_html__('فعال کردن اشتراک گذاری در پینترست', 'davici'),
                        'default' => false,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
                )
            );
            $this->sections[] = array(
				'icon' => 'icofont icofont-double-right',
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('لینک شبکه های اجتماعی', 'davici'),
                'fields' => array(
                    array(
                        'id'=>'socials_link',
                        'type' => 'switch',
                        'title' => esc_html__('فعال کردن لینک شبکه های اجتماعی', 'davici'),
                        'default' => true,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
                    array(
                        'id'=>'link-fb',
                        'type' => 'text',
                        'title' => esc_html__('لینک فیسبوک را وارد کنید', 'davici'),
						'default' => '#'
                    ),
                    array(
                        'id'=>'link-tw',
                        'type' => 'text',
                        'title' => esc_html__('لینک توئیتر را وارد کنید', 'davici'),
						'default' => '#'
                    ),
                    array(
                        'id'=>'link-linkedin',
                        'type' => 'text',
                        'title' => esc_html__('لینک لینکدین را وارد کنید', 'davici'),
						'default' => '#'
                    ),
                    array(
                        'id'=>'link-youtube',
                        'type' => 'text',
                        'title' => esc_html__('لینک یوتیوب را وارد کنید', 'davici'),
						'default' => '#'
                    ),
                    array(
                        'id'=>'link-pinterest',
                        'type' => 'text',
                        'title' => esc_html__('لینک پینترست را وارد کنید', 'davici'),
						'default' => '#'
                    ),
                    array(
                        'id'=>'link-instagram',
                        'type' => 'text',
                        'title' => esc_html__('لینک اینستاگرام را وارد کنید', 'davici'),
						'default' => '#'
                    ),
                )
            );			
            //     The end -----------
            // Styling Settings  -------------
            $this->sections[] = array(
                'icon' => 'icofont icofont-brand-appstore',
                'icon_class' => 'icon',
                'title' => esc_html__('طراحی ظاهری', 'davici'),
                'fields' => array(              
                )
            );  
            // Color & Effect Settings
            $this->sections[] = array(
                'icon' => 'icofont icofont-double-right',
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('رنگ و افکت', 'davici'),
                'fields' => array(
                    array(
                        'id'=>'compile-css',
                        'type' => 'switch',
                        'title' => esc_html__('کامپایل css', 'davici'),
                        'default' => false,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),					
                    array(
                      'id' => 'main_theme_color',
                      'type' => 'color',
                      'title' => esc_html__('رنگ اصلی قالب', 'davici'),
                      'subtitle' => esc_html__('رنگ اصلی را برای سایت انتخاب کنید', 'davici'),
                      'default' => '#222222',
                      'transparent' => false,
					  'required' => array('compile-css','equals',array(true)),
                    ),      
                    array(
                        'id'=>'show-loading-overlay',
                        'type' => 'switch',
                        'title' => esc_html__('درحال بارگذاری', 'davici'),
                        'default' => false,
                        'on' => esc_html__('نمایش', 'davici'),
                        'off' => esc_html__('پنهان', 'davici'),
                    ),
                    array(
                        'id'=>'banners_effect',
                        'type' => 'image_select',
                        'full_width' => true,
                        'title' => esc_html__('افکت بنر', 'davici'),
                        'options' => $davici_banners_effect,
                        'default' => 'banners-effect-1'
                    )                   
                )
            );
            // Typography Settings
            $this->sections[] = array(
                'icon' => 'icofont icofont-double-right',
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('تایپوگرافی', 'davici'),
                'fields' => array(
                    array(
                        'id'=>'custom_font',
                        'type' => 'switch',
                        'title' => esc_html__('فونت سفارشی', 'davici'),
                        'default' => false,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),				
                    array(
                        'id'=>'select-google-charset',
                        'type' => 'switch',
                        'title' => esc_html__('انتخاب مجموعه نویسه های فونت گوگل', 'davici'),
                        'default' => false,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
						'required' => array('custom_font','equals',true),
                    ),
                    array(
                        'id'=>'google-charsets',
                        'type' => 'button_set',
                        'title' => esc_html__('مجموعه نویسه های فونت گوگل', 'davici'),
                        'multi' => true,
                        'required' => array('select-google-charset','equals',true),
                        'options'=> array(
                            'cyrillic' => 'Cyrrilic',
                            'cyrillic-ext' => 'Cyrrilic Extended',
                            'greek' => 'Greek',
                            'greek-ext' => 'Greek Extended',
                            'khmer' => 'Khmer',
                            'latin' => 'Latin',
                            'latin-ext' => 'Latin Extneded',
                            'vietnamese' => 'Vietnamese'
                        ),
                        'default' => array('latin','greek-ext','cyrillic','latin-ext','greek','cyrillic-ext','vietnamese','khmer')
                    ),
                    array(
                        'id'=>'family_font_body',
                        'type' => 'typography',
                        'title' => esc_html__('Body فونت', 'davici'),
                        'google' => true,
                        'subsets' => false,
                        'font-style' => false,
                        'text-align' => false,
						'output'      => array('body'),
                        'color' => false,
                        'default'=> array(
                            'color'=>"#777777",
                            'google'=>true,
                            'font-weight'=>'400',
                            'font-family'=>'Open Sans',
                            'font-size'=>'14px',
                            'line-height' => '22px'
                        ),
						'required' => array('custom_font','equals',true)
                    ),
                    array(
                        'id'=>'h1-font',
                        'type' => 'typography',
                        'title' => esc_html__('H1 فونت', 'davici'),
                        'google' => true,
                        'subsets' => false,
                        'font-style' => false,
                        'text-align' => false,
                        'color' 	=> false,
						'output'      => array('body h1'),
                        'default'=> array(
                            'color'=>"#1d2127",
                            'google'=>true,
                            'font-weight'=>'400',
                            'font-family'=>'Open Sans',
                            'font-size'=>'36px',
                            'line-height' => '44px'
                        ),
						'required' => array('custom_font','equals',true)
                    ),
                    array(
                        'id'=>'h2-font',
                        'type' => 'typography',
                        'title' => esc_html__('H2 فونت', 'davici'),
                        'google' => true,
                        'subsets' => false,
                        'font-style' => false,
                        'text-align' => false,
                        'color' => false,
						'output'      => array('body h2'),
                        'default'=> array(
                            'color'=>"#1d2127",
                            'google'=>true,
                            'font-weight'=>'300',
                            'font-family'=>'Open Sans',
                            'font-size'=>'30px',
                            'line-height' => '40px'
                        ),
						'required' => array('custom_font','equals',true)
                    ),
                    array(
                        'id'=>'h3-font',
                        'type' => 'typography',
                        'title' => esc_html__('H3 فونت', 'davici'),
                        'google' => true,
                        'subsets' => false,
                        'font-style' => false,
                        'text-align' => false,
                        'color' => false,
						'output'      => array('body h3'),
                        'default'=> array(
                            'color'=>"#1d2127",
                            'google'=>true,
                            'font-weight'=>'400',
                            'font-family'=>'Open Sans',
                            'font-size'=>'25px',
                            'line-height' => '32px'
                        ),
						'required' => array('custom_font','equals',true)
                    ),
                    array(
                        'id'=>'h4-font',
                        'type' => 'typography',
                        'title' => esc_html__('H4 فونت', 'davici'),
                        'google' => true,
                        'subsets' => false,
                        'font-style' => false,
                        'text-align' => false,
                        'color' => false,
						'output'      => array('body h4'),
                        'default'=> array(
                            'color'=>"#1d2127",
                            'google'=>true,
                            'font-weight'=>'400',
                            'font-family'=>'Open Sans',
                            'font-size'=>'20px',
                            'line-height' => '27px'
                        ),
						'required' => array('custom_font','equals',true)
                    ),
                    array(
                        'id'=>'h5-font',
                        'type' => 'typography',
                        'title' => esc_html__('H5 فونت', 'davici'),
                        'google' => true,
                        'subsets' => false,
                        'font-style' => false,
                        'text-align' => false,
                        'color' => false,
						'output'      => array('body h5'),
                        'default'=> array(
                            'color'=>"#1d2127",
                            'google'=>true,
                            'font-weight'=>'600',
                            'font-family'=>'Open Sans',
                            'font-size'=>'14px',
                            'line-height' => '18px'
                        ),
						'required' => array('custom_font','equals',true)
                    ),
                    array(
                        'id'=>'h6-font',
                        'type' => 'typography',
                        'title' => esc_html__('H6 فونت', 'davici'),
                        'google' => true,
                        'subsets' => false,
                        'font-style' => false,
                        'text-align' => false,
                        'color' => false,
						'output'      => array('body h6'),
                        'default'=> array(
                            'color'=>"#1d2127",
                            'google'=>true,
                            'font-weight'=>'400',
                            'font-family'=>'Open Sans',
                            'font-size'=>'14px',
                            'line-height' => '18px'
                        ),
						'required' => array('custom_font','equals',true)
                    )
                )
            );
            //     The end -----------          
            if ( class_exists( 'Woocommerce' ) ) :
                $this->sections[] = array(
                    'icon' => 'icofont icofont-cart-alt',
                    'icon_class' => 'icon',
                    'title' => esc_html__('تجارت الکترونیک', 'davici'),
                    'fields' => array(              
                    )
                );
                $this->sections[] = array(
                    'icon' => 'icofont icofont-double-right',
                    'icon_class' => 'icon',
                    'subsection' => true,
                    'title' => esc_html__('بایگانی محصولات', 'davici'),
                    'fields' => array(
                        array(
                            'id'=>'category_style',
                            'title' => esc_html__('استایل بایگانی محصولات', 'davici'),
                            'type' => 'select',
							'options' => array(
                                'sidebar' => esc_html__('سایدبار', 'davici'),       
                                'filter_drawer' => esc_html__('فیلتر Drawer', 'davici'),
								'filter_dropdown' => esc_html__('فیلتر Dropdown', 'davici'),
								'filter_offcanvas' => esc_html__('فیلتر Off Canvas', 'davici'),
								'shop_background' => esc_html__('فروشگاه با پس زمینه', 'davici'),
								'only_categories' => esc_html__('فروشگاه فقط با دسته بندی', 'davici'),
                             ),
                            'default' => 'sidebar',
                        ),
						array(
                            'id'=>'show-subcategories',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش دسته های فرعی', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
						array(
                            'id'=>'style-subcategories',
							'title' => esc_html__('استایل دسته های فرعی', 'davici'),
                            'type' => 'select',
							'options' => array(
								'shop_mini_categories' => esc_html__('مینی دسته بندی ها', 'davici'),
								'icon_categories' => esc_html__('آیکون دسته بندی ها', 'davici'),
								'image_categories' => esc_html__('تصویر دسته بندی ها', 'davici'),
                             ),
                            'default' => 'mini',
							'required' => array('show-subcategories','equals','1'),
                        ),
                        array(
                            'id' => 'sub_col_large',
                            'type' => 'button_set',
                            'title' => esc_html__('ستون دسته بندی های فرعی در دسکتاپ', 'davici'),
                            'options' => array(
                                    '2' => '2',
                                    '3' => '3',
                                    '4' => '4',                         
                                    '6' => '6'                          
                                ),
                            'default' => '4',
							'required' => array('show-subcategories','equals','1'),
                            'sub_desc' => esc_html__( 'انتخاب تعداد ستون در صفحه نمایش دسکتاپ', 'davici' ),
                        ),
                        array(
                            'id' => 'sub_col_medium',
                            'type' => 'button_set',
                            'title' => esc_html__('ستون دسته بندی های فرعی در دسکتاپ متوسط', 'davici'),
                            'options' => array(
                                    '2' => '2',
                                    '3' => '3',
                                    '4' => '4',                         
                                    '6' => '6'                          
                                ),
                            'default' => '3',
							'required' => array('show-subcategories','equals','1'),
                            'sub_desc' => esc_html__( 'انتخاب تعداد ستون در صفحه نمایش دسکتاپ متوسط', 'davici' ),
                        ),
                        array(
                            'id' => 'sub_col_sm',
                            'type' => 'button_set',
                            'title' => esc_html__('ستون دسته بندی های فرعی در صفحه نمایش Ipad ', 'davici'),
                            'options' => array(
                                    '2' => '2',
                                    '3' => '3',
                                    '4' => '4',                         
                                    '6' => '6'                          
                                ),
                            'default' => '3',
							'required' => array('show-subcategories','equals','1'),
                            'sub_desc' => esc_html__( 'انتخاب تعداد ستون در صفحه نمایش Ipad ', 'davici' ),
                        ),						
                        array(
                            'id'=>'category-view-mode',
                            'type' => 'button_set',
                            'title' => esc_html__('حالت نمایش', 'davici'),
                            'options' => davici_ct_category_view_mode(),
                            'default' => 'grid',
                        ),
                        array(
                            'id' => 'product_col_large',
                            'type' => 'button_set',
                            'title' => esc_html__('ستون لیست محصولات در دسکتاپ', 'davici'),
                            'options' => array(
                                    '2' => '2',
                                    '3' => '3',
                                    '4' => '4',                         
                                    '6' => '6'                          
                                ),
                            'default' => '4',
							'required' => array('category-view-mode','equals','grid'),
                            'sub_desc' => esc_html__( 'انتخاب تعداد ستون در صفحه نمایش دسکتاپ', 'davici' ),
                        ),
                        array(
                            'id' => 'product_col_medium',
                            'type' => 'button_set',
                            'title' => esc_html__('ستون لیست محصولات در دسکتاپ متوسط', 'davici'),
                            'options' => array(
                                    '2' => '2',
                                    '3' => '3',
                                    '4' => '4',                         
                                    '6' => '6'                          
                                ),
                            'default' => '3',
							'required' => array('category-view-mode','equals','grid'),
                            'sub_desc' => esc_html__( 'انتخاب تعداد ستون در صفحه نمایش دسکتاپ متوسط', 'davici' ),
                        ),
                        array(
                            'id' => 'product_col_sm',
                            'type' => 'button_set',
                            'title' => esc_html__('ستون لیست محصولات در صفحه نمایش Ipad', 'davici'),
                            'options' => array(
                                    '2' => '2',
                                    '3' => '3',
                                    '4' => '4',                         
                                    '6' => '6'                          
                                ),
                            'default' => '3',
							'required' => array('category-view-mode','equals','grid'),
                            'sub_desc' => esc_html__( 'انتخاب تعداد ستون در صفحه نمایش Ipad', 'davici' ),
                        ),
						array(
                            'id' => 'product_col_xs',
                            'type' => 'button_set',
                            'title' => esc_html__('ستون لیست محصولات در صفحه نمایش موبایل', 'davici'),
                            'options' => array(
									'1' => '1',
                                    '2' => '2',
                                    '3' => '3',
                                    '4' => '4',                         
                                ),
                            'default' => '2',
							'required' => array('category-view-mode','equals','grid'),
                            'sub_desc' => esc_html__( 'انتخاب تعداد ستون در صفحه نمایش موبایل', 'davici' ),
                        ),
						array(
                            'id'=>'show-bestseller-category',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش پرفروش ترین ها در دسته بندی صفحه', 'davici'),
                            'default' => false,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
						 array(
                            'id' => 'bestseller_limit',
                            'type' => 'text',
                            'title' => esc_html__('پرفروش ترین محصولات فروشگاه', 'davici'),
                            'default' => '9',
							'required' => array('show-bestseller-category','equals',true),
                        ),
                        array(
                            'id'=>'show-banner-category',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش بنر دسته بندی', 'davici'),
                            'default' => false,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
                        array(
                            'id'=>'woo-show-rating',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش امتیاز در ویجت محصولات ووکامرس', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
                        array(
                            'id' => 'product_count',
                            'type' => 'text',
                            'title' => esc_html__('نمایش صفحات فروشگاه در محصول', 'davici'),
                            'default' => '12',
                            'sub_desc' => esc_html__( 'تعداد محصولات را در هر صفحه فروشگاه تایپ کنید', 'davici' ),
                        ),						
                        array(
                            'id'=>'category-image-hover',
                            'type' => 'switch',
                            'title' => esc_html__('فعال کردن افکت هاور تصاویر', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
                        array(
                            'id'=>'category-hover',
                            'type' => 'switch',
                            'title' => esc_html__('فعال کردن افکت هاور', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
                        array(
                            'id'=>'product-wishlist',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش لیست علاقه مندی ها', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
						array(
							'id'=>'product-compare',
							'type' => 'switch',
							'title' => esc_html__('نمایش مقایسه', 'davici'),
							'default' => false,
							'on' => esc_html__('بله', 'davici'),
							'off' => esc_html__('خیر', 'davici'),
						),						
                        array(
                            'id'=>'product_quickview',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش نمایش سریع', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici')
                        ),
                        array(
                            'id'=>'product-quickview-label',
                            'type' => 'text',
                            'required' => array('product-quickview','equals',true),
                            'title' => esc_html__('"Quick View" متن', 'davici'),
                            'default' => ''
                        ),
						array(
                            'id'=>'product-countdown',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش شمارش معکوس محصول', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici')
                        ),
						array(
                            'id'=>'product-attribute',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش ویژگی های محصول', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici')
                        ),						
                    )
                );
                $this->sections[] = array(
                    'icon' => 'icofont icofont-double-right',
                    'icon_class' => 'icon',
                    'subsection' => true,
                    'title' => esc_html__('محصول تکی', 'davici'),
                    'fields' => array(
                        array(
                            'id'=>'sidebar_detail_product',
                            'type' => 'image_select',
                            'title' => esc_html__('طرح صفحه', 'davici'),
                            'options' => $page_layouts,
                            'default' => 'full'
                        ),
                        array(
                            'id'=>'product-stock',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش وضعیت  "ناموجود" ', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
						array(
                            'id'=>'show-extra-sidebar',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش سایدبار اضافی', 'davici'),
                            'default' => false,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
						array(
                            'id'=>'show-featured-icon',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش آیکون ویژه', 'davici'),
                            'default' => false,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
						array(
                            'id'=>'show-sticky-cart',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش سبد چسپان محصول', 'davici'),
                            'default' => false,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
						array(
                            'id'=>'show-brands',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش برندها', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
						array(
                            'id'=>'show-offer',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش لیست پیشنهادات', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),						
						array(
                            'id'=>'show-countdown',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش شمارش معکوس', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
                        array(
                            'id'=>'product-short-desc',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش توضیح کوتاه', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
                        array(
                            'id'=>'show-trust-bages',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش نماد اعتماد محصول', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
						array(
							'id' => 'trust-bages',
							'type' => 'media',
							'title' => esc_html__('نماد اعتماد', 'davici'),
							'url'=> true,
							'readonly' => false,
							'required' => array('show-trust-bages','equals',true),
							'sub_desc' => '',
							'default' => array(
								'url' => ""
							)
						),					
                        array(
                            'id'=>'product-related',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش محصولات مرتبط', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
                        array(
                            'id'=>'product-related-count',
                            'type' => 'text',
                            'required' => array('product-related','equals',true),
                            'title' => esc_html__('تعداد محصولات مرتبط', 'davici'),
                            'default' => '10'
                        ),
                        array(
                            'id'=>'product-related-cols',
                            'type' => 'button_set',
                            'required' => array('product-related','equals',true),
                            'title' => esc_html__('ستون محصولات مرتبط', 'davici'),
                            'options' => davici_ct_related_product_columns(),
                            'default' => '4',
                        ),
                        array(
                            'id'=>'product-upsell',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش محصولات بیش فروش', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),                      
                        array(
                            'id'=>'product-upsell-count',
                            'type' => 'text',
                            'required' => array('product-upsell','equals',true),
                            'title' => esc_html__('تعداد محصولات بیش فروش', 'davici'),
                            'default' => '10'
                        ),
                        array(
                            'id'=>'product-upsell-cols',
                            'type' => 'button_set',
                            'required' => array('product-upsell','equals',true),
                            'title' => esc_html__('ستون محصولات بیش فروش', 'davici'),
                            'options' => davici_ct_related_product_columns(),
                            'default' => '3',
                        ),
                        array(
                            'id'=>'product-crosssells',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش محصولات فروش مکمل', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),                      
                        array(
                            'id'=>'product-crosssells-count',
                            'type' => 'text',
                            'required' => array('product-crosssells','equals',true),
                            'title' => esc_html__('تعداد محصولات فروش مکمل', 'davici'),
                            'default' => '10'
                        ),
                        array(
                            'id'=>'product-crosssells-cols',
                            'type' => 'button_set',
                            'required' => array('product-crosssells','equals',true),
                            'title' => esc_html__('ستون محصولات فروش مکمل', 'davici'),
                            'options' => davici_ct_related_product_columns(),
                            'default' => '3',
                        ),						
                        array(
                            'id'=>'product-hot',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش برچسپ "داغ"', 'davici'),
                            'desc' => esc_html__('در محصول ویژه نشان داده میشود.', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
                        array(
                            'id'=>'product-hot-label',
                            'type' => 'text',
                            'required' => array('product-hot','equals',true),
                            'title' => esc_html__('متن"داغ"', 'davici'),
                            'default' => ''
                        ),
                        array(
                            'id'=>'product-sale',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش  برچسپ  "فروش"', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
                         array(
                            'id'=>'product-sale-percent',
                            'type' => 'switch',
                            'required' => array('product-sale','equals',true),
                            'title' => esc_html__('نمایش درصد قیمت فروش', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),  
                        array(
                            'id'=>'product-share',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش لینک اشتراک گذاری شبکه های اجتماعی', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
                    )
                );
                $this->sections[] = array(
                    'icon' => 'icofont icofont-double-right',
                    'icon_class' => 'icon',
                    'subsection' => true,
                    'title' => esc_html__('تصویر محصول', 'davici'),
                    'fields' => array(
                        array(
                            'id'=>'product-thumbs',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش تصویر کوچک', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
                        array(
                            'id'=>'position-thumbs',
                            'type' => 'button_set',
                            'title' => esc_html__('مکان تصویر کوچک', 'davici'),
                            'options' => array('left' => esc_html__('چپ', 'davici'),
												'right' => esc_html__('راست', 'davici'),
												'bottom' => esc_html__('پایین', 'davici'),
												'outsite' => esc_html__('خارج', 'davici')),
                            'default' => 'bottom',
							'required' => array('product-thumbs','equals',true),
                        ),						
                        array(
                            'id' => 'product-thumbs-count',
                            'type' => 'button_set',
                            'title' => esc_html__('تعداد تصاویر کوچک', 'davici'),
                            'options' => array(
                                    '2' => '2',
                                    '3' => '3',
                                    '4' => '4', 
									'5' => '5', 									
                                    '6' => '6'                          
                                ),
							'default' => '4',
							'required' => array('product-thumbs','equals',true),
                        ),
                        array(
                            'id'=>'product-image-popup',
                            'type' => 'switch',
                            'title' => esc_html__('فعال کردن تصویر پاپ آپ', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),						
                        array(
                            'id'=>'layout-thumbs',
                            'type' => 'button_set',
                            'title' => esc_html__('طرح تصویر کوچک', 'davici'),
                            'options' => array('zoom' => esc_html__('بزرگنمایی', 'davici'),
												'scroll' => esc_html__('اسکرول', 'davici'),
												'sticky' => esc_html__('چسپان', 'davici'),
												'sticky2' => esc_html__('چسپان 2', 'davici'),
												'slider' => esc_html__('اسلایدر', 'davici'),
												'large_grid' => esc_html__('سبکه بزرگ', 'davici'),
												'small_grid' => esc_html__('شبکه کوچک', 'davici'),
											),	
                            'default' => 'zoom',
                        ),
						array(
                            'id'=>'background',
                            'type' => 'switch',
                            'title' => esc_html__('نمایش پس زمینه برای تصویر محصول', 'davici'),
                            'default' => false,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
                        ),
                        array(
                            'id'=>'zoom-type',
                            'type' => 'button_set',
                            'title' => esc_html__('نوع بزرگنمایی', 'davici'),
                            'options' => array('inner' => esc_html__('داخلی', 'davici'), 'lens' => esc_html__('لنز', 'davici')),
                            'default' => 'inner',
							'required' => array('layout-thumbs','equals',"zoom"),
                        ),
                        array(
                            'id'=>'zoom-scroll',
                            'type' => 'switch',
                            'title' => esc_html__('اسکرول بزرگنمایی', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
							'required' => array('layout-thumbs','equals',"zoom"),
                        ),
                        array(
                            'id'=>'zoom-border',
                            'type' => 'text',
                            'title' => esc_html__('اندازه حاشیه', 'davici'),
                            'default' => '2',
							'required' => array('layout-thumbs','equals',"zoom"),
                        ),
                        array(
                            'id'=>'zoom-border-color',
                            'type' => 'color',
                            'title' => esc_html__('رنگ حاشیه', 'davici'),
                            'default' => '#f9b61e',
							'required' => array('layout-thumbs','equals',"zoom"),
                        ),                      
                        array(
                            'id'=>'zoom-lens-size',
                            'type' => 'text',
                            'required' => array('zoom-type','equals',array('lens')),
                            'title' => esc_html__('اندازه لنز ', 'davici'),
                            'default' => '200',
							'required' => array('layout-thumbs','equals',"zoom"),
                        ),
                        array(
                            'id'=>'zoom-lens-shape',
                            'type' => 'button_set',
                            'required' => array('zoom-type','equals',array('lens')),
                            'title' => esc_html__('شکل لنز', 'davici'),
                            'options' => array('round' => esc_html__('گرد', 'davici'), 'square' => esc_html__('مربع', 'davici')),
                            'default' => 'square',
							'required' => array('layout-thumbs','equals',"zoom"),
                        ),
                        array(
                            'id'=>'zoom-contain-lens',
                            'type' => 'switch',
                            'required' => array('zoom-type','equals',array('lens')),
                            'title' => esc_html__('شامل بزرگنمایی لنز', 'davici'),
                            'default' => true,
                            'on' => esc_html__('بله', 'davici'),
                            'off' => esc_html__('خیر', 'davici'),
							'required' => array('layout-thumbs','equals',"zoom"),
                        ),
                        array(
                            'id'=>'zoom-lens-border',
                            'type' => 'text',
                            'required' => array('zoom-type','equals',array('lens')),
                            'title' => esc_html__('حاشیه ذره لنز', 'davici'),
                            'default' => true,
							'required' => array('layout-thumbs','equals',"zoom")
                        ),
                    )
                );
            endif;
            // Blog Settings  -------------
            $this->sections[] = array(
                'icon' => 'icofont icofont-ui-copy',
                'icon_class' => 'icon',
                'title' => esc_html__('وبلاگ', 'davici'),
                'fields' => array(              
                )
            );      
            $this->sections[] = array(
                'icon' => 'icofont icofont-double-right',
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('وبلاگ و بایگانی پست', 'davici'),
                'fields' => array(
                    array(
                        'id'=>'post-format',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش فرمت پست', 'davici'),
                        'default' => true,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
                    array(
                        'id'=>'hot-label',
                        'type' => 'text',
                        'title' => esc_html__(' متن "داغ"', 'davici'),
                        'desc' => esc_html__('برچسپ پست داغ', 'davici'),
                        'default' => ''
                    ),
                    array(
                        'id'=>'sidebar_blog',
                        'type' => 'image_select',
                        'title' => esc_html__('طرح صفحه', 'davici'),
                        'options' => $page_layouts,
                        'default' => 'left'
                    ),
                    array(
                        'id' => 'layout_blog',
                        'type' => 'button_set',
                        'title' => esc_html__('طرح وبلاگ', 'davici'),
                        'options' => array(
                                'list'  =>  esc_html__( 'لیست', 'davici' ),
                                'grid' =>  esc_html__( 'شبکه', 'davici' ),
								'modern' =>  esc_html__( 'مدرن', 'davici' )	
                        ),
                        'default' => 'list',
                        'sub_desc' => esc_html__( 'انتخاب استایل طرح وبلاگ', 'davici' ),
                    ),
                    array(
                        'id' => 'blog_col_large',
                        'type' => 'button_set',
                        'title' => esc_html__('ستون لیست وبلاگ در دسکتاپ', 'davici'),
                        'required' => array('layout_blog','equals','grid'),
                        'options' => array(
                                '2' => '2',
                                '3' => '3',
                                '4' => '4',                         
                                '6' => '6'                          
                            ),
                        'default' => '4',
                        'sub_desc' => esc_html__( 'انتخاب تعداد ستون در صفحه نمایش دسکتاپ', 'davici' ),
                    ),
                    array(
                        'id' => 'blog_col_medium',
                        'type' => 'button_set',
                        'title' => esc_html__('ستون لیست وبلاگ در صفحه نمایش دسکتاپ متوسط', 'davici'),
                        'required' => array('layout_blog','equals','grid'),
                        'options' => array(
                                '2' => '2',
                                '3' => '3',
                                '4' => '4',                         
                                '6' => '6'                          
                            ),
                        'default' => '3',
                        'sub_desc' => esc_html__( 'انتخاب تعداد ستون در صفحه نمایش دسکتاپ متوسط', 'davici' ),
                    ),   
                    array(
                        'id' => 'blog_col_sm',
                        'type' => 'button_set',
                        'title' => esc_html__('ستون لیست وبلاگ در صفحه نمایش Ipad', 'davici'),
                        'required' => array('layout_blog','equals','grid'),
                        'options' => array(
                                '2' => '2',
                                '3' => '3',
                                '4' => '4',                         
                                '6' => '6'                          
                            ),
                        'default' => '3',
                        'sub_desc' => esc_html__( 'انتخاب تعداد ستون در صفحه نمایش Ipad', 'davici' ),
                    ),   					
                    array(
                        'id'=>'archives-author',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش نویسنده', 'davici'),
                        'default' => true,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
                    array(
                        'id'=>'archives-comments',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش تعداد ستون ها', 'davici'),
                        'default' => true,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),                  
                    array(
                        'id'=>'blog-excerpt',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش برگزیده ها', 'davici'),
                        'default' => true,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
                    array(
                        'id'=>'list-blog-excerpt-length',
                        'type' => 'text',
                        'required' => array('blog-excerpt','equals',true),
                        'title' => esc_html__('طول لیست برگزیده ها', 'davici'),
                        'desc' => esc_html__('تعداد کلمات', 'davici'),
                        'default' => '50',
                    ),
                    array(
                        'id'=>'grid-blog-excerpt-length',
                        'type' => 'text',
                        'required' => array('blog-excerpt','equals',true),
                        'title' => esc_html__('طول شبکه برگزیده ها', 'davici'),
                        'desc' => esc_html__('تعداد کلمات', 'davici'),
                        'default' => '12',
                    ),                  
                )
            );
            $this->sections[] = array(
                'icon' => 'icofont icofont-double-right',
                'icon_class' => 'icon',
                'subsection' => true,
                'title' => esc_html__('پست تکی', 'davici'),
                'fields' => array(
                    array(
                        'id'=>'post-single-layout',
                        'type' => 'image_select',
                        'title' => esc_html__('طرح صفحه', 'davici'),
                        'options' => $page_layouts,
                        'default' => 'left'
                    ),
                    array(
                        'id'=>'post-title',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش عنوان', 'davici'),
                        'default' => true,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
                    array(
                        'id'=>'post-author',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش اطلاعات نویسنده', 'davici'),
                        'default' => true,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
                    ),
                    array(
                        'id'=>'post-comments',
                        'type' => 'switch',
                        'title' => esc_html__('نمایش نطرات', 'davici'),
                        'default' => true,
                        'on' => esc_html__('بله', 'davici'),
                        'off' => esc_html__('خیر', 'davici'),
					)
				)
			);	
            $this->sections[] = array(
				'id' => 'wbc_importer_section',
				'title'  => esc_html__( 'درون ریز دمو', 'davici' ),
				'icon'   => 'fa fa-cloud-download',
				'desc'   => wp_kses( 'حداکثر زمان اجرای خود را افزایش دهید ، 40000 را امتحان کنید من می دانم که بالا است اما به من اعتماد کنید.<br>
				محدودیت حافظه PHP خود را افزایش دهید ، 512 مگابایت را امتحان کنید.<br>
				1. فرآیند درون ریزی در نصب تمیز به بهترین وجه کار خواهد کرد. برای پاک کردن داده های خود می توانید از افزونه ای مانند Reset WordPress استفاده کنید.<br>
				2. اطمینان حاصل کنید که همه پلاگین ها از قبل نصب شده اند ، به عنوان مثال WooCommerce - هر افزونه ای که به آن محتوا اضافه می کنید.<br>
				3. صبور باشید و منتظر بمانید تا فرآیند درون ریزی کامل شود. ممکن است 3-5 دقیقه طول بکشد.<br>
				4. لذت ببرید','social' ),				
				'fields' => array(
					array(
						'id'   => 'wbc_demo_importer',
						'type' => 'wbc_importer'
					)
				)
            );			
        }
        public function setHelpTabs() {
        }
        public function setArguments() {
            $theme = wp_get_theme(); // For use with some settings. Not necessary.
            $this->args = array(
                'opt_name'          => 'davici_settings',
                'display_name'      => $theme->get('Name') . ' ' . esc_html__('گزینه های قالب', 'davici'),
                'display_version'   => esc_html__('نسخه قالب: ', 'davici') . davici_version,
                'menu_type'         => 'submenu',
                'allow_sub_menu'    => true,
                'menu_title'        => esc_html__('گزینه های قالب', 'davici'),
                'page_title'        => esc_html__('گزینه های قالب', 'davici'),
                'footer_credit'     => esc_html__('گزینه های قالب', 'davici'),
                'google_api_key' => 'AIzaSyAX_2L_UzCDPEnAHTG7zhESRVpMPS4ssII',
                'disable_google_fonts_link' => true,
                'async_typography'  => false,
                'admin_bar'         => false,
                'admin_bar_icon'       => 'dashicons-admin-generic',
                'admin_bar_priority'   => 50,
                'global_variable'   => '',
                'dev_mode'          => false,
                'customizer'        => false,
                'compiler'          => false,
                'page_priority'     => null,
                'page_parent'       => 'themes.php',
                'page_permissions'  => 'manage_options',
                'menu_icon'         => '',
                'last_tab'          => '',
                'page_icon'         => 'icon-themes',
                'page_slug'         => 'davici_settings',
                'save_defaults'     => true,
                'default_show'      => false,
                'default_mark'      => '',
                'show_import_export' => true,
                'show_options_object' => false,
                'transient_time'    => 60 * MINUTE_IN_SECONDS,
                'output'            => true,
                'output_tag'        => true,
                'database'              => '',
                'system_info'           => false,
                'hints' => array(
                    'icon'          => 'icon-question-sign',
                    'icon_position' => 'right',
                    'icon_color'    => 'lightgray',
                    'icon_size'     => 'normal',
                    'tip_style'     => array(
                        'color'         => 'light',
                        'shadow'        => true,
                        'rounded'       => false,
                        'style'         => '',
                    ),
                    'tip_position'  => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
                    ),
                    'tip_effect'    => array(
                        'show'          => array(
                            'effect'        => 'slide',
                            'duration'      => '500',
                            'event'         => 'mouseover',
                        ),
                        'hide'      => array(
                            'effect'    => 'slide',
                            'duration'  => '500',
                            'event'     => 'click mouseleave',
                        ),
                    ),
                ),
                'ajax_save'                 => false,
                'use_cdn'                   => true,
            );
            // Panel Intro text -> before the form
            if (!isset($this->args['global_variable']) || $this->args['global_variable'] !== false) {
                if (!empty($this->args['global_variable'])) {
                    $v = $this->args['global_variable'];
                } else {
                    $v = str_replace('-', '_', $this->args['opt_name']);
                }
            }
            $this->args['intro_text'] = sprintf('<p style="color: #0088cc">'.wp_kses('Please regenerate again default css files in <strong>Skin > Compile Default CSS</strong> after <strong>update theme</strong>.', 'davici').'</p>', $v);
        }           
    }
	if ( !function_exists( 'wbc_extended_example' ) ) {
		function wbc_extended_example( $demo_active_import , $demo_directory_path ) {
			reset( $demo_active_import );
			$current_key = key( $demo_active_import );	
			if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] )) {
				//Import Sliders
				if ( class_exists( 'RevSlider' ) ) {
					$wbc_sliders_array = array(
						'davici' => array('slider-1.zip','slider-2.zip','slider-3.zip','slider-4.zip','slider-5.zip','slider-6.zip','slider-7.zip','slider-8.zip')
					);
					$wbc_slider_import = $wbc_sliders_array[$demo_active_import[$current_key]['directory']];
					if( is_array( $wbc_slider_import ) ){
						foreach ($wbc_slider_import as $slider_zip) {
							if ( !empty($slider_zip) && file_exists( $demo_directory_path.'rev_slider/'.$slider_zip ) ) {
								$slider = new RevSlider();
								$slider->importSliderFromPost( true, true, $demo_directory_path.'rev_slider/'.$slider_zip );
							}
						}
					}else{
						if ( file_exists( $demo_directory_path.'rev_slider/'.$wbc_slider_import ) ) {
							$slider = new RevSlider();
							$slider->importSliderFromPost( true, true, $demo_directory_path.'rev_slider/'.$wbc_slider_import );
						}
					}
				}				
				// Setting Menus
				$primary = get_term_by( 'name', 'Main menu', 'nav_menu' );
				$primary_vertical = get_term_by( 'name', 'Vertical Menu', 'nav_menu' );
				$primary_currency = get_term_by( 'name', 'Currency Menu', 'nav_menu' );
				$primary_language = get_term_by( 'name', 'Language Menu', 'nav_menu' );
				$primary_topbar   = get_term_by( 'name', 'Menu Topbar', 'nav_menu' );
				if ( isset( $primary->term_id ) ) {
					set_theme_mod( 'nav_menu_locations', array(
							'main_navigation' => $primary->term_id,
							'vertical_menu' => $primary_vertical->term_id,
							'currency_menu' => $primary_currency->term_id,
							'language_menu' => $primary_language->term_id,
							'topbar_menu' => $primary_topbar->term_id
						)
					);
				}
				// Set HomePage
				$home_page = 'Home Page 1';
				$page = get_page_by_title( $home_page );
				if ( isset( $page->ID ) ) {
					update_option( 'page_on_front', $page->ID );
					update_option( 'show_on_front', 'page' );
				}					
			}
		}
		// Uncomment the below
		add_action( 'wbc_importer_after_content_import', 'wbc_extended_example', 10, 2 );
	}
    global $reduxDaviciSettings;
    $reduxDaviciSettings = new Redux_Framework_davici_settings();
}